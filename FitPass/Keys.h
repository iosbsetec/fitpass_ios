//
//  Keys.h
//  Conciergist
//
//  Created by Siba Prasad Hota  on 3/30/15.
//  Copyright (c) 2015 Conciergist. All rights reserved.
//

#import <Foundation/Foundation.h>


//
//#if iPhone6Plus
//
//#define INNER_TAB_1_SELECTED    @"myshow_ip6plus.png"
//#define INNER_TAB_2_SELECTED    @"discover_ip6plus.png"
//#define INNER_TAB_3_SELECTED    @"Mint_ip6plus.png"
//#define INNER_TAB_4_SELECTED    @"activity_ip6plus.png"
//#define INNER_TAB_5_SELECTED    @"profile_ip6plus.png"
//
//
//#elif iPhone6
//
//
//#define INNER_TAB_1_SELECTED    @"myshow_ip6.png"
//#define INNER_TAB_2_SELECTED    @"discover_ip6.png"
//#define INNER_TAB_3_SELECTED    @"Mint_ip6.png"
//#define INNER_TAB_4_SELECTED    @"activity_ip6.png"
//#define INNER_TAB_5_SELECTED    @"profile_ip6.png"
//
//#el
//
//
//#define INNER_TAB_1_SELECTED    @"myshow.png"
//#define INNER_TAB_2_SELECTED    @"discover.png"
//#define INNER_TAB_3_SELECTED    @"Mint.png"
//#define INNER_TAB_4_SELECTED    @"activity.png"
//#define INNER_TAB_5_SELECTED    @"profile.png"
//
//
//
//#endif


#define kVALIDUSERNAME @"Name length should be between 4-75 characters"
#define kVALIDEMAILID @"Please add a valid email id"
#define kVALIDPASSWORDLENGTH @"Password length should be between 6-16 characters"
#define kVALIDMOBILENUMBER @"Please add a valid 10 digit mobile number"
#define kVALIDPINCODE @"Please add a valid 6 digit pincode"
#define kAGREETERMS @"Please agree to the terms & conditions to proceed"
#define kREGISTEREDEMAIL @"Please enter the registered email id and password"


#define kAUTHKEY @"authkeyggh"
#define kUSERID  @"User_idSave"
#define kUSEREMAIL  @"User_email"



#define iPhone6     ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(750, 1334), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define iPhone4 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)
#define showAlert(title,content,ok,adelegate) [[[UIAlertView alloc] initWithTitle:title message:content delegate:adelegate cancelButtonTitle:ok otherButtonTitles:nil] show];



#define TILLDCOLOR [UIColor colorWithRed:25.0/255.0 green:196.0/255.0  blue:167.0/255.0  alpha:1.0]


#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define DummyAccessToken           @"3386055a355736e1c475e3f3bdeb3263bb5c6b48" // staging

#define ServerWebUrl @"http://stage.youareaceo.com"
#define ServerImageUrl @"http://sapi.youareaceo.com"
#define ServerUrl @"http://fitpass.co.in/api"
#define  VIDEO_DIRECT_URL  @"http://0e4d430fb4098f4615fe-9aedea8abd2894134979c98a7fc0997b.r67.cf1.rackcdn.com"

#define FACEBOOK_EXISTINGUSER_CHECK        @"facebookfriends/fb_google_check_exist_user"
#define  API_RESERVEWORKOUT                   @"/workout/reserveworkout"
#define  API_CANCELWORKOUT                   @"/workout/cancelworkout"


#define  API_WORKOUTLIST                   @"/workout/workoutlist"
#define  API_STUDIOLIST                    @"/studio/studiolist"
#define  API_STUDIODETAILS                 @"/studio/studiodetails"
#define  API_WORKOUTDETAILS                @"/workout/workoutdetails"
#define  API_UPCOMINGWORKOUT               @"/workout/upcomingworkout"
#define  API_PASTWORKOUT                   @"/workout/pastworkout"
#define  API_STUDIOS                       @"/workout/studios"

#define  API_GETUSERPROFILE                @"/user/getuserprofile"
#define  API_UPDATEUSERPROFILE             @"/user/updateuserprofile"
#define  API_UPLOADPHOTO                   @"/user/uploadPhoto"

#define  API_MYSHOW_SHOWROOM               @"/customshowroom/get_all_showrooms_list"
#define  API_ADDORUPDATESHOWROOM           @"/customshowroom/save_update_showroom_details"
#define  API_GETWORKOUT_ACTIVITY           @"/workout/activity"
#define  API_GETWORKOUT_ACTIVITY           @"/workout/activity"
#define  API_GETWORKOUT_ACTIVITY           @"/workout/activity"
#define API_LOGIN                           @"/user/login"
#define API_SIGNUP                          @"/user/registration"
#define  API_GETFAVORITESSTUDEOLIST         @"/user/favouritesstudiolist"
#define  API_GETSTUDIOSCHEDILE              @"/studio/getStudioschedule"
#define  API_FAVOURITESSTUDIO               @"/studio/addfavouritesstudio"

#define  API_USERAUTHENTICATION             @"/user/authentication"
#define  API_CHECKUSER                      @"/user/checkuserbuyfitpass"
#define  API_PURCHASENOW                      @"/user/buyFitpass"
#define  API_USERPASTEXP                      @"/workout/userPastExp"



#define API_LOGIN    @"/user/login"
#define API_SIGNUP    @"/user/registration"
#define  API_FORGETPASSWORD               @"/user/forgetpassword"
#define API_SIGNUPLOGINFB    @"/user/loginwithsocialmedia"

#define  API_GETFAVORITESSTUDEOLIST        @"/user/favouritesstudiolist"
//**************************************************************************************
//               BELOW ARE THE METHODS
//**************************************************************************************
#define METHOD_WORKOUTLIST               @"Workoutlist"
#define METHOD_STUDIOLIST                @"Studiolist"
#define METHOD_STUDIODETAILS             @"StudioDetails"
#define METHOD_WORKOUTDETAILS            @"WorkoutDetails"
#define METHOD_GETFAVORITESSTUDEOLIST    @"favouritesstudiolistdss"

#define METHOD_MYSHOW_SHOWROOM            @"MyShowShowRoom"
#define METHOD_ADDORUPDATESHOWROOM        @"addUpdateShowroom"
#define METHOD_GETWORKOUT_ACTIVITY        @"WorkoutActivity"
#define METHOD_GETWORKOUT_NEARLOCATION    @"NearbyLocation"
#define METHOD_GETSTUDIOS    @"Studios"
#define METHOD_PURCHASENOW    @"purchaseNow"

#define METHOD_UPCOMINGWORKOUT            @"upcomingworkout"
#define METHOD_PASTWORKOUT                @"pastworkout"

#define METHOD_SIGNUP                       @"signUp"
#define METHOD_LOGIN                        @"Login"
#define METHOD_STUDIOSCHEDULES              @"getStudioschedule"
#define METHOD_FAVOURITESSTUDIO             @"addfavouritesstudio"
#define METHOD_RESERVEWORKOUT               @"reserveworkout"
#define METHOD_CANCELWORKOUT                @"cancelworkout"
#define METHOD_SIGNUP @"signUp"
#define METHOD_LOGIN @"Login"
#define METHOD_SIGNUPLOGINFB @"signUpLoginFb"
#define METHOD_USERPASTEXP @"userPastExperience"

#define METHOD_FORGETPASSWORD             @"forgetpassword"
#define METHOD_GETUSERPROFILE             @"getuserprofile"
#define METHOD_UPDATEUSERPROFILE          @"updateuserprofile"
#define METHOD_UPLOADPHOTO                @"uploadPhoto"

#define  METHOD_USERAUTHENTICATION          @"authentication"
#define  METHOD_CHECKUSER                   @"checkuserbuyfitpass"
#define  FBUSERIMAGE                        @"userImage"


//**************************************************************************************
//               BELOW ARE THE CONSTANTS
//**************************************************************************************

#define ACTIVITYNAME                  @"activityName"
#define LOCATIONNAME                  @"locationName"
#define LOCATIONID                  @"locationId"
#define ACTIVITYLIST                  @"getactivitylist"
#define NEARESTLIST                  @"getnearestlist"

#define DEVICE_TOKEN                  @"deviceToken"
#define USERNAME                  @"name"
#define USERMOBILE                  @"mobileno"

#define CEO_VersionApp                  @"appVersion"
#define CEO_DeviceAccessToken           @"DeviceToken"
#define CEO_AccessToken                 @"accessToken"
#define CEO_LOGINSUCSSES                @"LoginSucsses"
#define CEO_FacebookAccessToken         @"FBAcessToken"
#define CEO_FacebookId                  @"FacebookId"
#define CEO_DeviceToken                 @"DeviceToken"
#define CEO_UserImage                   @"user_image_url"
#define CEO_UserName                    @"user_name"
#define CEO_UserId                      @"user_id"
#define CEO_TempUserId                  @"Temp_user_id"
#define CEO_UserFirstName               @"first_name"
#define CEO_UserLastName                @"last_name"
#define CEO_UserLocation                @"userLocName"
#define CEO_FollowerCount               @"follower_list_count"
#define CEO_FollowingCount              @"following_list_count"

#define CEO_ReferalID                   @"referalID"
#define CONSTANT_RESULT                 @"result"
#define CEO_MintType_Image              @"mintTypeImage"
#define CEO_MintType_Video              @"mintTypeVideo"
#define CEO_Gender                      @"gender"

#define CEOMintPlacholderText           @"Add an Image or Video"
#define CEOEncourageMintPlacholderText  @"Description..."
#define METHOD_RECEIVE_POWER_STATUS     @"RECIEVEPOWERSTREAK"


// first_name
//**************************************************************************************
//               BELOW ARE THE NSUSERDEFAULTS FUNCTIONS
//**************************************************************************************

#define USERDEFAULTS                [NSUserDefaults standardUserDefaults]
#define SYNCHRONISE                 [[NSUserDefaults standardUserDefaults] synchronize]
#define GETVALUE(keyname)  [[NSUserDefaults standardUserDefaults] valueForKey:keyname]
#define SETVALUE(value,keyname)  [[NSUserDefaults standardUserDefaults] setValue:value forKey:keyname]

//**************************************************************************************




#define ImagePostURL @"%@/achieveimage/ajax_image.php"
#define ImageFilePath @"Content-Disposition: form-data; name=\"images\"; filename=\"achievementPic.jpg\"\r\n"
#define VideoPostURL @"%@/achievevideo/ajax_video.php"
#define VideoFilePath @"Content-Disposition: form-data; name=\"file\"; filename=\"somefile.mov\"\r\n"



#pragma mark --- NSUserDefault ---

#define SYNCHRONIZE                 [[NSUserDefaults standardUserDefaults]synchronize]
#define FITPASS_USER_ID             [[NSUserDefaults standardUserDefaults]objectForKey:@"Userid"]
#define AUTHTENTICATION             [[NSUserDefaults standardUserDefaults]objectForKey:@"Authtentication"]

