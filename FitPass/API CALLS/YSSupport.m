//
//  YACCSupport.m
//  autorotationfix
//
//  Created by JITEN on 11/3/14.
//  Copyright (c) 2014 DiSalvo Technologies, LLC. All rights reserved.
//

#import "YSSupport.h"
#import "Reachability.h"


@implementation YSSupport


#pragma mark - Support
//for prevoious pattern
+ (UIImage *) getScreenShot : (UIView *) view{
    UIGraphicsBeginImageContext(view.frame.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenImage;
}



+ (UIImage *)imageRotatedByDegrees:(UIImage*)oldImage deg:(CGFloat)degrees{
    
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,oldImage.size.width, oldImage.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(degrees * M_PI / 180);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    //Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    //Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //Rotate the image context
    CGContextRotateCTM(bitmap, (degrees * M_PI / 180));
    
    //Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-oldImage.size.width / 2, -oldImage.size.height / 2, oldImage.size.width, oldImage.size.height), [oldImage CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


#pragma mark - Web Requests
//check network reachibility
+ (BOOL) isNetworkRechable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atError", nil) message:NSLocalizedString(@"amNetworkError", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"abOk", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    return !(networkStatus == NotReachable);
}

//get structured array

+ (NSMutableArray  *)getStacturedArrayForDisplayingUserFromArray:(NSArray *)array withNoOfImageBunch:(int)NoOfImageBunch

{
    NSInteger startIndex  =   0;
    NSInteger length      =   0;
    NSLog(@"%@", array);
    NSMutableArray *finalArray = [NSMutableArray arrayWithCapacity:0];
    
    if ([array count] > 0)
    {
        NSMutableArray *arrTemp = [NSMutableArray arrayWithCapacity:0];
        if (array.count <= NoOfImageBunch)
        {
            for (id object in array)
            {
                [arrTemp addObject:object];
            }
            [finalArray addObject:arrTemp];
        }
        else
        {
            for (int i = 0; i < (array.count / NoOfImageBunch); i++)
            {
                NSRange rangeForArrayDta = NSMakeRange( startIndex, NoOfImageBunch);
                NSArray *itemsForView = [array subarrayWithRange: rangeForArrayDta];
                [finalArray addObject:[itemsForView mutableCopy]];
                startIndex += NoOfImageBunch;
            }
            if (array.count > startIndex)
            {
                length = array.count - startIndex;
                NSRange rangeForArrayDta = NSMakeRange( startIndex, length);
                NSArray *itemsForView = [array subarrayWithRange: rangeForArrayDta];
                [finalArray addObject:[itemsForView mutableCopy]];
            }
        }
    }
    return finalArray;
}

+ (BOOL) validateEmail: (NSString *) candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:candidate];
}

+ (void) runSpinAnimationOnView:(UIView*)view
                       duration:(CGFloat)duration
                      rotations:(CGFloat)rotations
                         repeat:(float)repeat;
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}

//[self performSelector:@selector(fooFirstInput:)
//withObject:arrayOfThingsIWantToPassAlong
//afterDelay:15.0];

+ (void)runDelaySpinAnimationOnView:(UIView*)view
                       duration:(CGFloat)duration
                      rotations:(CGFloat)rotations
                         repeat:(float)repeat;
{
   
    NSDictionary *dictTopass=[NSDictionary dictionaryWithObjectsAndKeys:view,@"ViewtoAnimate",rotations,@"rotations",duration,@"duration",repeat,@"repeat",nil];
    
    [self performSelector:@selector(DelayrunSpinAnimationOnView:)
               withObject:dictTopass
               afterDelay:0.5];
}


+ (void)DelayrunSpinAnimationOnView:(NSDictionary*)Objects

{
    UIView *requiredView    =(UIView*)[Objects valueForKey:@"ViewtoAnimate"];
    CGFloat rotations       =[[Objects valueForKey:@"rotations"] floatValue];
    CGFloat duration        =[[Objects valueForKey:@"duration"] floatValue];
    float repeat            =[[Objects valueForKey:@"repeat"] floatValue];
    
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    [requiredView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}



+ (UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


+ (NSString *) getFormatedNumber : (NSString *) number
{
    
    NSNumberFormatter *indCurrencyFormatter = [[NSNumberFormatter alloc] init];
    [indCurrencyFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [indCurrencyFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_IN"]];
    NSString *formattedString =  [indCurrencyFormatter stringFromNumber:[NSNumber numberWithLongLong:[number longLongValue]]];
    return formattedString;
    
    
}
#pragma mark - Server PointSystem

+(NSMutableAttributedString *)getDollarScreen : (NSString *) final withStringToCheck:(NSString *) checkString
{
    
    
    // NSString *final = [NSString stringWithFormat:@"It will take $ 51,114 C-Bucks to pass Level 57."];
    
    NSTextAttachment *attachment = [[NSTextAttachment alloc] init];
    attachment.image             = [UIImage imageNamed:@"icon_cbucks_black_office"];
    NSAttributedString *attachmentString = [NSAttributedString attributedStringWithAttachment:attachment];
    
    
    NSString *rangeString = final;
    NSRange range1 ;
    range1 = [rangeString rangeOfString:checkString];
    NSMutableAttributedString *str = [[NSMutableAttributedString alloc] initWithString:final];
    [str replaceCharactersInRange:range1 withAttributedString:attachmentString];
    
    return str;
}
//==============================================================================

+(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark --
#pragma mark Get Mint Types


+(NSString *)getCurrentDate
{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    NSString *resultString = [dateFormatter stringFromDate: currentTime];
    return resultString;

}


+(NSString*)replaceColonAndlastComa:(NSString*)string1
{
    NSArray *arr1 = [[NSArray alloc]init];
    NSArray *arrname = [string1 componentsSeparatedByString:@":"];
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    NSLog(@"%@",arrname);
    for (int i=0;i<[arrname count];i++)
    {
        if (i == [arrname count]-1) {
            if ([[[arrname objectAtIndex:i] lowercaseString] containsString:@"and"])
            {
                [arr addObject:[NSString stringWithFormat:@"%@",[arrname objectAtIndex:i]]];
            }
            else
                [arr addObject:[NSString stringWithFormat:@" and%@",[arrname objectAtIndex:i]]];
        }
        else
            [arr addObject:[NSString stringWithFormat:@"%@",[arrname objectAtIndex:i]]];
    }
    arr1 = arr;
    NSMutableString *gy = [[NSMutableString alloc]init];
    for (int i=0;i<[arr1 count];i++)
    {
        if (i == [arr1 count])
        {
            if ([[[arr1 objectAtIndex:i] lowercaseString] containsString:@"and"])
            {
                [gy appendString:[NSString stringWithFormat:@"%@",[arr1 objectAtIndex:i]]];
            }
            else
                [gy appendString:[NSString stringWithFormat:@"and %@",[arr1 objectAtIndex:i]]];
        }
        else if(i == 0)
            [gy appendString:[arr1 objectAtIndex:i]];
        else
        {
            if ([[[arr1 objectAtIndex:i] lowercaseString] containsString:@","])
                [gy appendString:[NSString stringWithFormat:@"%@",[arr1 objectAtIndex:i]]];
            else
                [gy appendString:[NSString stringWithFormat:@",%@",[arr1 objectAtIndex:i]]];
        }
    }
    NSString *final = [gy stringByReplacingOccurrencesOfString:@", and" withString:@" and"];
    NSLog(@"%@",final);
    
    return final;
}


+(void)imageViewHide:(UIView*)sview
{
    for (UIView *view in sview.subviews)
    {
        if ([view isKindOfClass:[UIScrollView class]])
        {
            for (UIView *view1 in view.subviews)
            {
                if ([view1 isKindOfClass:[UILabel class]])
                {
                    for (UIView *view2 in view1.subviews)
                    {
                        if ([view2 isKindOfClass:[UIButton class]])
                        {
                            for (UIView *view3 in view2.subviews)
                            {
                                if ([view3 isKindOfClass:[UIImageView class]])
                                {
                                    view3.alpha = 0;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

+(void)showImage:(UIButton *)button
{
    for (UIView *view in button.subviews)
    {
        if ([view isKindOfClass:[UIImageView class]])
        {
            view.alpha = 1;
        }
    }
    
}

+(void)webView:(UIWebView*)view seturl:(NSURL*)url
{
    if (!([[NSString stringWithFormat:@"%@",url] rangeOfString:@"ceomall"].location == NSNotFound))
    {
        view.scalesPageToFit=YES;
    }
    
    view.alpha=1.0;
    view.scrollView.bounces=NO;
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [view loadRequest:requestObj];
    });
    
}
+(void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        NSString *keyName = (NSString *)key;
        if ([keyName isEqualToString:ACTIVITYNAME] || [keyName isEqualToString:LOCATIONNAME]  || [keyName isEqualToString:LOCATIONID]|| [keyName isEqualToString:ACTIVITYLIST] || [keyName isEqualToString:NEARESTLIST] || [keyName isEqualToString:@"locationcheck"] || [keyName isEqualToString:@"carparking"] || [keyName isEqualToString:@"airconditioner"]) {
            [defs removeObjectForKey:key];
            
        }
        
    }
    [defs synchronize];
}

+(void)resetDefaultsLogout {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
    
        if (![(NSString *)key isEqualToString:DEVICE_TOKEN])
            {
            [defs removeObjectForKey:key];
            }
            
        
        
    }
    [defs synchronize];
}
#pragma mark - ******* Validate Pincode *******
+(BOOL)isValidPinCode:(NSString*)pincode    {
    NSString *pinRegex = @"^[0-9]{6}$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:pincode];
    return pinValidates;
}


@end
