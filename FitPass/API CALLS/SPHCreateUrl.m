

//==============================================================================
///////////    THIS CLASS CREATES ALL URL             //////////////////////////
//==============================================================================



#import "SPHCreateUrl.h"
#import "Keys.h"

@implementation SPHCreateUrl

@synthesize
url = _url,
methodName=_methodName,
params = _params,
error = _error;




//==============================================================================
///////////    THIS BELOW METHOD SEPARATES ALL METHODS  ////////////////////////
//==============================================================================


 // Single Pameters
+ (NSString*)SerializeURLWithSingleParam:(NSString *)parameter
                              methodName:(NSString *)method_Name
{
    
    NSLog(@"Create URL Method Name : %@   AND Passed parameter = %@",method_Name,parameter);
    
    if ([method_Name isEqualToString:METHOD_WORKOUTLIST])
        return [self serializeURLForWorkoutList:parameter];
    return nil;
}

+ (NSString*)SerializeURL:(NSDictionary *)params  methodName:(NSString *)method_Name
{
    NSLog(@"Create URL Method Name : %@   AND Passed params = %@",method_Name,params);
    if ([method_Name isEqualToString:METHOD_WORKOUTLIST])
        return [self serializeURLForWorkOutList:params];
    if ([method_Name isEqualToString:METHOD_STUDIOLIST])
        return [self serializeURLForStudioList:params];
    if ([method_Name isEqualToString:METHOD_STUDIODETAILS])
        return [self serializeURLForStudioDetails:params];
    if ([method_Name isEqualToString:METHOD_WORKOUTDETAILS])
        return [self serializeURLForWorkoutDetails:params];
    if ([method_Name isEqualToString:METHOD_RESERVEWORKOUT])
        return [self serializeURLForReserve:params];
    if ([method_Name isEqualToString:METHOD_CANCELWORKOUT])
        return [self serializeURLForWorkoutDetails:params];
//page_number
    
    if ([method_Name isEqualToString:METHOD_SIGNUP])
        return [self serializeURLForSignUp:params];
    if ([method_Name isEqualToString:METHOD_LOGIN])
        return [self serializeURLForLogin:params];
    if ([method_Name isEqualToString:METHOD_FORGETPASSWORD])
        return [self serializeURLForForgetPassword:params];
    
    if ([method_Name isEqualToString:METHOD_RESERVEWORKOUT])
        return [self serializeURLForWorkoutDetails:params];
    if ([method_Name isEqualToString:METHOD_CANCELWORKOUT])
        return [self serializeURLForWorkoutDetails:params];
    if ([method_Name isEqualToString:METHOD_STUDIOSCHEDULES])
        return [self serializeURLForStudioSchedule:params];

    if ([method_Name isEqualToString:METHOD_GETUSERPROFILE])
        return [self serializeURLGetUserProfile:params];
    if ([method_Name isEqualToString:METHOD_UPDATEUSERPROFILE])
        return [self serializeURLUpdateUserProfile:params];
    if ([method_Name isEqualToString:METHOD_UPLOADPHOTO])
        return [self serializeURLUploadPhoto:params];
    
    if ([method_Name isEqualToString:@""])
        return [self serializeURLForLogin:params];
   
    return nil;
}


//==============================================================================
///////////    ALL  METHOD SEPARATED ABOVE            //////////////////////////
//==============================================================================
+ (NSString*)serializeURLForSingle:(NSString *)params
{
    return [NSString stringWithFormat:@"%@/users/get_login",@""];
}
+ (NSString*)serializeURLForWorkoutList:(NSString *)params
{
    return [NSString stringWithFormat:@"%@%@?user_id=%@",ServerUrl,API_WORKOUTLIST,params];
}
//==============================================================================



//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForMyShowShowroom:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"access_token=%@",params[@"access_token"]]];
    [pairs addObject:[NSString stringWithFormat:@"showroom_list_type=%@",params[@"showroom_list_type"]]];
    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_MYSHOW_SHOWROOM,query];
    
}


+ (NSString*)serializeURLForWorkOutList:(NSDictionary *)params
{
    
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    
    if ([[params  valueForKey:@"locality_id"] length])
        [pairs addObject:[NSString stringWithFormat:@"locality_id=%@",[self escapedValue:params[@"locality_id"]]]];
    else if ([[params  valueForKey:@"latitude"] length] && [[params  valueForKey:@"longitude"] length])
    {
        [pairs addObject:[NSString stringWithFormat:@"latitude=%@",params[@"latitude"]]];
        [pairs addObject:[NSString stringWithFormat:@"longitude=%@",params[@"longitude"]]];
    }
    
    if ([[params  valueForKey:@"date_of_workout"] length])
        [pairs addObject:[NSString stringWithFormat:@"date_of_workout=%@",params[@"date_of_workout"]]];
    
    if ([[params  valueForKey:@"parking_facility"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"parking_facility=%@",params[@"parking_facility"]]];
    if ([[params  valueForKey:@"air_conditioner"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"air_conditioner=%@",params[@"air_conditioner"]]];
    if ([[params  valueForKey:@"start_time"] length])
        [pairs addObject:[NSString stringWithFormat:@"start_time=%@",params[@"start_time"]]];
    
    if ([[params  valueForKey:@"end_time"] length])
        [pairs addObject:[NSString stringWithFormat:@"end_time=%@",params[@"end_time"]]];

    if ([[params  valueForKey:@"work_out_name"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"work_out_name=%@",[self escapedValue:params[@"work_out_name"]]]];
    if ([[params  valueForKey:@"view_studio_workouts"] length])
        [pairs addObject:[NSString stringWithFormat:@"view_studio_workouts=%@",params[@"view_studio_workouts"]]];
    
    if ([[params  valueForKey:@"studio_id"] length])
        [pairs addObject:[NSString stringWithFormat:@"studio_id=%@",params[@"studio_id"]]];
    
    if ([[params  valueForKey:@"page_number"] length])
        [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    
    //
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_WORKOUTLIST,query];
    
}


//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForStudioList:(NSDictionary *)params
{
    //  NSDictionary *paramDict=[NSDictionary dictionaryWithObjectsAndKeys:@"2077",studio_id,@"user_id",@"studio_id",nil];
    // user_id=1951&studio_id=189&locality_id=991&work_out_name=abc&&parking_facility=1&air_conditioner=1

    
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    if ([[params  valueForKey:@"studio_id"] length])
        [pairs addObject:[NSString stringWithFormat:@"studio_id=%@",params[@"studio_id"]]];
    
    if ([[params  valueForKey:@"locality_id"] length])
        [pairs addObject:[NSString stringWithFormat:@"locality_id=%@",[self escapedValue:params[@"locality_id"]]]];
    else if ([[params  valueForKey:@"latitude"] length] && [[params  valueForKey:@"longitude"] length])
    {
        [pairs addObject:[NSString stringWithFormat:@"latitude=%@",params[@"latitude"]]];
        [pairs addObject:[NSString stringWithFormat:@"longitude=%@",params[@"longitude"]]];
    }
    
    
    if ([[params  valueForKey:@"date_of_workout"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"date_of_workout=%@",params[@"date_of_workout"]]];
    if ([[params  valueForKey:@"parking_facility"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"parking_facility=%@",params[@"parking_facility"]]];
    if ([[params  valueForKey:@"air_conditioner"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"air_conditioner=%@",params[@"air_conditioner"]]];
    
    if ([[params  valueForKey:@"start_time"] length])
      [pairs addObject:[NSString stringWithFormat:@"start_time=%@",params[@"start_time"]]];
    
    if ([[params  valueForKey:@"end_time"] length])
        [pairs addObject:[NSString stringWithFormat:@"end_time=%@",params[@"end_time"]]];
    
    if ([[params  valueForKey:@"work_out_name"] length])
        
        [pairs addObject:[NSString stringWithFormat:@"work_out_name=%@",[self escapedValue:params[@"work_out_name"]]]];
    if ([[params  valueForKey:@"page_number"] length])
        [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];

    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_STUDIOLIST,query];
    
}
//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForStudioDetails:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
      [pairs addObject:[NSString stringWithFormat:@"studio_id=%@",params[@"studio_id"]]];
    
    //    [pairs addObject:[NSString stringWithFormat:@"page_number=%@",params[@"page_number"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_STUDIODETAILS,query];
    
}


+ (NSString*)serializeURLForStudioSchedule:(NSDictionary *)params
{
    
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"studio_id=%@",params[@"studio_id"]]];
    
    if ([[params  valueForKey:@"date_of_workout"] length])
        [pairs addObject:[NSString stringWithFormat:@"date_of_workout=%@",params[@"date_of_workout"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETSTUDIOSCHEDILE,query];
}

//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForReserve:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"location_id=%@",params[@"location_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"batch_id=%@",params[@"batch_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"schedule_date=%@",params[@"schedule_date"]]];
    [pairs addObject:[NSString stringWithFormat:@"studio_id=%@",params[@"studio_id"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_RESERVEWORKOUT,query];
}
//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForCancel:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"schedule_id=%@",params[@"location_id"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_CANCELWORKOUT,query];
}

//dictionaryWithObjectsAndKeys:DummyAccessToken,@"access_token",@"0",@"showroom_list_type",@"0",@"page_number", nil];
+ (NSString*)serializeURLForWorkoutDetails:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"work_out_id=%@",params[@"work_out_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"batch_id=%@",params[@"batch_id"]]];

    if ([[params  valueForKey:@"date_of_workout"] length])
        [pairs addObject:[NSString stringWithFormat:@"date_of_workout=%@",params[@"date_of_workout"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_WORKOUTDETAILS,query];
}

//==============================================================================

+ (NSString*)escapedValue:(NSString *)originalValue
{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes( NULL,(CFStringRef)originalValue,NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    return escaped_value;
}

+ (NSString*)serializeURLForSignUp:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"first_name=%@",params[@"first_name"]]];
    [pairs addObject:[NSString stringWithFormat:@"email_address=%@",params[@"email_address"]]];
    [pairs addObject:[NSString stringWithFormat:@"password=%@",params[@"password"]]];
    [pairs addObject:[NSString stringWithFormat:@"mobile_no=%@",params[@"mobile_no"]]];
    [pairs addObject:[NSString stringWithFormat:@"gender=%@",params[@"gender"]]];
    [pairs addObject:[NSString stringWithFormat:@"pin_code=%@",params[@"pin_code"]]];
    [pairs addObject:[NSString stringWithFormat:@"device_id=%@",params[@"device_id"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_SIGNUP,query];
}

//email_address=jnanendra.sovy@gmail.com&password=password
+ (NSString*)serializeURLForLogin:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"email_address=%@",params[@"email_address"]]];
    [pairs addObject:[NSString stringWithFormat:@"password=%@",params[@"password"]]];
    [pairs addObject:[NSString stringWithFormat:@"device_id=%@",params[@"device_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"device_type=%@",params[@"device_type"]]];
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_LOGIN,query];
}

+ (NSString*)serializeURLForForgetPassword:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"email_address=%@",params[@"email_address"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_FORGETPASSWORD,query];
}


+ (NSString*)serializeURLGetUserProfile:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_GETUSERPROFILE,query];
}

+ (NSString*)serializeURLUpdateUserProfile:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"first_name=%@",params[@"first_name"]]];
    [pairs addObject:[NSString stringWithFormat:@"mobile_no=%@",params[@"mobile_no"]]];
    [pairs addObject:[NSString stringWithFormat:@"gender=%@",params[@"gender"]]];
    [pairs addObject:[NSString stringWithFormat:@"address_line1=%@",params[@"address_line1"]]];
    [pairs addObject:[NSString stringWithFormat:@"address_line2=%@",params[@"address_line2"]]];
    [pairs addObject:[NSString stringWithFormat:@"city=%@",params[@"city"]]];
    [pairs addObject:[NSString stringWithFormat:@"state_name=%@",params[@"state_name"]]];
    [pairs addObject:[NSString stringWithFormat:@"pin_code=%@",params[@"pin_code"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_UPDATEUSERPROFILE,query];
}

+ (NSString*)serializeURLUploadPhoto:(NSDictionary *)params
{
    NSMutableArray* pairs = [NSMutableArray array];
    [pairs addObject:[NSString stringWithFormat:@"user_id=%@",params[@"user_id"]]];
    [pairs addObject:[NSString stringWithFormat:@"profile_pic=%@",params[@"profile_pic"]]];
    
    NSString* query = [pairs componentsJoinedByString:@"&"];
    return [NSString stringWithFormat:@"%@%@?%@",ServerUrl,API_UPLOADPHOTO,query];
}

@end







