//
//  WorkOutModel.h
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PastWorkOutModel : NSObject

@property(nonatomic,strong)     NSArray *arrData;

@property(nonatomic,strong)     NSString  *batch_id;
@property(nonatomic,strong)     NSString  *batch_status;
@property(nonatomic,strong)     NSString  *end_time;
@property(nonatomic,strong)     NSString  *duration;

@property(nonatomic,strong)     NSString  *locality_id;
@property(nonatomic,strong)     NSString  *start_time;
@property(nonatomic,strong)     NSString  *studio_id;
@property(nonatomic,strong)     NSString  *studio_name;
@property(nonatomic,strong)     NSString  *work_out_id;
@property(nonatomic,strong)     NSString  *work_out,*address_line1,*address_line2,*schedule_date,*parking_facility,*pin_code,*venue_name,*schedule_Id,*locality_name;

@property(nonatomic,strong)     NSString  *locationame;
@property(nonatomic,strong)     NSString  *work_out_name;

-(PastWorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;

@end


