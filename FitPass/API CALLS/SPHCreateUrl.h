

//==============================================================================
///////////    THIS CLASS CREATES ALL URL             //////////////////////////
//==============================================================================


#import <Foundation/Foundation.h>



@interface SPHCreateUrl : NSObject


@property(nonatomic,retain) NSError* error;
@property(nonatomic,copy) NSString* url;
@property(nonatomic,copy) NSString* methodName;
@property(nonatomic,retain) NSMutableDictionary* params;


+ (NSString*)SerializeURL:(NSDictionary *)params
               methodName:(NSString *)method_Name;

+ (NSString*)SerializeURLWithSingleParam:(NSString *)parameter
                              methodName:(NSString *)method_Name;


@end



