//
//  WorkOutModel.h
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UpComingWorkOutModel : NSObject

@property(nonatomic,strong)     NSDictionary  *dictUpcomingData;
@property(nonatomic,strong)     NSString  *batch_id;
@property(nonatomic,strong)     NSString  *batch_status;
@property(nonatomic,strong)     NSString  *duration;
@property(nonatomic,strong)     NSString  *end_time;

@property(nonatomic,strong)     NSString  *pin_code;
@property(nonatomic,strong)     NSString  *schedule_date;
@property(nonatomic,strong)     NSString  *schedule_date_original;
@property(nonatomic,strong)     NSString  *schedule_id;
@property(nonatomic,strong)     NSString  *start_time;
@property(nonatomic,strong)     NSString  *studio_name;
@property(nonatomic,strong)     NSString  *date_of_workout;

@property(nonatomic,strong)     NSString  *workout_id;
@property(nonatomic,strong)     NSString  *user_id;
@property(nonatomic,strong)     NSString  *venue_name,*work_out,*address_line1,*address_line2,*locality_name,*studio_id;

-(UpComingWorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;

@end

