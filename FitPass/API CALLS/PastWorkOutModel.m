//
//  WorkOutModel.m
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "PastWorkOutModel.h"
#import "NSNull+JSON.h"

@implementation PastWorkOutModel
-(PastWorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    
    self.studio_name            = [showRoomDict valueForKey:@"studio_name"];
    self.studio_id              = [showRoomDict  valueForKey:@"studio_id"];
    self.work_out               = [showRoomDict valueForKey:@"work_out"];
    self.schedule_date          = [showRoomDict valueForKey:@"schedule_date"];
    self.start_time             = [showRoomDict valueForKey:@"start_time"];
    self.end_time               = [showRoomDict valueForKey:@"end_time"];
    
    NSLog(@"Date work %@",[showRoomDict valueForKey:@"schedule_date"]);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:[showRoomDict valueForKey:@"schedule_date"]];
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    
    NSLog(@"Date work %@",finalString);
    
    self.schedule_date          = finalString;
    
    self.duration               = [showRoomDict valueForKey:@"duration"];
    self.locality_name          = [showRoomDict valueForKey:@"locality_name"];
    self.venue_name             = [showRoomDict valueForKey:@"venue_name"];
    self.address_line1          = [showRoomDict valueForKey:@"address_line1"];
    self.address_line2          = [showRoomDict valueForKey:@"address_line2"];
    self.locality_id            = [showRoomDict valueForKey:@"locality_id"];
    self.pin_code               = [showRoomDict valueForKey:@"pin_code"];
    
    
    _locationame  = [showRoomDict valueForKey:@"location_name"];
    _work_out_name= [showRoomDict valueForKey:@"work_out_name"];
    dateFormatter = nil;
    orignalDate = nil;
    return self;
}

//"batch_id" = 142;
//"batch_status" = 1;
//duration = "60 min";
//"end_time" = "07:00";
//"location_id" = 128;
//"schedule_date" = "2015-08-06";
//"start_time" = "06:00";
//"studio_id" = 1844;
//"work_out_id" = 149;
//"work_out_name" = Yoga;


@end



