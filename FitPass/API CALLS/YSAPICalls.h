//
//  YSAPICalls.h
//  Your Show
//
//  Created by Siba Prasad Hota on 29/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Generalmodel.h"


@interface YSAPICalls : NSObject

//Checking for whiteSpace
+(BOOL)CheckForWhiteSpace : (NSString *) string;
+ (NSString*)escapedValue:(NSString *)originalValue;


+ (BOOL) isNetworkRechable;



+(void)getWorkoutDetailswithParamDict:(NSDictionary*)paramDict
                     SuccessionBlock:(void(^)(id newResponse))successBlock
                     andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getAllWorkOutListwithDateOfWork:(NSDictionary*)paramDict
                       SuccessionBlock:(void(^)(id newResponse))successBlock
                       andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getAllStudioListwithParamDict:(NSDictionary*)paramDict
                       SuccessionBlock:(void(^)(id newResponse))successBlock
                       andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getStudioDetailsWithStudioID:(NSString*)studio_id
                    SuccessionBlock:(void(^)(id newResponse))successBlock
                    andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)getWorkOutActivitywithSuccessionBlock:(void(^)(id newResponse))successBlock
                     andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getWorkOutNearLocationwithSuccessionBlock:(void(^)(id newResponse))successBlock
                             andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)getLoginDetailsParamData:(NSData*)paramDict
                SuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getSignUpDetailsParamData:(NSData*)paramDict
                 SuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getForgetPasswordDetailsParamData:(NSData*)paramDict
                         SuccessionBlock:(void(^)(id newResponse))successBlock
                         andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getfavotarestudeidetailswithSuccessionBlock:(void(^)(id newResponse))successBlock
                                   andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getUpcomingWorkoutDetailsParamData:(NSData*)paramDict
                          SuccessionBlock:(void(^)(id newResponse))successBlock
                          andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getPastWorkoutDetailsParamData:(NSData*)paramDict
                      SuccessionBlock:(void(^)(id newResponse))successBlock
                      andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)getStudiosSuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)getStudioscheduleDetails:(NSDictionary *)schedulDic
                SuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getaddfavouritesstudioDetailsParamData:(NSData*)paramDict
                          SuccessionBlock:(void(^)(id newResponse))successBlock
                          andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getUserProfileWithData:(NSData*)paramData
              SuccessionBlock:(void(^)(id newResponse))successBlock
              andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)uploadUserPhotoWithData:(NSData*)paramData
               SuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)updateUserProfileWithData:(NSData*)paramData
                 SuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock;


+(void)buyFitpass:(NSData*)paramData
      SuccessionBlock:(void(^)(id newResponse))successBlock
      andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)authentication:(NSData*)paramData
               SuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)checkuserbuyfitpass:(NSData*)paramData
               SuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)userpastExperience:(NSData*)paramData
           SuccessionBlock:(void(^)(id newResponse))successBlock
           andFailureBlock:(void(^)(NSError *error))failureBlock;

+(void)getSignUpSignInDetailsFBParamData:(NSData*)paramDict
                         SuccessionBlock:(void(^)(id newResponse))successBlock
                         andFailureBlock:(void(^)(NSError *error))failureBlock;
- (void)PostRequestWithProfilePhotoUrlAndData:(NSData *)imgData
                                    paramDict:(NSDictionary*)params
                                       mathod:(NSString*)method
                                         link:(NSURL*)url
                          withSuccessionBlock:(void(^)(id response))successBlock
                              andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)reserveworkout:(NSData*)paramData
      SuccessionBlock:(void(^)(id newResponse))successBlock
      andFailureBlock:(void(^)(NSError *error))failureBlock;
+(void)cancelworkout:(NSData*)paramData
     SuccessionBlock:(void(^)(id newResponse))successBlock
     andFailureBlock:(void(^)(NSError *error))failureBlock;
@end
