//
//  YSAPICalls.m
//  Your Show
//
//  Created by Siba Prasad Hota on 29/07/15.
//  Copyright (c) 2015 Siba Prasad Hota. All rights reserved.
//

#import "YSAPICalls.h"
#import "Reachability.h"
#import "SPHServiceRequest.h"
#import "Generalmodel.h"
#import "Keys.h"


@implementation YSAPICalls

+ (BOOL) isNetworkRechable {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atError", nil) message:NSLocalizedString(@"amNetworkError", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"abOk", nil) otherButtonTitles:nil, nil];
//        [alert show];
    }
    return !(networkStatus == NotReachable);
}


+(BOOL)CheckForWhiteSpace : (NSString *) string
{
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [string stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0) {
        // Text was empty or only whitespace.
        return YES;
    }  else
        return NO;
}

+(void)getAllWorkOutListwithDateOfWork:(NSDictionary*)paramDict
                       SuccessionBlock:(void(^)(id newResponse))successBlock
                       andFailureBlock:(void(^)(NSError *error))failureBlock;
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    [sreq getBlockServerResponseForparam:paramDict mathod:METHOD_WORKOUTLIST withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     
     {
         failureBlock(error);
     }];
}

+(void)getSignUpDetailsParamData:(NSData*)paramDict
                 SuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_SIGNUP link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_SIGNUP]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getSignUpSignInDetailsFBParamData:(NSData*)paramDict
                 SuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_SIGNUPLOGINFB link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_SIGNUPLOGINFB]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getLoginDetailsParamData:(NSData*)paramDict
                SuccessionBlock:(void(^)(id newResponse))successBlock
                andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_LOGIN link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_LOGIN]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getForgetPasswordDetailsParamData:(NSData*)paramDict
                         SuccessionBlock:(void(^)(id newResponse))successBlock
                         andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_FORGETPASSWORD link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_FORGETPASSWORD]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

 +(void)getfavotarestudeidetailswithSuccessionBlock:(void(^)(id newResponse))successBlock
andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETFAVORITESSTUDEOLIST link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_GETFAVORITESSTUDEOLIST]]
               withSuccessionBlock:^(id json)
     {
         successBlock (json);
     } andFailureBlock:^(NSError *error)
     {
     }];
}

+(void)getWorkoutDetailswithParamDict:(NSDictionary*)paramDict
                      SuccessionBlock:(void(^)(id newResponse))successBlock
                      andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    [sreq getBlockServerResponseForparam:paramDict mathod:METHOD_WORKOUTDETAILS withSuccessionBlock:^(id response)
     {
         successBlock(response);
     }andFailureBlock:^(NSError *error) {
         failureBlock(error);
         
     }];

}

+(void)getAllStudioListwithParamDict:(NSDictionary*)paramDict
                     SuccessionBlock:(void(^)(id newResponse))successBlock
                     andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    [sreq getBlockServerResponseForparam:paramDict mathod:METHOD_STUDIOLIST withSuccessionBlock:^(id response)
     
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getStudioDetailsWithStudioID:(NSString*)studio_id
                    SuccessionBlock:(void(^)(id newResponse))successBlock
                    andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    NSDictionary *paramDict=[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studio_id,@"studio_id",nil];
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    [sreq getBlockServerResponseForparam:paramDict mathod:METHOD_STUDIODETAILS withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}



+(void)getWorkOutActivitywithSuccessionBlock:(void(^)(id newResponse))successBlock
                             andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETWORKOUT_ACTIVITY link:[NSURL URLWithString:[NSString stringWithFormat:@"%@/workout/activity",ServerUrl]]
               withSuccessionBlock:^(id json)
     {
         
         
         successBlock (json);
     } andFailureBlock:^(NSError *error)
    {
    }];
}


+(void)getWorkOutNearLocationwithSuccessionBlock:(void(^)(id newResponse))successBlock
                                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETWORKOUT_NEARLOCATION link:[NSURL URLWithString:[NSString stringWithFormat:@"%@/location/localities",ServerUrl]] withSuccessionBlock:^(id json)
     {
         successBlock(json);
     }andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getStudiosSuccessionBlock:(void(^)(id newResponse))successBlock
                                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:submitData paramDict:nil mathod:METHOD_GETSTUDIOS link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_STUDIOS]] withSuccessionBlock:^(id json)
     {
         successBlock(json);
     }andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+ (NSString*)escapedValue:(NSString *)originalValue
{
    NSString* escaped_value = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)originalValue,NULL,(CFStringRef)@"!*'();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
    return escaped_value;
}


+(void)getUpcomingWorkoutDetailsParamData:(NSData*)paramDict
                          SuccessionBlock:(void(^)(id newResponse))successBlock
                          andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_UPCOMINGWORKOUT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_UPCOMINGWORKOUT]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}

+(void)getPastWorkoutDetailsParamData:(NSData*)paramDict
                      SuccessionBlock:(void(^)(id newResponse))successBlock
                      andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_PASTWORKOUT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_PASTWORKOUT]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}


+(void)getStudioscheduleDetails:(NSDictionary *)schedulDic
                    SuccessionBlock:(void(^)(id newResponse))successBlock
                    andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *sreq=[[SPHServiceRequest alloc]init];
    [sreq getBlockServerResponseForparam:schedulDic mathod:METHOD_STUDIOSCHEDULES withSuccessionBlock:^(id response)

    {
        successBlock(response);
    } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}



+(void)uploadUserPhotoWithData:(NSData*)paramData
               SuccessionBlock:(void(^)(id newResponse))successBlock
               andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithProfilePhotoUrlAndData:paramData paramDict:nil mathod:METHOD_UPLOADPHOTO link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_UPLOADPHOTO]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}
+(void)getUserProfileWithData:(NSData*)paramData
              SuccessionBlock:(void(^)(id newResponse))successBlock
              andFailureBlock:(void(^)(NSError *error))failureBlock;
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_GETUSERPROFILE link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_GETUSERPROFILE]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}


+(void)getaddfavouritesstudioDetailsParamData:(NSData*)paramDict
                              SuccessionBlock:(void(^)(id newResponse))successBlock
                              andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramDict paramDict:nil mathod:METHOD_FAVOURITESSTUDIO link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_FAVOURITESSTUDIO]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}




+(void)updateUserProfileWithData:(NSData*)paramData
                 SuccessionBlock:(void(^)(id newResponse))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock;
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_UPDATEUSERPROFILE link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_UPDATEUSERPROFILE]] withSuccessionBlock:^(id response)

     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];

    
}

+(void)authentication:(NSData*)paramData
      SuccessionBlock:(void(^)(id newResponse))successBlock
      andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_USERAUTHENTICATION link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_USERAUTHENTICATION]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];

}

+(void)checkuserbuyfitpass:(NSData*)paramData
           SuccessionBlock:(void(^)(id newResponse))successBlock
           andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_CHECKUSER link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_CHECKUSER]] withSuccessionBlock:^(id response)
     {
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];

}

+(void)buyFitpass:(NSData*)paramData
  SuccessionBlock:(void(^)(id newResponse))successBlock
  andFailureBlock:(void(^)(NSError *error))failureBlock
{


//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_PURCHASENOW link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_PURCHASENOW]] withSuccessionBlock:^(id response)
     {
         
         
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];







}

+(void)userpastExperience:(NSData*)paramData
          SuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock
{



//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_USERPASTEXP link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_USERPASTEXP]] withSuccessionBlock:^(id response)
     {
         
         
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
    
    




}

+(void)reserveworkout:(NSData*)paramData
          SuccessionBlock:(void(^)(id newResponse))successBlock
          andFailureBlock:(void(^)(NSError *error))failureBlock
{
    
    
    
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_RESERVEWORKOUT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_RESERVEWORKOUT]] withSuccessionBlock:^(id response)
     {
         
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
    
    
    
    
    
    
}


+(void)cancelworkout:(NSData*)paramData
      SuccessionBlock:(void(^)(id newResponse))successBlock
      andFailureBlock:(void(^)(NSError *error))failureBlock
{
//    if (![self isNetworkRechable])
//        return;
    
    SPHServiceRequest *req = [[SPHServiceRequest alloc]init];
    [req PostRequestWithUrlAndData:paramData paramDict:nil mathod:METHOD_CANCELWORKOUT link:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ServerUrl,API_CANCELWORKOUT]] withSuccessionBlock:^(id response)
     {
         
         
         successBlock(response);
     } andFailureBlock:^(NSError *error)
     {
         failureBlock(error);
     }];
}
@end
