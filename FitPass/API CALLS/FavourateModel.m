//
//  FavourateModel.m
//  FitPass
//
//  Created by BseTec on 8/5/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FavourateModel.h"

@implementation FavourateModel


-(FavourateModel *)initWithDic:(NSDictionary *)favourateDic
{
    _favourites_status  =  [favourateDic valueForKey:@"favourites_status"];
    _locality_name      =  [favourateDic valueForKey:@"locality_name"];
    _profile_pic        =  [favourateDic valueForKey:@"profile_pic"];
    _rating             =  [favourateDic valueForKey:@"rating"];
    _studio_id          =  [favourateDic valueForKey:@"studio_id"];
    _studio_name        =  [favourateDic valueForKey:@"studio_name"];
    
    return self;
}

@end
