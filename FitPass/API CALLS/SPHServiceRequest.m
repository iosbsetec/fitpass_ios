//
//  SPHServiceRequest.m
//  Exam_View
//
//  Created by Heart on 31/05/14.
//  Copyright (c) 2014 BSEtec. All rights reserved.
//

//==============================================================================
///////////    THIS CLASS CREATES ALL REQUESTS        //////////////////////////
//==============================================================================


#import "SPHServiceRequest.h"
#import "SPHCreateUrl.h"
#import "SPHSeparteParams.h"
#import "Keys.h"

@implementation SPHServiceRequest

#pragma single parameters

//==============================================================================
///////////    BELOW CODES FOR DIFFERENT REQUEST CREATION  /////////////////////
//==============================================================================


- (NSDictionary*)dictionaryFromResponseData:(NSData*)responseData{
    NSDictionary* dictionary = nil;
    if (responseData ) {
        id object = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingAllowFragments error:nil];
        if ([object isKindOfClass:[NSDictionary class]]){
            dictionary = (NSDictionary*)object;
        }
        else
        {
            dictionary = (object)?[NSDictionary dictionaryWithObject:object forKey:@"result"]:nil;
        }
    }
    
    NSLog(@"dict = %@",dictionary);
    return dictionary;
}



- (void)getBlockServerResponseForString:(NSString *)params mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSLog(@"%@",[NSURL URLWithString:[SPHCreateUrl SerializeURLWithSingleParam:params methodName:method]]);
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[SPHCreateUrl SerializeURLWithSingleParam:params methodName:method]]];
    [request setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [request setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}


#pragma Multiple Parameters

- (void)getBlockServerResponseForparam:(NSDictionary *)params mathod:(NSString*)method withSuccessionBlock:(void(^)(id response))successBlock
                       andFailureBlock:(void(^)(NSError *error))failureBlock;

{
    NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSLog(@"paramsssss =%@",params);
    NSLog(@"My Url =%@",myURL);
   
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:myURL];
    [request setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [request setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];

}




- (void)postBlockServerResponseForData :(NSData *)somedata
                              paramDict:(NSDictionary*)params
                                 mathod:(NSString*)method
                    withSuccessionBlock:(void(^)(id response))successBlock
                        andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:myURL];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setHTTPBody:somedata];
    [submitrequest setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [submitrequest setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [submitrequest setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];

    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
             
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}



- (void)PostRequestWithProfilePhotoUrlAndData:(NSData *)imgData
                                    paramDict:(NSDictionary*)params
                                       mathod:(NSString*)method
                                         link:(NSURL*)url
                          withSuccessionBlock:(void(^)(id response))successBlock
                              andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSLog(@"My Url =%@",url);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    
    [request setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [request setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"ebrj6b3qril766r";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"
                      dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[FITPASS_USER_ID  dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Disposition: form-data; name=\"profile_pic\"; filename=\"images.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imgData];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    //     {
    //         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    //         NSLog(@"dict = %@",newStr);
    //
    ////         [MBProgressHUD  hideHUDForView:self.view animated:YES];
    //     }];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"dict = %@",newStr);
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}

- (void)PostRequestWithUrlAndData:(NSData *)somedata
                        paramDict:(NSDictionary*)params
                           mathod:(NSString*)method
                             link:(NSURL*)url
              withSuccessionBlock:(void(^)(id response))successBlock
                  andFailureBlock:(void(^)(NSError *error))failureBlock
{
    NSLog(@"My Url =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:url];
    [submitrequest setHTTPMethod:@"POST"];
    [submitrequest setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [submitrequest setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [submitrequest setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];
    
    [submitrequest setHTTPBody:somedata];
    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];
}

- (void)GETRequestWithUrlAndData:(NSDictionary*)params
                          mathod:(NSString*)method
                            link:(NSString*)url
             withSuccessionBlock:(void(^)(id response))successBlock
                 andFailureBlock:(void(^)(NSError *error))failureBlock
{
    //NSURL *myURL = [NSURL URLWithString:[SPHCreateUrl SerializeURL:params methodName:method]];
    NSLog(@"paramsssss =%@",params);
    NSLog(@"My Url =%@",url);
    NSMutableURLRequest *submitrequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [submitrequest setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [submitrequest setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [submitrequest setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];

    [NSURLConnection sendAsynchronousRequest:submitrequest queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if(!data)
         {
             failureBlock(error);
         }
         else
         {
             successBlock([SPHSeparteParams SeparateParams:[self dictionaryFromResponseData:data] methodName:method]);
         }
     }];

    
}


@end
