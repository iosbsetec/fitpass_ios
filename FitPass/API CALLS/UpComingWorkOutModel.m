//
//  WorkOutModel.m
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "UpComingWorkOutModel.h"
#import "NSNull+JSON.h"

@implementation UpComingWorkOutModel
-(UpComingWorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    self.address_line1       = [showRoomDict valueForKey:@"address_line1"];
    self.date_of_workout       = [showRoomDict valueForKey:@"date_of_workout"];

    self.address_line2       = [showRoomDict  valueForKey:@"address_line2"];
    self.duration            = [showRoomDict  valueForKey:@"duration"];
    self.end_time            = [showRoomDict  valueForKey:@"end_time"];
    self.locality_name       = [showRoomDict  valueForKey:@"locality_name"];
    self.pin_code            = [showRoomDict  valueForKey:@"pin_code"];
    self.workout_id          =   [showRoomDict  valueForKey:@"work_out_id"];
    self.schedule_date_original = [showRoomDict valueForKey:@"schedule_date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:[showRoomDict valueForKey:@"schedule_date"]];
    
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    self.schedule_date          = finalString;
    
    self.schedule_id            =[showRoomDict  valueForKey:@"schedule_id"];
    self.start_time             = [showRoomDict  valueForKey:@"start_time"];
    self.studio_id              =[showRoomDict  valueForKey:@"studio_id"];
    self.studio_name            = [showRoomDict  valueForKey:@"studio_name"];
    self.user_id                =[showRoomDict  valueForKey:@"user_id"];
    self.venue_name             = [showRoomDict  valueForKey:@"venue_name"];
    self.work_out               = [showRoomDict  valueForKey:@"work_out"];
    self.batch_id               = [showRoomDict  valueForKey:@"batch_id"];

    dateFormatter = nil;
    orignalDate = nil;
    return self;
}

@end
