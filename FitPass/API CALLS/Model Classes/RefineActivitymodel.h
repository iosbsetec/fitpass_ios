//
//  RefineActivity.h
//  FitPass
//
//  Created by Bsetec on 03/08/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RefineActivitymodel : NSObject

@property(nonatomic,strong)     NSString  *refineActivity_id;
@property(nonatomic,strong)     NSString  *refineActivity_name;



-(RefineActivitymodel *)initWithDic:(NSDictionary *)activityDic;



@end
