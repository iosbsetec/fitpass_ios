//
//  WorkOutModel.h
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserProfileModel : NSObject

@property(nonatomic,strong)     NSString  *address_line1;
@property(nonatomic,strong)     NSString  *address_line2;
@property(nonatomic,strong)     NSString  *city;
@property(nonatomic,strong)     NSString  *first_name;
@property(nonatomic,strong)     NSString  *end_of_cyle;
@property(nonatomic,strong)     NSString  *gender;
@property(nonatomic,strong)     NSString  *mobile_no;
@property(nonatomic,strong)     NSString  *pin_code;
@property(nonatomic,strong)     NSString  *profile_pic;
@property(nonatomic,strong)     NSString  *state_name;
@property(nonatomic,strong)     NSString  *user_id;

-(UserProfileModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;

@end


