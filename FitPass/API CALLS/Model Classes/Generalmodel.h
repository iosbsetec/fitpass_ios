//
//  Generalmodel.h
//  YACC
//
//  Created by Siba Prasad Hota on 04/11/14.
//  Copyright (c) 2014 BseTech. All rights reserved.
//

#import <Foundation/Foundation.h>
@class StudioModel;
@interface Generalmodel : NSObject

@property int numberOfMission;

@property(nonatomic,strong)  NSString *status;
@property(nonatomic,strong)  NSString *status_Msg;
@property(nonatomic,strong)  NSString *user_id;
@property(nonatomic,strong)  NSString *user_name;
@property(nonatomic,strong)  NSString *user_mobile;
@property(nonatomic,strong)  NSString *user_email;

@property(nonatomic,strong)  NSString *login_status;
@property(nonatomic,strong)  NSString *buy_status;

@property(nonatomic,strong)  NSString *status_code;
@property(nonatomic,strong)  NSString *temprecord;
@property(nonatomic,strong)  NSString *authtentication;
@property(nonatomic,strong)  NSString *end_of_cyle;
@property(nonatomic,strong)  NSMutableArray *tempArray;
@property(nonatomic,strong)  NSMutableArray *tempArray2;
@property(nonatomic,strong)  NSMutableArray *tempArray3;

@property(nonatomic,strong)  StudioModel * DetailsData;


@end
