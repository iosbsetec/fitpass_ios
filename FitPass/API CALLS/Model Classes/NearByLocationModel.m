//
//  NearByLocationModel.m
//  FitPass
//
//  Created by Bsetec on 03/08/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "NearByLocationModel.h"
#import "NSNull+JSON.h"


@implementation NearByLocationModel

-(NearByLocationModel *)initWithDic:(NSDictionary *)activityDic
{
    self.nearbylocation_id = [activityDic valueForKey:@"locality_id"];
    self.nearbylocation_name = [activityDic valueForKey:@"locality_name"];
    return self;
}
@end
