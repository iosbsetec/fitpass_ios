//
//  StudioModel.m
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "StudioModel.h"

@implementation StudioModel

-(StudioModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    
    self.about_studio   = [showRoomDict valueForKey:@"about_studio"];
    self.activities    = [showRoomDict valueForKey:@"activities"];
    self.air_conditioner    = [showRoomDict valueForKey:@"air_conditioner"];

    self.address_line1  = [showRoomDict valueForKey:@"address_line1"];
    self.address_line2  = [showRoomDict valueForKey:@"address_line2"];
    self.favourites_status    = [showRoomDict valueForKey:@"favourites_status"];
    self.latitude  = [showRoomDict valueForKey:@"latitude"];
    self.longitude  = [showRoomDict valueForKey:@"longitude"];

    self.locality_name  = [showRoomDict valueForKey:@"locality_name"];
    self.nearest_metrostation  = [showRoomDict valueForKey:@"nearest_metrostation"];
    self.parking_facility  = [showRoomDict valueForKey:@"parking_facility"];

    
    self.pin_code       = [showRoomDict valueForKey:@"pin_code"];
    self.profile_pic    = [showRoomDict valueForKey:@"profile_pic"];
    self.studio_id      = [showRoomDict valueForKey:@"studio_id"];
    self.studio_name    = [showRoomDict valueForKey:@"studio_name"];
    
    self.rating  = [showRoomDict valueForKey:@"rating"];

    

       return self;
    
}


@end
