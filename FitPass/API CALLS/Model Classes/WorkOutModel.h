//
//  WorkOutModel.h
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WorkOutModel : NSObject

@property(nonatomic,strong)     NSString  *batch_id;
@property(nonatomic,strong)     NSString  *batch_status;
@property(nonatomic,strong)     NSString  *duration;
@property(nonatomic,strong)     NSString  *end_time;
@property(nonatomic,strong)     NSString  *locality_name;
@property(nonatomic,strong)     NSString  *location_id;
@property(nonatomic,strong)     NSString  *feedPostedDate;
@property(nonatomic,strong)     NSString  *start_time;
@property(nonatomic,strong)     NSString  *studio_id;
@property(nonatomic,strong)     NSString  *studio_name;
@property(nonatomic,strong)     NSString  *work_out_id;
@property(nonatomic,strong)     NSString  *user_notice,*share_url,*schedule_id;

@property(nonatomic,strong)     NSString  *locationame;

@property(nonatomic,strong)     NSString  *work_description;
@property(nonatomic,strong)     NSString  *work_out_name,*address_line1,*address_line2,*air_conditioner,*date_of_workout,*nearest_metrostation,*parking_facility,*work_out_pic,*longitude,*latitude;

-(WorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;

@end


