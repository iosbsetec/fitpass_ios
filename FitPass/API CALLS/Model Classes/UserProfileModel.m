//
//  WorkOutModel.m
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "UserProfileModel.h"
#import "NSNull+JSON.h"

@implementation UserProfileModel
-(UserProfileModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    self.address_line1       = [showRoomDict valueForKey:@"address_line1"];
    self.address_line2       = [showRoomDict valueForKey:@"address_line2"];
    self.city       = [showRoomDict valueForKey:@"city"];
    self.first_name       = [showRoomDict valueForKey:@"first_name"];
    self.gender       = [showRoomDict valueForKey:@"gender"];
    self.mobile_no   =   [showRoomDict valueForKey:@"mobile_no"];
    self.pin_code       = [showRoomDict valueForKey:@"pin_code"];
    self.profile_pic       =   [showRoomDict valueForKey:@"profile_pic"];
    self.state_name  = [showRoomDict valueForKey:@"state_name"];
    self.user_id    =[showRoomDict valueForKey:@"user_id"];
    self.end_of_cyle = [showRoomDict valueForKey:@"end_of_cyle"];
    return self;

}
@end
