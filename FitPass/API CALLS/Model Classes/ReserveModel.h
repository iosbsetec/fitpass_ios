//
//  CatModel.h
//  Unity-iPhone
//
//  Created by Siba Prasad Hota on 06/10/14.
//
//

#import <Foundation/Foundation.h>

@interface ReserveModel : NSObject

@property(nonatomic,strong)NSString *location_id;
@property(nonatomic,strong)NSString *studio_id;
@property(nonatomic,strong)NSString *batch_id;
@property(nonatomic,strong)NSString *schedule_date;

@end
