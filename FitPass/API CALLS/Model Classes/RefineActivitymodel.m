//
//  RefineActivity.m
//  FitPass
//
//  Created by Bsetec on 03/08/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "RefineActivitymodel.h"
#import "NSNull+JSON.h"


@implementation RefineActivitymodel


-(RefineActivitymodel *)initWithDic:(NSDictionary *)activityDic;
{
    NSLog(@"activityDic %@",activityDic);
    
    self.refineActivity_id         = [activityDic valueForKey:@"work_out_id"];
    self.refineActivity_name       = [activityDic valueForKey:@"work_out_name"];
    
    return self;
}
@end
