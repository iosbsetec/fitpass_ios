//
//  WorkOutModel.m
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "WorkOutModel.h"
#import "NSNull+JSON.h"

@implementation WorkOutModel
-(WorkOutModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict
{
    self.address_line1       = [showRoomDict valueForKey:@"address_line1"];
    self.address_line2       = [showRoomDict valueForKey:@"address_line2"];
    self.air_conditioner       = [showRoomDict valueForKey:@"air_conditioner"];
    self.date_of_workout       = [showRoomDict valueForKey:@"date_of_workout"];
    if ([[showRoomDict valueForKey:@"about_studio"] length]) {
        self.work_description      = [showRoomDict valueForKey:@"about_studio"];
    }
    
    
    //
    self.batch_id       = [showRoomDict valueForKey:@"batch_id"];
    self.batch_status   =   [showRoomDict valueForKey:@"batch_status"];
    self.duration       = [showRoomDict valueForKey:@"duration"];
    self.end_time       =   [showRoomDict valueForKey:@"end_time"];
    self.locality_name  = [showRoomDict valueForKey:@"locality_name"];
    self.location_id    =[showRoomDict valueForKey:@"location_id"];
    self.nearest_metrostation    =[showRoomDict valueForKey:@"nearest_metrostation"];
    self.parking_facility    =[showRoomDict valueForKey:@"parking_facility"];
    self.start_time     = [showRoomDict valueForKey:@"start_time"];
    self.studio_id      =[showRoomDict valueForKey:@"studio_id"];
    self.longitude     = [showRoomDict valueForKey:@"longitude"];
    self.latitude      =[showRoomDict valueForKey:@"latitude"];
    self.studio_name    = [showRoomDict valueForKey:@"studio_name"];
    self.work_out_id    =[showRoomDict valueForKey:@"work_out_id"];
    self.work_out_name  = [showRoomDict valueForKey:@"work_out_name"];
    self.work_out_pic  = [showRoomDict valueForKey:@"work_out_pic"];

    self.schedule_id  = [showRoomDict valueForKey:@"schedule_id"];

    _locationame  = [showRoomDict valueForKey:@"location_name"];
    
    if ([[showRoomDict valueForKey:@"user_notice"] length]) {
        self.user_notice  = [showRoomDict valueForKey:@"user_notice"];
    }
    if ([[showRoomDict valueForKey:@"share_url"] length]) {
        self.share_url  = [showRoomDict valueForKey:@"share_url"];
    }
    return self;

}
@end
//"batch_id" = 426;
//"batch_status" = 1;
//duration = "210 min";
//"end_time" = "21:00";
//"locality_name" = Kalkaji;
//"location_id" = 332;
//"start_time" = "17:30";
//"studio_id" = 342;
//"studio_name" = "Knock Out Fight Club Kalkaji";
//"work_out_id" = 357;
//"work_out_name" = "Street Fight Secrets";