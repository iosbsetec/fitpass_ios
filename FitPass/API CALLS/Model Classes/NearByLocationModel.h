//
//  NearByLocationModel.h
//  FitPass
//
//  Created by Bsetec on 03/08/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NearByLocationModel : NSObject

@property(nonatomic,strong)     NSString  *nearbylocation_id;
@property(nonatomic,strong)     NSString  *nearbylocation_name;

-(NearByLocationModel *)initWithDic:(NSDictionary *)activityDic;

@end
