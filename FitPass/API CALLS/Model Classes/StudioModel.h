//
//  StudioModel.h
//  FitPass
//
//  Created by JITENDRA on 7/31/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StudioModel : NSObject


@property(nonatomic,strong)     NSString  *about_studio;
@property(nonatomic,strong)     NSString  *address_line1;
@property(nonatomic,strong)     NSString  *address_line2;
@property(nonatomic,strong)     NSString  *locality_name;

@property(nonatomic,strong)     NSString  *pin_code;
@property(nonatomic,strong)     NSString  *profile_pic;
@property(nonatomic,strong)     NSString  *studio_id;
@property(nonatomic,strong)     NSString  *studio_name;
@property(nonatomic,strong)     NSString  *activities,*air_conditioner,*favourites_status,*latitude,*longitude,*nearest_metrostation,*parking_facility,*rating;


-(StudioModel *)initWithShowRoomDict:(NSDictionary*)showRoomDict;


//
//"about_studio" = "<p>Abin Academy of Martial Arts&nbsp;offers training programs that are designed for people of all age groups. They believe there is a need for specific self-defense training to cover the gaps left by traditional martial arts. To bridge those gaps, they have introduced personalized Defense Training Programs.<br />
//\n<br />
//\nThey are affiliated with the governing body of karate and kick boxing in India. The Academy is currently producing more than 20 quality national players in karate and kick-boxing in a year. They provide personal classes for karate, kick-boxing, self-defense and MMA at our premises. They also provide self-defense and kick-boxing classes for corporates and colleges, karate classes for Schools.</p>";
//activities = "Kickboxing, Karate";
//"address_line1" = "Corporate Dojo, S-3117";
//"address_line2" = "Behind Corporate Park";
//"locality_name" = "DLF Phase 3";
//"pin_code" = 122002;
//"profile_pic" = "http://ec2-54-254-186-249.ap-southeast-1.compute.amazonaws.com/upload/1805-fit-pass/FE248E6B7F26B2E9F89152CA1F71F22A.png";
//"studio_id" = 1805;
//"studio_name" = "Abin Academy of Martial Arts Gurgaon";
@end
