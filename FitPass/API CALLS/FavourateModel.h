//
//  FavourateModel.h
//  FitPass
//
//  Created by BseTec on 8/5/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FavourateModel : NSObject

@property(nonatomic,strong)NSString *favourites_status;
@property(nonatomic,strong)NSString *locality_name;
@property(nonatomic,strong)NSString *profile_pic;
@property(nonatomic,strong)NSString *rating;
@property(nonatomic,strong)NSString *studio_id;
@property(nonatomic,strong)NSString *studio_name;


-(FavourateModel *)initWithDic:(NSDictionary *)favourateDic;


@end
