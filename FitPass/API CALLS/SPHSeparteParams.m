

//==============================================================================
///////////    THIS CLASS SEPARATES ALL PARAMETRES    //////////////////////////
//==============================================================================


#import "SPHSeparteParams.h"
#import "NSNull+JSON.h"
#import "Keys.h"
#import "Generalmodel.h"
#import "WorkOutModel.h"
#import "StudioModel.h"
#import "RefineActivitymodel.h"
#import "NearByLocationModel.h"
#import "FavourateModel.h"
#import "UpComingWorkOutModel.h"
#import "PastWorkOutModel.h"
#import "DBManager.h"
#import "UserProfileModel.h"

@implementation SPHSeparteParams

@synthesize

methodName=_methodName,
params =_params,
error =_error;

// Profile_IamgeURL METHOD_USERDETAILS

//==============================================================================
///////////    THIS BELOW METHOD SEPARATES ALL METHODS  ////////////////////////
//==============================================================================

+(id)SeparateParams:(NSDictionary *)params  methodName:(NSString *)method_Name
{
    if ([method_Name isEqualToString:METHOD_WORKOUTLIST])
        return [self SeparateParamForWorkOutList:params];
    if ([method_Name isEqualToString:METHOD_STUDIOLIST])
        return [self SeparateParamForStudioList:params];
    if ([method_Name isEqualToString:METHOD_STUDIODETAILS])
        return [self SeparateParamForStudioDetails:params];
    
    if ([method_Name isEqualToString:METHOD_WORKOUTDETAILS])
        return [self SeparateParamForWorkoutDetails:params];
    if ([method_Name isEqualToString:@""])
        return [self SeparateParamForComplimintUserList:params];
    if ([method_Name isEqualToString:METHOD_GETWORKOUT_ACTIVITY])
        return [self SeparateParamForGetRefineActivity:params];
    if ([method_Name isEqualToString:METHOD_GETWORKOUT_NEARLOCATION])
        return [self SeparateParamForGetNearLocation:params];
    if ([method_Name isEqualToString:METHOD_GETFAVORITESSTUDEOLIST])
        return [self SeparateParamForGetFavorates:params];
    if ([method_Name isEqualToString:METHOD_SIGNUP])
        return [self SeparateParamForSignUp:params];
    if ([method_Name isEqualToString:METHOD_LOGIN])
        return [self SeparateParamForLogin:params];
    if ([method_Name isEqualToString:METHOD_GETSTUDIOS])
        return [self SeparateParamForStudios:params];
    //
    if ([method_Name isEqualToString:METHOD_SIGNUPLOGINFB])
        return [self SeparateParamForSignUp:params];

    
    if ([method_Name isEqualToString:METHOD_FORGETPASSWORD])
        return [self SeparateParamForForGetPassword:params];
    
    if ([method_Name isEqualToString:METHOD_UPCOMINGWORKOUT])
        return [self SeparateParamForUpComingWorkoutDetails:params];
    if ([method_Name isEqualToString:METHOD_PASTWORKOUT])
        return [self SeparateParamForUpComingWorkoutDetails:params];
    if ([method_Name isEqualToString:METHOD_STUDIOSCHEDULES])
        return [self SeparateParamForStudioSchedule:params];
    if ([method_Name isEqualToString:METHOD_FAVOURITESSTUDIO])
        return [self SeparateParamForFavorateStudio:params];
    

    
    if ([method_Name isEqualToString:METHOD_GETUSERPROFILE])
        return [self SeparateParamForGetUserProfile:params];
    if ([method_Name isEqualToString:METHOD_UPDATEUSERPROFILE])
        return [self SeparateParamForUpdateUserProfile:params];
    if ([method_Name isEqualToString:METHOD_UPLOADPHOTO])
        return [self SeparateParamForUploadPhoto:params];
    
    if ([method_Name isEqualToString:METHOD_USERAUTHENTICATION])
        return [self SeparateParamForuserAuthentication:params];
    if ([method_Name isEqualToString:METHOD_CHECKUSER])
        return [self SeparateParamForcheckfitpassuser:params];
    
    
    if ([method_Name isEqualToString:METHOD_PURCHASENOW])
        return [self SeparateParamForPurchaseNow:params];
    
    
    if ([method_Name isEqualToString:METHOD_USERPASTEXP])
        return [self SeparateParamForuserPastExperience:params];
    if ([method_Name isEqualToString:METHOD_RESERVEWORKOUT] ||[method_Name isEqualToString:METHOD_CANCELWORKOUT])
        return [self SeparateParamForuserReserveWorkout:params];
    
    return nil;
}
+(id)SeparateParamForuserReserveWorkout:(NSDictionary *)params
{
    NSLog(@"userPastExperience Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg = [params valueForKey:@"message"];
        gModel.status_code = @"1";
        
    }
    else
        
    {
        
        gModel.status_Msg = @"Failed";
        gModel.status_code = @"0";
    }
    
    return gModel;
}
+(id)SeparateParamForuserPastExperience:(NSDictionary *)params
{
    NSLog(@"userPastExperience Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg = [params valueForKey:@"message"];
        gModel.status_code = @"1";

    }
    else
        
    {
    
        gModel.status_Msg = @"Failed";
        gModel.status_code = @"0";
    }
    
    return gModel;
}
+(id)SeparateParamForuserAuthentication:(NSDictionary *)params
{
    NSLog(@"Authentication Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    
    if ([params valueForKey:@"login_status"])
    gModel.login_status = [params valueForKey:@"login_status"];
    
    return gModel;
}

+(id)SeparateParamForPurchaseNow:(NSDictionary *)params
{
    NSLog(@"response Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    gModel.status_code = @"1";
    
    if ([params valueForKey:@"end_of_cyle"])
        SETVALUE([params valueForKey:@"end_of_cyle"], @"endofcycle");
    
    return gModel;
}

+(id)SeparateParamForcheckfitpassuser:(NSDictionary *)params
{
    NSLog(@"Checkfitpassuser Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    
    gModel.status = [params valueForKey:@"buy_status"];
    
    return gModel;
}


+(id)SeparateParamForFavorateStudio:(NSDictionary *)params
{
    NSLog(@"UpComingWorkout Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];

    gModel.status = [params valueForKey:@"favourites_status"];
    
    return gModel;
}




+(id)SeparateParamForStudioSchedule:(NSDictionary *)params
{
    NSLog(@"UpComingWorkout Param %@",params);
    
    Generalmodel *gModel = [Generalmodel new];
    
    NSArray *workOutList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    for(NSDictionary *dictObject in workOutList)
    {
        [gModel.tempArray addObject:[[WorkOutModel alloc]initWithShowRoomDict:dictObject ]];
        
    }
    
    return gModel;
}




+(id)SeparateParamForUpComingWorkoutDetails:(NSDictionary *)params
{
    
    
     NSLog(@"UpComingWorkout Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    gModel.status = @"Data Fetched";
    gModel.status_code =@"1";
    gModel.tempArray = [NSMutableArray new];
    //    [gModel.tempArray addObject:[[UpComingWorkOutModel alloc]initWithShowRoomDict:params ]];
    
    NSArray *workOutList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    for(NSDictionary *dictObject in workOutList)
    {
        [gModel.tempArray addObject:[[UpComingWorkOutModel alloc]initWithShowRoomDict:dictObject ]];
        
    }
    
       return gModel;
}

+(id)SeparateParamForPastWorkoutDetails:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =@"data fetched";
    gModel.status_code =@"1";
    gModel.tempArray = [NSMutableArray new];
    NSArray *workOutList = [params valueForKey:@"result"];

    for(NSDictionary *dictObject in workOutList)
    {
        [gModel.tempArray addObject:[[PastWorkOutModel alloc]initWithShowRoomDict:dictObject]];
        
    }
    return gModel;
}


+(id)SeparateParamForGetFavorates:(NSDictionary *)params
{
    
    NSLog(@"Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    
    NSArray *activityList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    for (NSDictionary *dicObjects in activityList)
    {
        [gModel.tempArray addObject:[[FavourateModel alloc]initWithDic:dicObjects ]];
    }
    
    return gModel;
}

+(id)SeparateParamForWorkOutList:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    
    
    
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code = @"0";
    }
    else
    {
        NSArray *workOutList = [params valueForKey:@"result"];
        gModel.tempArray = [NSMutableArray new];
        
        for(NSDictionary *dictObject in workOutList)
        {
            [gModel.tempArray addObject:[[WorkOutModel alloc]initWithShowRoomDict:dictObject ]];
            
        }
        gModel.status_code =@"1";
    }

    
    return gModel;
}

+(id)SeparateParamForStudioList:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];

    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code = @"0";
    }
    else{
    // gModel.status =[params valueForKey:@"status"];
    gModel.status_code = @"1";
    // gModel.status_Msg =[params valueForKey:@"success_message"];
    
    NSArray *workOutList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    for(NSDictionary *dictObject in workOutList)
    {
        [gModel.tempArray addObject:[[StudioModel alloc]initWithShowRoomDict:dictObject ]];
        
    }
    }
    return gModel;
}

+(id)SeparateParamForWorkoutDetails:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    gModel.status_code =@"1";
    gModel.tempArray = [NSMutableArray new];
    [gModel.tempArray addObject:[[WorkOutModel alloc]initWithShowRoomDict:params ]];

    return gModel;
}

+(id)SeparateParamForStudioDetails:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    gModel.status_code =@"1";
    gModel.tempArray = [NSMutableArray new];
    [gModel.tempArray addObject:[[StudioModel alloc]initWithShowRoomDict:params ]];

    return gModel;
}

//
+(id)SeparateParamForGetRefineActivity:(NSDictionary *)params
{
    DBManager *dbManager;
    
    // Initialize the dbManager object.
    dbManager = [[DBManager alloc] initWithDatabaseFilename:@"fitpassdb.sql"];
    NSLog(@"Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    
    NSArray *activityList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    

    
   
    for (NSDictionary *dicObjects in activityList)
    {
        //   create an insert query.
        NSString *query;
        
        query = [NSString stringWithFormat:@"insert into Activity values( %d,'%@')",[[dicObjects valueForKey:@"work_out_id"] intValue], [dicObjects valueForKey:@"work_out_name"]];
        // Execute the query.
        [dbManager executeQuery:query];
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            NSLog(@"Could not execute the query.");
        }

        

        //[gModel.tempArray addObject:[[RefineActivitymodel alloc]initWithDic:dicObjects ]];
    }
    dbManager = nil;
    return gModel;
}

//
+(id)SeparateParamForGetNearLocation:(NSDictionary *)params
{
    DBManager *dbManager;

    NSLog(@"Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    // Initialize the dbManager object.
    dbManager = [[DBManager alloc] initWithDatabaseFilename:@"fitpassdb.sql"];

    NSArray *activityList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
   
    for (NSDictionary *dicObjects in activityList)
    {
        //   create an insert query.
        NSString *query;
//        [[dicObjects valueForKey:@"locality_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        query = [NSString stringWithFormat:@"insert into Locality values(%d,'%@')",[[dicObjects valueForKey:@"locality_id"] intValue], [[dicObjects valueForKey:@"locality_name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]];
        // Execute the query.
        [dbManager executeQuery:query];
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        

      //  [gModel.tempArray addObject:[[NearByLocationModel alloc]initWithDic:dicObjects ]];
    }
    
    return gModel;
}
//
+(id)SeparateParamForStudios:(NSDictionary *)params
{
    DBManager *dbManager;
    
    NSLog(@"Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    // Initialize the dbManager object.
    dbManager = [[DBManager alloc] initWithDatabaseFilename:@"fitpassdb.sql"];
    
    NSArray *activityList = [params valueForKey:@"result"];
    gModel.tempArray = [NSMutableArray new];
    
    for (NSDictionary *dicObjects in activityList)
    {
        //   create an insert query.
        NSString *query;
        
        NSString *str;
        str = [[dicObjects valueForKey:@"studio_name"] stringByReplacingOccurrencesOfString:@ "'" withString: @"''"];
        
        query = [NSString stringWithFormat:@"insert into StudioList values( %d,'%@')",[[dicObjects valueForKey:@"studio_id"] intValue], str];
        
        // Execute the query.
        [dbManager executeQuery:query];
        // If the query was successfully executed then pop the view controller.
        if (dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", dbManager.affectedRows);
            
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        
        
        //  [gModel.tempArray addObject:[[NearByLocationModel alloc]initWithDic:dicObjects ]];
    }
    dbManager = nil;
    return gModel;
}

+(id)SeparateParamForSignUp:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    
   
    if ( [params valueForKey:@"code"] || ![[params allKeys] count])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
        SETVALUE([params valueForKey:@"end_of_cyle"], @"endofcycle");
        
        if ([[params valueForKey:@"end_of_cyle"] isEqualToString:@"In-Active"])
        {
            SETVALUE(@"Inactive", @"endofcycle");
        }
        
        gModel.status_Msg =[NSString stringWithFormat:@"%@~~%@~~%@",[params valueForKey:@"first_name"],[params valueForKey:@"email_address"],GETVALUE(@"endofcycle")];
        
        SETVALUE(gModel.status_Msg, @"UserNameandEmail");
        gModel.user_id = [params valueForKey:@"user_id"];
        gModel.user_email = [params valueForKey:@"email_address"];
        gModel.authtentication = [params valueForKey:@"auth_key"];
        gModel.status_code =@"1";
    }
    
    return gModel;
}

+(id)SeparateParamForLogin:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    
    if ( [params valueForKey:@"code"] || ![[params allKeys] count])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
        NSLog(@"%@~~%@",[params valueForKey:@"first_name"],[params valueForKey:@"email_address"]);
        
        
        gModel.status_Msg =[NSString stringWithFormat:@"%@~~%@~~%@",[params valueForKey:@"first_name"],[params valueForKey:@"email_address"],[params valueForKey:@"end_of_cyle"]];
        SETVALUE([params valueForKey:@"end_of_cyle"], @"endofcycle");
        if ([GETVALUE(@"endofcycle") isEqualToString:@"In-Active"])
        {
            SETVALUE(@"Inactive", @"endofcycle");
        }
        SETVALUE(gModel.status_Msg, @"UserNameandEmail");
        gModel.user_id = [params valueForKey:@"user_id"];
        gModel.user_email = [params valueForKey:@"email_address"];
        gModel.user_name = [params valueForKey:@"first_name"];
        gModel.user_mobile = [params valueForKey:@"mobile_no"];
        gModel.authtentication = [params valueForKey:@"auth_key"];
        gModel.status_code =@"1";
    }
    gModel.tempArray = [NSMutableArray new];
    return gModel;
}

+(id)SeparateParamForForGetPassword:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    
    if ([[params valueForKey:@"code"] integerValue] ==  412)
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
        gModel.status_code =@"1";
        gModel.status_Msg =[params valueForKey:@"message"];

    }
    
    return gModel;
}


+(id)SeparateParamForGetUserProfile:(NSDictionary *)params
{
    NSLog(@"Param %@",params);
    Generalmodel *gModel = [Generalmodel new];
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
    gModel.tempArray = [NSMutableArray new];
        gModel.status_code =@"1";
        
    [gModel.tempArray addObject:[[UserProfileModel alloc]initWithShowRoomDict:params ]];
    
    }
    return gModel;
}

+(id)SeparateParamForUpdateUserProfile:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
        gModel.status_code =@"1";
    }
    
    gModel.tempArray = [NSMutableArray new];
    return gModel;
}

+(id)SeparateParamForUploadPhoto:(NSDictionary *)params
{
    Generalmodel *gModel = [Generalmodel new];
    gModel.status =[params valueForKey:@"status"];
    if ([params valueForKey:@"code"])
    {
        gModel.status_Msg =[params valueForKey:@"message"];
        gModel.status_code =@"0";
    }
    else
    {
        gModel.status_code =@"1";
    }
    
    gModel.tempArray = [NSMutableArray new];
    return gModel;
}

//list
+(id)SeparateParamForComplimintUserList:(NSDictionary *)params
{
    return nil;
}

@end
