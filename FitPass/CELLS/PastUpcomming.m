//
//  PastUpcomming.m
//  FitPass
//
//  Created by BseTec on 8/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "PastUpcomming.h"

@implementation PastUpcomming
{
    __weak IBOutlet UILabel *lblClassStartTime;
    __weak IBOutlet UILabel *lblActivityName;
    
    __weak IBOutlet UILabel *lblClassLocation;
    __weak IBOutlet UILabel *lblClassDuration;
    __weak IBOutlet UILabel *lbldate;
    
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)setCellDataUpComingWorkout : (UpComingWorkOutModel *) workOut
{
    _lblClassName.text  = workOut.studio_name;
    lblClassStartTime.text = workOut.start_time;
    lblActivityName.text = workOut.work_out;
    lblClassDuration.text = workOut.duration;
    lblClassLocation.text = workOut.locality_name;
    lbldate.text = workOut.schedule_date;

}

-(void)setCellDataPastWorkout : (PastWorkOutModel *) workOut;
{
    _lblClassName.text      = workOut.studio_name;
    lblClassStartTime.text  = workOut.start_time;
    lblActivityName.text    = workOut.work_out;
    lblClassDuration.text   = workOut.duration;
    
    lbldate.text = workOut.schedule_date;
    lblClassLocation.text   = workOut.locality_name;
}


@end
