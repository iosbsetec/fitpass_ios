//
//  CellFindClass.m
//  FitPass
//
//  Created by JITENDRA on 7/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "CellFindClass.h"
#import "WorkOutModel.h"
#import "UpComingWorkOutModel.h"
#import "PastWorkOutModel.h"

@implementation CellFindClass
{

    

    __weak IBOutlet UILabel *lblClassStartTime;
    __weak IBOutlet UILabel *lblActivityName;

    __weak IBOutlet UILabel *lblClassLocation;
    __weak IBOutlet UILabel *lblClassDuration;
}



- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellData : (WorkOutModel *) workOut;

{
   
    _lblClassName.text  = workOut.studio_name;
    lblClassStartTime.text = workOut.start_time;
    lblActivityName.text = workOut.work_out_name;
    lblClassDuration.text = workOut.duration;
    lblClassLocation.text = workOut.locality_name;



}
-(void)setCellDataUpComingWorkout : (UpComingWorkOutModel *) workOut
{
    _lblClassName.text  = workOut.studio_name;
    lblClassStartTime.text = workOut.start_time;
    lblActivityName.text = workOut.work_out;
    lblClassDuration.text = workOut.duration;
    lblClassLocation.text = workOut.locality_name;
}

-(void)setCellDataPastWorkout : (PastWorkOutModel *) workOut;
{
    _lblClassName.text       = workOut.studio_name;
    lblClassStartTime.text  = workOut.start_time;
    lblActivityName.text    = workOut.work_out;
    lblClassDuration.text   = workOut.duration;
    lblClassLocation.text   = workOut.locality_name;
}

-(void)setCellDataStudioSchedule : (WorkOutModel *) workOut
{
    NSLog(@"%@",workOut.start_time);
    //lblClassName.text       = workOut.studio_name;
    lblClassStartTime.text  = workOut.start_time;
    lblActivityName.text    = workOut.work_out_name;
    lblClassDuration.text   = workOut.duration;
    lblClassLocation.text   = workOut.locationame;
}

//


@end
