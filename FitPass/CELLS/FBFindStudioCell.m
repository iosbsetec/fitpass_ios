//
//  FBFindStudioCell.m
//  FitPass
//
//  Created by JITEN on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FBFindStudioCell.h"
#import "AsyncImageView.h"
@implementation FBFindStudioCell
{
    __weak IBOutlet UIImageView *imgVProfilePic;
    __weak IBOutlet UILabel *lblLocationName;
    __weak IBOutlet UILabel *lblActitvity;
    __weak IBOutlet UILabel *lblStudioName;
}

- (void)awakeFromNib {
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCellData : (StudioModel *) studioData
{

    @try {
        lblActitvity.text = [NSString stringWithFormat:@"Workouts: %@",studioData.activities];
        lblLocationName.text = studioData.locality_name;
        lblStudioName.text = studioData.studio_name;
        imgVProfilePic.imageURL =[NSURL URLWithString:studioData.profile_pic];
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception %@",exception);
    }
    @finally {
        
        
    }
    
    

}

@end
