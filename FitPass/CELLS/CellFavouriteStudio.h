//
//  CellFavouriteStudio.h
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class FavourateModel;
@interface CellFavouriteStudio : UITableViewCell
{
    
    IBOutlet UILabel *lblratings;
}
-(void)cellData : (FavourateModel *) favModel;

@end
