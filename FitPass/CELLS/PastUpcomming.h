//
//  PastUpcomming.h
//  FitPass
//
//  Created by BseTec on 8/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UpComingWorkOutModel.h"
#import "PastWorkOutModel.h"


@interface PastUpcomming : UITableViewCell



@property (strong, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UILabel *lblClassName;


-(void)setCellDataUpComingWorkout : (UpComingWorkOutModel *) workOut;
-(void)setCellDataPastWorkout : (PastWorkOutModel *) workOut;


@end
