//
//  CellFindClass.h
//  FitPass
//
//  Created by JITENDRA on 7/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
@class WorkOutModel,UpComingWorkOutModel,PastWorkOutModel;
@interface CellFindClass : UITableViewCell
@property (strong, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UILabel *lblClassName;
-(void)setCellData : (WorkOutModel *) workOut;
-(void)setCellDataUpComingWorkout : (UpComingWorkOutModel *) workOut;
-(void)setCellDataPastWorkout : (PastWorkOutModel *) workOut;
-(void)setCellDataStudioSchedule : (PastWorkOutModel *) workOut;
@end
