//
//  FBFindStudioCell.h
//  FitPass
//
//  Created by JITEN on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StudioModel.h"
@interface FBFindStudioCell : UITableViewCell

-(void)setCellData : (StudioModel *) studioData;


@end
