//
//  CellFavouriteStudio.m
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "CellFavouriteStudio.h"
#import "FavourateModel.h"
#import "AsyncImageView.h"
@implementation CellFavouriteStudio

{

    __weak IBOutlet UIImageView *imgVStudio;

    __weak IBOutlet UIImageView *imgVHeart;

    __weak IBOutlet UILabel *lblRatio;

    __weak IBOutlet UILabel *lblStudioName;
    __weak IBOutlet UILabel *lblLocalityName;
}
- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    lblratings.layer.borderColor = [UIColor whiteColor].CGColor;
    lblratings.layer.borderWidth = 2.0;

    // Configure the view for the selected state
}
-(void)cellData : (FavourateModel *) favModel
{

    imgVStudio.imageURL = [NSURL URLWithString:favModel.profile_pic];
    lblLocalityName.text = favModel.locality_name;
    lblratings.text = [NSString stringWithFormat:@"%@/5",favModel.rating];
    lblStudioName.text = favModel.studio_name;
    imgVHeart.image  = ([favModel.favourites_status integerValue] == 1)?[UIImage imageNamed:@"35x35_icon_like_white.png"]:[UIImage imageNamed:@"icon_like_white_border.png"];


}

@end
