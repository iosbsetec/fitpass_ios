//
//  FPUpcomingVC.h
//  FitPass
//
//  Created by bsetec on 7/10/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FPUpcomingVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    BOOL isUpcomingWorkout;
    __weak IBOutlet UIButton *btnback;
    __weak IBOutlet UITableView *fitpass_tableview;
    __weak IBOutlet UIView *viewHeaderDetail;
}
@property (weak, nonatomic) IBOutlet UIButton *btnupcoming;
@property (weak, nonatomic) IBOutlet UIButton *btnPast;
- (IBAction)btnsettings:(id)sender;

- (IBAction)btnbackaction:(id)sender;
@property(strong,nonatomic)NSString *str;

@end
