//
//  FpSettingVC.m
//  FitPass
//
//  Created by JITENDRA on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#define arraySectionHeader  @[@{@"PROFILE":@[@"Email Id"]}, @{@"BILLING":@[@"Renew/Add a new card",@"Current cycle:ends 28/07/15"]},@{@"INFO":@[@"Sweatiquette and FAQs",@"Terms and Conditions",@"Privacy Policy"]}]

#import "FpSettingVC.h"
#import "FPAppDelegate.h"
#import "UserProfileModel.h"
#import "MBProgressHUD.h"
#import "PaymentPageViewController.h"

@interface FpSettingVC ()
{
    IBOutlet UIView *viewSectionHeader;
    IBOutlet UITableView *tblVwSettings;
    IBOutlet UILabel *lblHeaderText;
    UIFont *myFont ;
    UIView *footerView;
    UIButton *button;
    
    IBOutlet UIButton *btnlogout;
}
@property (strong, nonatomic) IBOutlet UIView *tblFooterView;
@end

@implementation FpSettingVC
- (IBAction)btnbackaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    myFont = [ UIFont fontWithName: @"OPENSANS" size: 15.0 ];
    btnlogout.layer.cornerRadius = 5.0;
    btnlogout.clipsToBounds = YES;
    tblVwSettings.backgroundColor = [UIColor whiteColor];
//    self.view.backgroundColor = [UIColor colorWithRed:234.0/255.0 green:234/255.0 blue:235/255. alpha:1.0];

    tblVwSettings.tableFooterView = _tblFooterView;
}

-(void)viewWillAppear:(BOOL)animated
{
    [tblVwSettings reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *sectionHeader = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds),60)];
    sectionHeader.backgroundColor = [UIColor colorWithRed:32/255. green:186/255.0 blue:150/255. alpha:1.0];
//    sectionHeader.backgroundColor = [UIColor clearColor];
    sectionHeader.font =  [UIFont fontWithName:@"OPENSANS" size:15];
    sectionHeader.textColor = [UIColor whiteColor];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    sectionHeader.text = [[[arraySectionHeader objectAtIndex:section] allKeys] lastObject];
    return sectionHeader;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *sectionFooter = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds),22)];
    sectionFooter.backgroundColor = [UIColor whiteColor];
    return sectionFooter;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"settingCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];;
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    NSString *strNameAndMail = [[NSUserDefaults standardUserDefaults]objectForKey:@"UserNameandEmail"];

    if (indexPath.section==0)
    {
        if (indexPath.row == 0)
        {
            if ([[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:0] || [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:2])
            {
                cell.textLabel.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:0];
                cell.detailTextLabel.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:1];
            }
            cell.detailTextLabel.textColor = [UIColor grayColor];
        }
    }
    else if(indexPath.section == 1)
    {
        if (indexPath.row==0)
        {
            cell.textLabel.text = @"Renew/Add a new card";
        }
        else
        {
            if (GETVALUE(@"endofcycle"))
                cell.textLabel.text = [NSString stringWithFormat:@"Current Cycle Ends : %@",GETVALUE(@"endofcycle")];
            else
                cell.textLabel.text =@"Current Cycle Ends";
        }
    }
    else
    {
        cell.textLabel.text = [[[arraySectionHeader objectAtIndex: indexPath.section] valueForKey:[[[arraySectionHeader objectAtIndex:indexPath.section] allKeys] lastObject]] objectAtIndex:indexPath.row];
    }

    cell.textLabel.font = myFont;
    cell.textLabel.backgroundColor = [UIColor whiteColor];
    tblVwSettings.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // We have to use the borderColor/Width as opposed to just setting the
    // backgroundColor else the view becomes transparent and disappears during
    // the cell's selected/highlighted animation
    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 44, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.alpha = 0.5;
    separatorView.layer.borderWidth = 1.0;
    [cell.contentView addSubview:separatorView];
   // cell.accessoryType = UITableViewCellAccessoryCheckmark;
    UIImageView *imgView = [[UIImageView alloc ]initWithFrame:CGRectMake(0, 0, 12, 17)];
    [imgView setImage:[UIImage imageNamed:@"right_arrow_go.png"]];

    if (indexPath.section==0||(indexPath.section == 1 && indexPath.row == 0) || indexPath.section == 2)
        cell.accessoryView = imgView;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    CGFloat footerHeight = 0;
    if (section == 2)
    {
        footerHeight = 20;
    }
    return footerHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
            return 1;
            break;
        case 1:
            return 2;
            break;
        case 2:
            return 3;
            break;
        default:
            break;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    FPPrivatePolicyVC *privatepolicy;
    FpFAQSVC *faqs;
    FPTermsforUsers *TermsforUsers;
    FpUpdateProfileVC *updateProfile;
    PaymentPageViewController *pcontrol;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        privatepolicy = [[FPPrivatePolicyVC alloc]initWithNibName:@"FPPrivatePolicyVC" bundle:nil];
        faqs = [[FpFAQSVC alloc]initWithNibName:@"FpFAQSVC" bundle:nil];
        TermsforUsers = [[FPTermsforUsers alloc]initWithNibName:@"FPTermsforUsers" bundle:nil];
        updateProfile = [[FpUpdateProfileVC alloc]initWithNibName:@"FpUpdateProfileVC" bundle:nil];
        pcontrol = [[PaymentPageViewController alloc]initWithNibName:@"PaymentPageViewController" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        privatepolicy = [[FPPrivatePolicyVC alloc]initWithNibName:@"FPPrivatePolicyVC_ipad" bundle:nil];
        faqs = [[FpFAQSVC alloc]initWithNibName:@"FpFAQSVC_ipad" bundle:nil];
        TermsforUsers = [[FPTermsforUsers alloc]initWithNibName:@"FPTermsforUsers_ipad" bundle:nil];
        updateProfile = [[FpUpdateProfileVC alloc]initWithNibName:@"FpUpdateProfileVC_ipad" bundle:nil];
        pcontrol = [[PaymentPageViewController alloc]initWithNibName:@"PaymentPageViewController_ipad" bundle:nil];
    }
    
    switch (indexPath.section)
    {
        case 0:
            if (indexPath.row == 0)
            {
                [self.navigationController pushViewController: updateProfile animated:YES];
            }
            break;
        case 1:
            if (indexPath.row==0)
            {
                if ([self checkEndOfCycle])
                {
                    [self.navigationController pushViewController:pcontrol animated:YES];
                }
            }
            break;
        case 2:
            if (indexPath.row == 0)
                [self.navigationController pushViewController: faqs animated:YES];
            else if (indexPath.row == 1)
                [self.navigationController pushViewController:TermsforUsers animated:YES];
            else if (indexPath.row ==2)
                [self.navigationController pushViewController:privatepolicy animated:YES];
            break;
        default:
            break;
    }
}

-(BOOL)checkEndOfCycle
{
    NSString *strEndData =GETVALUE(@"endofcycle");
    if ([strEndData isEqualToString:@"Inactive"])
        return YES;
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    [dateFormat setDateFormat:@"dd/MM/yyyy"];

    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormat dateFromString:strEndData];
    NSDate *fiveDaysAgo = [dateFromString dateByAddingTimeInterval:-5*24*60*60];
    NSLog(@"%@  %@",[dateFormat stringFromDate:now],[dateFormat stringFromDate:fiveDaysAgo]);
    
    
    NSComparisonResult result;
    
    result = [[dateFormat stringFromDate:now] compare:[dateFormat stringFromDate:fiveDaysAgo]]; // comparing two dates
    
    if(result == NSOrderedAscending) // today is less
        return NO;
    else if(result == NSOrderedDescending || result == NSOrderedSame) // newDate is less or Both dates are same
        return YES;
    else                       // Date cannot be compared
        return NO;
}

#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36;
}

- (IBAction)btnLogout:(id)sender
{
    [YSSupport resetDefaultsLogout];
//    [self.navigationController popToRootViewControllerAnimated:YES];
    FPAppDelegate *appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    [appDelegate setRootViewController];
//    [[NSUserDefaults standardUserDefaults]setObject:nil forKey:@"Userid"];
}
@end
