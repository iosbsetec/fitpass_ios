//
//  SignInViewController.m
//  FitPass
//
//  Created by Pothiraj_BseTec on 17/07/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpVC.h"
#import "SignInVc.h"
#import "FPMenuVC.h"
@interface SignInViewController ()

{

    NSString *fNmae,*email;


}
    
- (IBAction)signUpwithEmail:(UIButton *)sender;


@end

@implementation SignInViewController
@synthesize FeceBook,Email;

- (void)viewDidLoad {
    [super viewDidLoad];
    FeceBook.layer.cornerRadius = 4.0;
    [FeceBook setClipsToBounds:YES];
    Email.layer.cornerRadius = 4.0;
    [Email setClipsToBounds:YES];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back:(id)sender{
    //ThirdViewController *third = [[ThirdViewController alloc]initWithNibName:@"ThirdViewController" bundle:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


- (IBAction)signUpwithEmail:(UIButton *)sender
{
    SignInVc *rootController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        rootController = [[SignInVc alloc] initWithNibName:@"SignInVc" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rootController = [[SignInVc alloc] initWithNibName:@"SignInVc_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:rootController animated:YES];
}
- (IBAction)signUpWithFacebook:(UIButton *)sender {
    
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController.view setUserInteractionEnabled:NO];
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name,email"}]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                                 
                                                 fNmae  = [result valueForKey:@"first_name"];
                                                 email  = [result valueForKey:@"email"];
                                                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200",[result valueForKey:@"id"]] forKey:FBUSERIMAGE];
                                                 SETVALUE(email, kUSEREMAIL);
                                                 SETVALUE(fNmae, USERNAME);
                                                    SYNCHRONISE;
                                                 
                                                 // imgVuserPhoto.imageURL= [ NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200",[result valueForKey:@"id"]]];
                                                 [self createAccount];
                                                 
                                             }
                                         }];
                                    }
                                }
                            }];
}

- (void)createAccount
{
    FPAppDelegate* appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }

    NSString  *currentDeviceId = [appDelegate getDeviceID];
    NSData *submitData = [[NSString stringWithFormat:@"first_name=%@&email_address=%@&password=%@&device_id=%@&device_type=iOS&device_token=%@",fNmae,email,email,currentDeviceId,GETVALUE(DEVICE_TOKEN)] dataUsingEncoding:NSUTF8StringEncoding];

//    NSData *submitData = [[NSString stringWithFormat:@"first_name=%@&email_address=%@&password=%@&device_id=%@&device_type=iOS&device_token=%@",fNmae,email,email,@"DABA0A50-7F67-4E7A-BA51-998592FEF1D5",@"e717e3e62a72e62b62fbce1d20597b7350e2467db5302f12b8ddd7b4a68763fe"] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString * dataFinal = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
    
    NSLog(@"final data %@",dataFinal);
    
    [YSAPICalls getSignUpSignInDetailsFBParamData:submitData SuccessionBlock:^(id newResponse)
     {
         //  isReserveDetailsRetrive = YES;
         UIWindow *window = [[UIApplication sharedApplication] keyWindow];
         [window.rootViewController.view setUserInteractionEnabled:YES];
         
         Generalmodel *model = newResponse;
         if ([model.status_code isEqualToString:@"0"])
         {
             [appDelegate onCreateToastMsgInWindow:[model.status_Msg stringByAppendingString:@"Please login"]];
         }
         else
         {
             if ([model.status_code isEqualToString:@"0"])
             {
                 [appDelegate onCreateToastMsgInWindow:model.status_Msg];
             }
             else
             {

                 [[NSUserDefaults standardUserDefaults]setObject:model.user_id forKey:@"Userid"];
                 [[NSUserDefaults standardUserDefaults]setObject:model.authtentication forKey:@"Authtentication"];
                 
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 
                 UIViewController *rootController;
                 if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                 {
                     rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
                 }
                 else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                 {
                     rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
                 }
                 
                 [self.navigationController pushViewController:rootController animated:YES];
             }
         }
         
     } andFailureBlock:^(NSError *error) {
         UIWindow *window = [[UIApplication sharedApplication] keyWindow];
         [window.rootViewController.view setUserInteractionEnabled:YES];
     }];
}
@end
