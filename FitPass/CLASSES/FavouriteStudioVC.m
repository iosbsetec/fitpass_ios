//
//  FavouriteStudioVC.m
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "FavouriteStudioVC.h"
#import "CellFavouriteStudio.h"
#import "FPStudioDetailsVC.h"
#import "FPFindClassVC.h"
#import "FpSettingVC.h"
#import "MBProgressHUD.h"
#import "FavourateModel.h"

@interface FavouriteStudioVC ()

{
    IBOutlet UIView *headerView;
    NSMutableArray *arrayFavoriteList;
}
@end

@implementation FavouriteStudioVC
@synthesize btnviewclasses;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    btnviewclasses.layer.cornerRadius = 4.0f;
    btnviewclasses.clipsToBounds = YES;
    arrayFavoriteList = [NSMutableArray array];
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height >= 667.0f)
        {
            [viewHeaderDetail setFrame:CGRectMake(viewHeaderDetail.frame.origin.x, viewHeaderDetail.frame.origin.y+45, viewHeaderDetail.frame.size.width, viewHeaderDetail.frame.size.height)];
        }
    }

    // Do any additional setup after loading the view from its nib.
}
- (IBAction)settingTapped:(UIButton *)sender
{
    FpSettingVC *setting;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        setting = [[FpSettingVC alloc]initWithNibName:@"FpSettingVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        setting = [[FpSettingVC alloc]initWithNibName:@"FpSettingVC_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:setting animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        simpleTableIdentifier = @"CellFavouriteStudio";
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        simpleTableIdentifier = @"CellFavouriteStudio_ipad";
    }
   CellFavouriteStudio  *cell = (CellFavouriteStudio *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil)
    {
        NSArray *nib;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"CellFavouriteStudio" owner:self options:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            nib = [[NSBundle mainBundle] loadNibNamed:@"CellFavouriteStudio_ipad" owner:self options:nil];
        }
        cell = [nib objectAtIndex:0];
    }
    
//    FavourateModel *fmodel = [arrayFavoriteList objectAtIndex:indexPath.row];
//    if ([fmodel.favourites_status isEqualToString:@"1"])
//    {
//        cell.imageView.image = [UIImage imageNamed:@"35x35_icon_like_white.png"];
//    }else
//    {
//        cell.imageView.image = [UIImage imageNamed:@"icon_like_white_border.png"];
//    }
    
    UIView *separateView;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        separateView = [[UIView alloc]initWithFrame:CGRectMake(0, 179, tableView.frame.size.width, 1)];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        separateView = [[UIView alloc]initWithFrame:CGRectMake(0, 299, tableView.frame.size.width, 1)];
    }
    separateView.backgroundColor = [UIColor whiteColor];
    [cell addSubview:separateView];
    
    [cell cellData:arrayFavoriteList[indexPath.row]];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayFavoriteList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 180;
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 300;
    }
    return 180;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     FavourateModel *favmodel = [arrayFavoriteList objectAtIndex:indexPath.row];
    FPStudioDetailsVC *obj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC_ipad" bundle:nil];
    }
     NSLog(@"arrayFavoriteList %@",favmodel.studio_id);
     obj.studeo_id = favmodel.studio_id;
     [self.navigationController pushViewController:obj animated:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self getFavorateStudioList];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnviewclassesaction:(id)sender
{
    FPFindClassVC * findclass;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        findclass = [[FPFindClassVC alloc]initWithNibName:@"FPFindClassVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        findclass = [[FPFindClassVC alloc]initWithNibName:@"FPFindClassVC_ipad" bundle:nil];
    }
    findclass.isComeFromFavorite = YES;
    [self.navigationController pushViewController:findclass animated:YES];
}

- (IBAction)btnbackaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];

}


-(void)getFavorateStudioList
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [YSAPICalls getfavotarestudeidetailswithSuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         Generalmodel *model = newResponse;
         NSLog(@"Gm =%@",model.tempArray);
         
         if ([arrayFavoriteList count]) {
             [arrayFavoriteList removeAllObjects];
         }
         
         if (model.tempArray.count > 0) {
             [arrayFavoriteList addObjectsFromArray:model.tempArray];
             
             _favouritestudio_tableview.tableHeaderView = nil;
             [btnviewclasses setHidden:NO];
         }
         else
         {
             _favouritestudio_tableview.scrollEnabled = NO;
             [btnviewclasses setHidden:YES];
           _favouritestudio_tableview.tableHeaderView = headerView;
         }
         
         [_favouritestudio_tableview reloadData];
         
     } andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

     }];
    
}
@end
