//
//  FPStudioDetailsScheduleVC.h
//  FitPass
//
//  Created by bsetec on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FPStudioDetailsScheduleVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIButton *btnPreviousDate;
    IBOutlet UIButton *btnNextDate;
    NSInteger nIncOrDecDate;
    NSString *selectedDate;
    IBOutlet UIImageView *imgPrevious;
    IBOutlet UIImageView *imgNextArrow;
}
@property (strong, nonatomic) IBOutlet UITableView *scheduletableview;
@property (strong ,nonatomic) NSString *studio_id;
@property (strong ,nonatomic) NSString *studio_name;
- (IBAction)btnbackaction:(id)sender;
- (IBAction)btnNextDateTapped:(id)sender;
- (IBAction)btnPreviousDateTapped:(id)sender;
@end
