//
//  JScrollView+PageControl+AutoScroll.m
//  demoScrollView+PageControl+AutoScroll
//
//  Created by jacob on 29/7/13.
//  Copyright (c) 2013年 jacob QQ:110773265. All rights reserved.
//

#import "JScrollView+PageControl+AutoScroll.h"
#define arrayLble @[@"FITPASS IS YOUR\nONE - STOP FITNESS\nMEMBERSHIP",@"WORK OUT AT THE\nBEST STUDIOS AND\nGYMS",@"THOUSANDS OF\nCLASSES ALL DAY\nEVERY DAY"]
#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


@implementation JScrollView_PageControl_AutoScroll

@synthesize scrollView=_scrollView;
@synthesize pageControl=_pageControl;
@synthesize currentPage=_currentPage;
@synthesize viewsArray=_viewsArray;
@synthesize autoScrollDelayTime=_autoScrollDelayTime;
@synthesize displayLabel = _displayLabel;
@synthesize lblArray = _lblArray;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];

        _scrollView = [[UIScrollView alloc] initWithFrame:self.frame];//self.bounds
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(self.bounds.size.width * 3, self.bounds.size.height);
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.pagingEnabled = YES;
        [_scrollView addGestureRecognizer:tap];
        [self addSubview:_scrollView];
        
        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"black_bgd"]];
        image.alpha = 0.8;
        [image setFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
        [self addSubview:image];

        _displayLabel = [[UILabel alloc]init];
        _displayLabel.font = [UIFont fontWithName:@"OPENSANS" size:16.0];

        
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        if(iOSDeviceScreenSize.height == 480){
            
            CGRect rect = self.bounds;
            rect.origin.x = -122;
            rect.origin.y = rect.size.height - 310;
            rect.size.height = 25;
            _pageControl = [[UIPageControl alloc] initWithFrame:rect];
            _pageControl.userInteractionEnabled = NO;
            _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:25.0f/255.0f green:196.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            [self addSubview:_pageControl];
            
            CGRect rect1 = self.bounds;
            rect1.origin.x = 35;
            rect1.origin.y = rect1.size.height - 435;
            rect1.size.height = 150;
           [ _displayLabel setFrame:rect1];

            _displayLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _displayLabel.numberOfLines = 0;
            _displayLabel.textColor = [UIColor whiteColor];
            [self addSubview:_displayLabel];

            
        }else if(iOSDeviceScreenSize.height == 568){
            
            CGRect rect = self.bounds;
            rect.origin.x = -138;
            rect.origin.y = rect.size.height - 262;
            rect.size.height = 100;
            _pageControl = [[UIPageControl alloc] initWithFrame:rect];
            _pageControl.userInteractionEnabled = NO;
            _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:25.0f/255.0f green:196.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            [self addSubview:_pageControl];
            
            
           
            CGRect rect1 = self.bounds;
            rect1.origin.x = 20;
            rect1.origin.y = rect1.size.height - 345;
            rect1.size.height = 150;
            [ _displayLabel setFrame:rect1];
            _displayLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _displayLabel.numberOfLines = 0;
            _displayLabel.textColor = [UIColor whiteColor];
            [self addSubview:_displayLabel];
            
        }else if(iOSDeviceScreenSize.height == 667){
            
            CGRect rect = self.bounds;
            rect.origin.x = -138;
            rect.origin.y = rect.size.height - 132;
            rect.size.height = 30;
            _pageControl = [[UIPageControl alloc] initWithFrame:rect];
            _pageControl.userInteractionEnabled = NO;
            _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:25.0f/255.0f green:196.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            [self addSubview:_pageControl];
            
            
            CGRect rect1 = self.bounds;
            rect1.origin.x = 20;
            rect1.origin.y = rect1.size.height - 260;
            rect1.size.height = 150;
            [ _displayLabel setFrame:rect1];
            _displayLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _displayLabel.numberOfLines = 0;
            _displayLabel.textColor = [UIColor whiteColor];
            [self addSubview:_displayLabel];
            
        }else if(iOSDeviceScreenSize.height == 736){
            
            CGRect rect = self.bounds;
            rect.origin.x = -157;
            rect.origin.y = rect.size.height - 132;
            rect.size.height = 30;
            _pageControl = [[UIPageControl alloc] initWithFrame:rect];
            _pageControl.userInteractionEnabled = NO;
            _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:25.0f/255.0f green:196.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            [self addSubview:_pageControl];
            
            
            CGRect rect1 = self.bounds;
            rect1.origin.x = 20;
            rect1.origin.y = rect1.size.height - 260;
            rect1.size.height = 150;
            [ _displayLabel setFrame:rect1];
            _displayLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _displayLabel.numberOfLines = 0;
            _displayLabel.textColor = [UIColor whiteColor];
            [self addSubview:_displayLabel];

        }
        else if(iOSDeviceScreenSize.height == 1024){
            
            CGRect rect = self.bounds;
            rect.origin.x = -250;
            rect.origin.y = rect.size.height - 200;
            rect.size.height = 30;
            _pageControl = [[UIPageControl alloc] initWithFrame:rect];
            _pageControl.userInteractionEnabled = NO;
            _pageControl.pageIndicatorTintColor = [UIColor colorWithRed:25.0f/255.0f green:196.0f/255.0f blue:167.0f/255.0f alpha:1.0f];
            _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
            [self addSubview:_pageControl];
            
            
            CGRect rect1 = self.bounds;
            rect1.origin.x = 104;
            rect1.origin.y = rect1.size.height - 390;
            rect1.size.height = 200;
            
            _displayLabel.font = [UIFont fontWithName:@"OPENSANS" size:30.0];
            [ _displayLabel setFrame:rect1];
            _displayLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _displayLabel.numberOfLines = 0;
            _displayLabel.textColor = [UIColor whiteColor];
            [self addSubview:_displayLabel];
            
        }

      
        
      


    }
    return self;
}

-(void)shouldAutoShow:(BOOL)shouldStart
{
    if (shouldStart) {
        if ([autoScrollTimer isValid]) {
            
        }
        else
            autoScrollTimer=[NSTimer scheduledTimerWithTimeInterval:_autoScrollDelayTime target:self selector:@selector(autoShowNext) userInfo:nil repeats:YES];
    }
    else
    {
        if ([autoScrollTimer isValid]) {
            [autoScrollTimer invalidate];
            autoScrollTimer = nil;
        }
    }
}

-(void)autoShowNext
{
    if (_currentPage+1>=[_viewsArray count]) {
        _currentPage=0;
    }
    else
        _currentPage++;
    // If view one is visible, hides it and add the new one with the "Flip" style transition
    
  
                         [_scrollView setContentOffset:CGPointMake(self.bounds.size.width*2, 0) animated:YES];
                         [_displayLabel setText:[_lblArray objectAtIndex:_currentPage]];
    
    
  
// Commit animations to show the effect

}

-(void)reloadData
{
    [firstView removeFromSuperview];
    [middleView removeFromSuperview];
    [lastView removeFromSuperview];
    
    if (_currentPage==0)
    {
        firstView=[_viewsArray lastObject];
        middleView=[_viewsArray objectAtIndex:_currentPage];
        if ([_viewsArray count] >1) {
            lastView=[_viewsArray objectAtIndex:_currentPage+1];
            
            
          
        }
    }
    else if (_currentPage==[_viewsArray count]-1)
    {
        firstView=[_viewsArray objectAtIndex:_currentPage-1];
        middleView=[_viewsArray objectAtIndex:_currentPage];
        lastView=[_viewsArray objectAtIndex:0];
    }
    else
    {
        firstView=[_viewsArray objectAtIndex:_currentPage-1];
        middleView=[_viewsArray objectAtIndex:_currentPage];
        lastView=[_viewsArray objectAtIndex:_currentPage+1];
    }
    [_pageControl setCurrentPage:_currentPage];
    [_displayLabel setText:[_lblArray objectAtIndex:_currentPage]];
    CGSize scrollSize=_scrollView.bounds.size;
    [firstView setFrame:CGRectMake(0, 0, scrollSize.width, scrollSize.height)];
    [middleView setFrame:CGRectMake(scrollSize.width, 0, scrollSize.width, scrollSize.height)];
    [lastView setFrame:CGRectMake(scrollSize.width*2, 0, scrollSize.width, scrollSize.height)];
    
    [_scrollView addSubview:firstView];
    [_scrollView addSubview:middleView];
    [_scrollView addSubview:lastView];
    [_scrollView setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:NO];
    
    
}


#pragma mark Setter

-(void)setViewsArray:(NSMutableArray *)viewsArray
{
    if (viewsArray) {
        _pageControl.numberOfPages=[viewsArray count];
        _viewsArray=viewsArray;
        _currentPage=0;
        [_pageControl setCurrentPage:_currentPage];
        
    }
    [self reloadData];
}

- (void)setLblArray:(NSMutableArray *)lblArray
{
    if (lblArray) {
        _lblArray=lblArray;
        _currentPage=0;
        [_pageControl setCurrentPage:_currentPage];
        _displayLabel.text = [lblArray objectAtIndex:_currentPage];
        
    }
    [self reloadData];
}

#pragma mark ScrollView Delegate
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    //自动timer滑行后自动替换，不再动画
    [self reloadData];
    
  //   NSInteger pagenumber=_scrollView.bounds.size.width/ _scrollView.contentOffset.x;
  //  _displayLabel.text = [_lblArray objectAtIndex:pagenumber];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //手动滑动自动替换，悬停timer
    [autoScrollTimer invalidate];
    autoScrollTimer=nil;
    autoScrollTimer=[NSTimer scheduledTimerWithTimeInterval:_autoScrollDelayTime target:self selector:@selector(autoShowNext) userInfo:nil repeats:YES];
    int x = scrollView.contentOffset.x;
    NSLog(@"x is %d",x);
    //往下翻一张
    if(x >= (2*self.frame.size.width)) {
        if (_currentPage+1==[_viewsArray count]) {
            _currentPage=0;
        }
        else
            _currentPage++;
    }
    
    //往上翻
    if(x <= 0) {
        if (_currentPage-1<0) {
            _currentPage=[_viewsArray count]-1;
        }
        else
            _currentPage--;
    }
    
    [self reloadData];

}


#pragma protocol

- (void)handleTap:(UITapGestureRecognizer *)tap {
    
    if ([_delegate respondsToSelector:@selector(didClickPage:atIndex:)]) {
        [_delegate didClickPage:self atIndex:_currentPage];
        
       
    }
    
}

@end
