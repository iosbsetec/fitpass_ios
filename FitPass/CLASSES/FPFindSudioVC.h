//
//  MapVC.h
//  MapAnnotation
//
//  Created by bsetec on 7/8/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface FPFindSudioVC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate,UITextFieldDelegate>
{
    CLLocationManager *locmanagerobj;
    CLLocation *locationobj;
   
}

@property (strong ,nonatomic) StudioModel *studioData;
@property (weak, nonatomic) IBOutlet MKMapView *mapviewobj;

@end
