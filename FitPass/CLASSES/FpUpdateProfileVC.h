//
//  FpSettingVC.h
//  FitPass
//
//  Created by JITENDRA on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//
//@Shwetha, can u verify update profile screen http://prntscr.com/82758n
#import <UIKit/UIKit.h>
#import "FPPrivatePolicyVC.h"
#import "FpFAQSVC.h"
#import "FPTermsforUsers.h"
#import "FPAppDelegate.h"

@interface FpUpdateProfileVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>
{
    IBOutlet UIScrollView *scrollViewSettings;
    IBOutlet UITextField *FFirstName,*FEmailId,*FAddress1,*FAddress2,*FCity,*FStateCode,*FZipCode,*FPhoneNo;
    IBOutlet UIButton *btnSave;
    IBOutlet UILabel *lblAccount;
    IBOutlet UILabel *lblContactInfo;
    IBOutlet UILabel *lblEmergencyInfo;
    IBOutlet UIView *viewSegmentController;
    IBOutlet UIImageView *imgUserPhoto;
    IBOutlet UIButton *btnUpdateImg;
    IBOutlet UIButton *btnSelectImg;
    FPAppDelegate *appDelegate;
    IBOutlet UIActivityIndicatorView *actIndicator;
    IBOutlet UIButton *btnMale;
    IBOutlet UIButton *btnUnspecified;
    IBOutlet UIButton *btnFemale;
    BOOL isKeyboardShown;
    
    IBOutlet UIView *viewImgBG;
}
@property(nonatomic,retain)NSString *strFirstName,*strEmailId,*strAddress1,*strAddress2,*strCity,*strStateCode,*strZipCode,*strPhoneNo,*strGender,*strImageUrl,*strEndOfCycle;

-(IBAction)btnUpdateImageTapped:(id)sender;
-(IBAction)btnChangeImageTapped:(id)sender;

-(IBAction)btnSaveTapped:(id)sender;
@end
