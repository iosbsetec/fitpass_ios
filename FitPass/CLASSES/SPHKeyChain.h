//
//  SPHKeyChain.h
//  ChupaChat
//
//  Created by Siba Prasad Hota  on 3/20/14.
//  Copyright (c) 2014 Wemakeappz. All rights reserved.
//

#import <Foundation/Foundation.h>
//@class SimpleKeychainUserPass;

@interface SPHKeyChain : NSObject


+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;



@end
