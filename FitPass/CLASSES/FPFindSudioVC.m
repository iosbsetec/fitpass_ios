//
//  MapVC.m
//  MapAnnotation
//
//  Created by bsetec on 7/8/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "FPFindSudioVC.h"
#import "FBFindStudioCell.h"
#import "FPGetFitVC.h"
#import "FPRefineSearch.h"
#import "FPStudioDetailsVC.h"
#import "YSAPICalls.h"
#import "Generalmodel.h"
#import "Keys.h"
#import "MBProgressHUD.h"
#import "FilterListTable.h"


@interface FPFindSudioVC ()<UITableViewDataSource,UITableViewDelegate,FilterListTableDelegate,FPRefineSearchDelegate>
{
    CLGeocoder *geocoderobj;
    NSMutableArray *studioListArray;
     UIImageView *searchIcon;
     FilterListTable *filterTableView;
    BOOL noDataFound;
    NSString *studio_id;
    NSDictionary *paramDict;
    BOOL scrolled;
    int pagenumberScroll;
    NSString *pagenumber;
    BOOL isLoadingFirstTime,clearData;

    NSMutableDictionary *dictFinalParameter;
    NSDictionary *dictRefineSearch;
}

@property (weak, nonatomic) IBOutlet UITableView *tblFindStudio;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarText;

- (IBAction)btnRefine:(id)sender;


@end

@implementation FPFindSudioVC
@synthesize mapviewobj;

- (void)viewDidLoad {
    [super viewDidLoad];
    scrolled = NO;
    clearData = NO;

    pagenumber =@"1";
    pagenumberScroll=1;
    isLoadingFirstTime = YES;
    //INTIALISATION OF ARRAY
    
    studioListArray = [NSMutableArray array];
    [_tblFindStudio reloadData];
    
    
    [UITextField appearanceWhenContainedIn:[UISearchBar class], nil].leftView = nil;
    _searchBarText.searchTextPositionAdjustment = UIOffsetMake(10, 0);
    searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon_new.png"]];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        searchIcon.frame = CGRectMake(260, 12, 19, 21);
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        searchIcon.frame = CGRectMake(240, 12, 19, 21);
    }
    [_searchBarText addSubview:searchIcon];
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateWithRefineDatatoFindStudio:) name:@"REFINESERACH" object:nil];
    dictRefineSearch = [NSDictionary dictionary];
    // Do any additional setup after loading the view from its nib.
}

-(void)UpdateWithRefineDatatoFindStudio : (NSNotification*) notification
{
    scrolled = NO;
    NSDictionary *dict = [notification userInfo];
    dictRefineSearch = [notification userInfo];
    dictFinalParameter = nil;
    dictFinalParameter = [NSMutableDictionary dictionaryWithDictionary:dict];
    [dictFinalParameter setValue:@"1" forKey:@"page_number"];
    
    [self getStudioList:dictFinalParameter];
}

#pragma mark --- TableView Delagete Start ---

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (scrolled) {
        return [studioListArray count];
    }
     return (noDataFound)?1:[studioListArray count];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return (![studioListArray count])? 140:100.0f;
    }
    else
    {
        return (![studioListArray count])? 120:80.0f;
    }
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        return (noDataFound)? 120:83.0f;
//    }
//    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        return (noDataFound)? 120:104.0f;
//    }
    return (noDataFound)? 120:83.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (noDataFound && !scrolled) {
        
        static NSString *simpleTableIdentifier = @"NoItems";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            
        }
        cell.textLabel.text = @"Sorry, there aren't any more workouts open for reservation today. Try another day?";
        cell.textLabel.font = [UIFont fontWithName:@"OPENSANS" size:14];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.numberOfLines = 2;
        return cell;
    }
    else
    {
    
        static NSString *CellIdentifier = @"FBFindStudioCell";
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CellIdentifier = @"FBFindStudioCell";
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CellIdentifier = @"FBFindStudioCell_ipad";
        }
        
    FBFindStudioCell *cell = (FBFindStudioCell *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FBFindStudioCell" owner:self options:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FBFindStudioCell_ipad" owner:self options:nil];
        }
        cell = [topLevelObjects objectAtIndex:0];
        
    }
    [cell setCellData:[studioListArray objectAtIndex:indexPath.row]];
//    UIView *separatorView;
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 82, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
//    }
//    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 104, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
//    }
//    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    separatorView.alpha = 0.5;
//    separatorView.layer.borderWidth = 1.0;
//    [cell addSubview:separatorView];

    return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    // pothi
    FPStudioDetailsVC *obj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC_ipad" bundle:nil];
    }
    obj.studioData = [studioListArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:obj animated:YES];
}

#pragma mark --- TableView Delagete End ---


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    scrolled = NO;
    
    [super viewDidAppear:animated];
    if (isLoadingFirstTime)
    {
        dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",pagenumber,@"page_number",nil];
        [self getStudioList :dictFinalParameter];
    }
    else  if (clearData) {
        [self getStudioList:dictFinalParameter];
        clearData = NO;
    }

    isLoadingFirstTime = NO;
}

-(void)getStudioList: (NSDictionary *)dic
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // NSDictionary *paramDict=[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studio_id,@"studio_id",nil];
    
    [YSAPICalls getAllStudioListwithParamDict:dic SuccessionBlock:^(id newResponse) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

        Generalmodel *model = newResponse;
        [_searchBarText resignFirstResponder];
        
        if ([model.status_code isEqualToString:@"1"])
        {
       
            if (model.tempArray)
            {
                noDataFound = NO;

                if (scrolled) {
                    [studioListArray addObjectsFromArray:model.tempArray];
                }
                else{
                if ([studioListArray count])
                {
                    [studioListArray removeAllObjects];
                }
                    [studioListArray addObjectsFromArray:model.tempArray];

                }

            }
        }
            else
            {
                noDataFound = YES;
            }
       
        
        NSLog(@"studioListArray %@",studioListArray);
        [_tblFindStudio reloadData];
        [MBProgressHUD  hideHUDForView:self.view animated:YES];
        
        
    } andFailureBlock:^(NSError *error){
         [_searchBarText resignFirstResponder];
        
[MBProgressHUD  hideHUDForView:self.view animated:YES];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRefine:(id)sender
{
    FPRefineSearch *refineobj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        refineobj = [[FPRefineSearch alloc]initWithNibName:@"FPRefineSearch" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        refineobj = [[FPRefineSearch alloc]initWithNibName:@"FPRefineSearch_ipad" bundle:nil];
    }
    refineobj.delegate = self;
    [self.navigationController pushViewController:refineobj animated:YES];

}

#pragma THIS CODE FOR FILETR VIEW

- (void) FilterListTableDelegateMethod: (FilterListTable *) sender selectedIndex :(NSString *) studioId andText:(NSString *)textData
{
    NSLog(@"S _id =%@",studioId);
    studio_id = studioId;
    paramDict = nil;
    
    if ([[dictRefineSearch allKeys] count]>1) {
        
        dictFinalParameter = nil;
        dictFinalParameter = [NSMutableDictionary dictionaryWithDictionary:dictRefineSearch];
        [dictFinalParameter setValue:@"1" forKey:@"page_number"];
        [dictFinalParameter setValue:studioId forKey:@"studio_id"];
        
        [self getStudioList :dictFinalParameter];
        
    }
    else
    {
        paramDict   = [NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",@"1",@"page_number",studio_id,@"studio_id",nil];
        [self  getStudioList:paramDict];
    }
    _searchBarText.text = textData;
    [filterTableView hideDropDown];
}

#pragma mark --- SearchBar Delegate *** Start *** ----

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

-(IBAction)showStudioFilter
{
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds isAlreadySelected:NO];
    filterTableView.delegate = self;
}

- (void) FilterListTableDelegateMethodDone
{
    [_searchBarText resignFirstResponder];
}



- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    UITextField *textField = [searchBar valueForKey:@"_searchField"];
    textField.clearButtonMode = UITextFieldViewModeNever;
    
    [self showStudioFilter];
    //[searchIcon removeFromSuperview];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    
    [_searchBarText addSubview:searchIcon];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Cancel");
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"Text Changing");
    
}

- (void) clearData
{
    clearData = YES;
    dictRefineSearch = nil;
    dictFinalParameter = nil;
    
    dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",pagenumber,@"page_number",nil];
}

#pragma mark --- SearchBar Delegate *** End *** ----

#pragma mark --- ScrollView end Scroll ---
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight          = scrollView.frame.size.height;
    float scrollContentSizeHeight   = scrollView.contentSize.height;
    float scrollOffset              = scrollView.contentOffset.y;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
        NSLog(@"**** Top ***");
    }
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        NSLog(@"**** End ***");
        if (!noDataFound) {
            pagenumberScroll = pagenumberScroll +1;
            scrolled = YES;
            NSLog(@"pagenumberScroll %d",pagenumberScroll);
            pagenumber = [NSString stringWithFormat:@"%d",pagenumberScroll];
            [dictFinalParameter setValue:pagenumber forKey:@"page_number"];
            //[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",                               [self selectedDate:newDate1],@"date_of_workout",pagenumber,@"page_number",nil]
            [self getStudioList :dictFinalParameter];
        }
    }
}


@end
