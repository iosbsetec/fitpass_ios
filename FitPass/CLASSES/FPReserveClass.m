//
//  FPReserveClass.m
//  FitPass
//
//  Created by JITEN on 7/23/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPReserveClass.h"
#import <CoreImage/CoreImage.h>
#import <QuartzCore/QuartzCore.h>
#import "SPHServiceRequest.h"
#import "ReserveModel.h"
#import "Generalmodel.h"
#import "AsyncImageView.h"
#import "UIImageEffects.h"
#import "MBProgressHUD.h"
@interface FPReserveClass ()
{
    BOOL  isReserveDetailsRetrive;


}

- (IBAction)btnClose:(id)sender;

@property (strong, nonatomic)  ReserveModel *schedule_date;
@property (strong, nonatomic)  ReserveModel *reserveData;
@property (weak, nonatomic) IBOutlet UIButton *btnNorefference;
@property (weak, nonatomic) IBOutlet UIButton *btnYesReference;

@end

@implementation FPReserveClass

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    // _imgBluredImage.image = [UIImage imageNamed:@"4.jpg"];
    //rgb(255,99,104)
    //[UIColor colorWithRed:16.0/255 green:176.0/255 blue:230.0/255 alpha:1];

//    NSLog(@"%@",_workoutData.work_out_pic);
//    UIImage *effectImage = nil;
//    effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_workoutData.work_out_pic]]]];
//    [_imgBluredImage setImage:effectImage];
    

    _viewYESNO.layer.borderColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1].CGColor;
    _viewYESNO.layer.borderWidth = 1.0f;
    _viewYESNO.layer.cornerRadius = 5.0f;
    _viewYESNO.clipsToBounds = YES;
   
    _btnconformation.layer.borderColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1].CGColor;
    _btnconformation.layer.cornerRadius = 5.0f;
    _btnconformation.clipsToBounds = YES;
    _btnNorefference.backgroundColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1];
   // [self setScreenData];
}

-(void)setScreenData
{
//    _imgBluredImage.imageURL =  [NSURL URLWithString:_workoutData.work_out_pic];
//    _lblClassName.text  = _workoutData.studio_name;
//
//    
//    //lblstudioTitles.text = studioData.studio_name;
//    // lblDate.text = studioData.date_of_workout;
//    _lblTime.text = [NSString stringWithFormat:@"%@ - %@",_workoutData.start_time,_workoutData.end_time];
//
//    _lblDate.text= _selectedDate;

    if ([_workoutData.batch_status integerValue]==1)
    {
        [_btnconformation setBackgroundColor:[UIColor clearColor]];
        _btnconformation.layer.borderColor = [UIColor redColor].CGColor;
        [_btnconformation setTitle:@"YES, PLEASE CANCEL" forState:UIControlStateNormal];
        _btnconformation.titleLabel.text = @"YES, PLEASE CANCEL";
    }



}


-(IBAction)onConfirmation:(UIButton *)sender
{
    NSLog(@"hi%@hi",[[sender titleLabel] text]);
    if ([[[sender titleLabel] text] isEqualToString:@"YES, PLEASE CANCEL"])
    {
        [self getCancelDetails];
    }
    else
    {
        [self getReserveDetails];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addworkoutdetailstocalendar" object:nil];
    }
}

-(void)getReserveDetails
{
    //    location_id=399&batch_id=793&user_id=1951&schedule_date=2015-07-20&studio_id=1841
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }

    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&studio_id=%@&location_id=%@&batch_id=%@&schedule_date=%@",FITPASS_USER_ID,_workoutData.studio_id,_workoutData.location_id,_workoutData.batch_id,_workoutData.date_of_workout]dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"str is %@",str);
    
    
    [YSAPICalls reserveworkout:submitData SuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
         Generalmodel *model = newResponse;
         
         if ([model.status_code integerValue]) {
             
             [self btnClose:nil];
         }
         
     } andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];
     }];

}
-(void)getCancelDetails
{
    //    location_id=399&batch_id=793&user_id=1951&schedule_date=2015-07-20&studio_id=1841
    
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&schedule_id=%@",FITPASS_USER_ID,_workoutData.schedule_id]dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *str = [[NSString alloc]initWithData:submitData encoding:NSUTF8StringEncoding];
    NSLog(@"str is %@",str);
    
    [YSAPICalls cancelworkout:submitData SuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

         Generalmodel *model = newResponse;
         
         if ([model.status_code integerValue]) {
             
             [self btnClose:nil];
         }
         
     } andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnYes:(id)sender
{
    _btnYesReference.backgroundColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1];
    _btnNorefference.backgroundColor = [UIColor clearColor];
    [self userPastExp:YES];

}

- (IBAction)btnNo:(id)sender
{
    _btnNorefference.backgroundColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1];
    _btnYesReference.backgroundColor = [UIColor clearColor];
    [self userPastExp:NO];
}

-(void)userPastExp : (BOOL) yesno
{

    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
//    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&studio_id=%@&status=%@",FITPASS_USER_ID,_workoutData.studio_id,(yesno)?@"Yes":@"No"]dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls userpastExperience:submitData SuccessionBlock:^(id newResponse)
     {
//         [MBProgressHUD hideHUDForView:self.view animated:YES];

         Generalmodel *model = newResponse;
       
         if ([model.status_code integerValue]) {
             
         }
     
     } andFailureBlock:^(NSError *error) {
         [MBProgressHUD hideHUDForView:self.view animated:YES];

     }];


}
- (IBAction)btnClose:(id)sender
{
  [[NSNotificationCenter defaultCenter] postNotificationName:@"closeReservation" object:nil];
}

@end
