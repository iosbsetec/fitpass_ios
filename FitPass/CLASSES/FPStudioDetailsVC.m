//
//  FPFindStudioVC.m
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

typedef enum{
    iPadRetina,iPadNoRetina,iPhoneiPod35InchRetina,iPhoneiPod35InchNoRetina,iPhoneiPod4InchRetina}DeviceType;


#import "FPStudioDetailsVC.h"
#import "FPStudioDetailsScheduleVC.h"
#import "YSAPICalls.h"
#import "Keys.h"
#import "Generalmodel.h"
#import "StudioModel.h"
#import "AsyncImageView.h"

@interface FPStudioDetailsVC ()
{
    __weak IBOutlet UIImageView *imgVStudio;

    __weak IBOutlet UIImageView *imgVLike,*imgVAirconditioner,*imgVCarparking;

    __weak IBOutlet UILabel *lblStudioName;

    __weak IBOutlet UILabel *lblLocation;
    __weak IBOutlet UILabel *lblRatingValue;
    __weak IBOutlet UILabel *lblNearestMetroStation;
    __weak IBOutlet UILabel *lblAddress1;
    __weak IBOutlet UILabel *lblAddress2;
    __weak IBOutlet UILabel *lbllocality;
    __weak IBOutlet UILabel *lblAbout;
    __weak IBOutlet UITextView *txtViewAbout;
    __weak IBOutlet UILabel *lblActivity;
    
    CLGeocoder *geocoderobj;
    __weak IBOutlet UIButton *btnaddFavorateReference;

    BOOL isStudioDetailsRetrive,   isLoadingFirstTime;
    NSString *addfavorateStatus,*studio_id;
    IBOutlet MKMapView *mapviewobj;
    CGRect newtxtAboutRect;
    CGRect newlblActivitiesRect;
}


- (IBAction)btnAddfavorates:(id)sender;


@end

@implementation FPStudioDetailsVC
@synthesize studiodetailsscrollview;

- (void)viewDidLoad {
    [super viewDidLoad];
    lblratings.layer.borderColor = [UIColor whiteColor].CGColor;
    lblratings.layer.borderWidth = 2.0;
    [self devicesize];
    // Do any additional setup after loading the view from its nib.
    
     NSLog(@"Studioe id %@",_studeo_id);
    if ([_studioData.studio_id length]) {
        [self getStudioDetails];
    }
    else
        [self setScreenData:_studioData];
    //IBOUTLETS
    isLoadingFirstTime = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (isLoadingFirstTime)
        [self getStudioDetails];
    isLoadingFirstTime = NO;
    NSLog(@"Studioe id %@",_studeo_id);
    //studeoName = _studioData.studio_name;

}
-(void)getStudioDetails
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    //[YSAPICalls getStudioDetailsWithStudioID:_studioData.studio_id SuccessionBlock:^(id newResponse)
    NSString *temp_studio_id;
    if ([_studioData.studio_id length]) {
        temp_studio_id = _studioData.studio_id;
    }
    else
        temp_studio_id = _studeo_id;
    
    [YSAPICalls getStudioDetailsWithStudioID:temp_studio_id SuccessionBlock:^(id newResponse) {
        isStudioDetailsRetrive = YES;
        Generalmodel *model = newResponse;
        _studioData = [model.tempArray lastObject];
        
        [self setScreenData:_studioData];
        [self setMapData];

    } andFailureBlock:^(NSError *error) {
        
    }];
}


-(void)setScreenData : (StudioModel *)studioData
{
    imgVStudio.imageURL =  [NSURL URLWithString:studioData.profile_pic];
    lblStudioName.text  = studioData.studio_name;
    lblNearestMetroStation.text  = studioData.nearest_metrostation;
    lblAddress1.text  = studioData.address_line1;
    lblAddress2.text  = studioData.address_line2;
    lbllocality.text  = studioData.locality_name;
    lblRatingValue.text = [NSString stringWithFormat:@"%@/%@",studioData.rating,@"5"];
    lblActivity.text    = studioData.activities;
    //  activities
   
    studio_id = studioData.studio_id;
    addfavorateStatus = studioData.favourites_status;
    
    if ([addfavorateStatus isEqualToString:@"1"])
    {
        [btnaddFavorateReference setBackgroundImage:[UIImage imageNamed:@"35x35_icon_like_white.png"] forState:UIControlStateNormal];
    }
    else
    {
       [btnaddFavorateReference setBackgroundImage:[UIImage imageNamed:@"icon_like_white_border.png"] forState:UIControlStateNormal];
    }
    
    imgVLike.image  = ([studioData.favourites_status length])?[UIImage imageNamed:@"35x35_icon_like_white.png"]:[UIImage imageNamed:@"icon_like_white_border.png"];
    lblLocation.text = studioData.locality_name;
    
    UIFont *font;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        font = [UIFont fontWithName:@"OPENSANS" size:13.0];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        font = [UIFont fontWithName:@"OPENSANS" size:19.0];
    }
    
    NSMutableAttributedString *strAbout = [[NSMutableAttributedString alloc] initWithData:[studioData.about_studio dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setAlignment:NSTextAlignmentJustified];
    
    [strAbout addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, strAbout.length)];
    
    [strAbout addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, [strAbout length])];
    
    NSLog(@"%@",strAbout);

//    NSString *str =  [[NSString alloc] initWithData:studioData.about_studio encoding:NSUTF8StringEncoding];
    txtViewAbout.attributedText =strAbout ;
    txtViewAbout.userInteractionEnabled = NO;
    
    newtxtAboutRect = [[txtViewAbout.attributedText string] boundingRectWithSize:CGSizeMake(txtViewAbout.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];

    newlblActivitiesRect = [lblActivity.text boundingRectWithSize:CGSizeMake(lblActivity.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: font} context:nil];
    
    CGRect rect = txtViewAbout.frame;
    CGRect rectlblAct = lblActivity.frame;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        rect.size.height = newtxtAboutRect.size.height+40;
        rectlblAct.size.height = newlblActivitiesRect.size.height+5;
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        if (txtViewAbout.frame.size.height>109)
            rect.size.height = newtxtAboutRect.size.height+40;
        else
            rect.size.height = newtxtAboutRect.size.height+20;
        
        rectlblAct.size.height = newlblActivitiesRect.size.height+10;
    }
    [lblActivity setFrame:rectlblAct];
    
//    lblActivity.layer.borderColor = [UIColor redColor].CGColor;
//    lblActivity.layer.borderWidth = 2.0;
    [txtViewAbout setFrame:rect];
    
 
    CGRect rectViewAme = viewAmenities.frame;
    rectViewAme.origin.y = lblActivity.frame.origin.y+rectlblAct.size.height+10;
    [viewAmenities setFrame:rectViewAme];
    
    CGRect rectViewtxtAbout = txtViewAbout.frame;
    rectViewtxtAbout.origin.y = viewAmenities.frame.origin.y+viewAmenities.frame.size.height;
    [txtViewAbout setFrame:rectViewtxtAbout];
    
    CGRect rectViewLoc = viewLocation.frame;
    rectViewLoc.origin.y = txtViewAbout.frame.origin.y+rect.size.height;
    [viewLocation setFrame:rectViewLoc];
    
    [self devicesize];
    imgVAirconditioner.image = ([studioData.air_conditioner isEqualToString:@"Yes"])? [UIImage imageNamed:@"icon_white_air_conditioned.png"]:[UIImage imageNamed:@"icon_bg_gray_conditioned"];

    imgVCarparking.image = ([studioData.parking_facility isEqualToString:@"Yes"])? [UIImage imageNamed:@"icon_white_parking.png"]:[UIImage imageNamed:@"icon_bg_gray_parking"];
}

-(void)devicesize
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
        {
            if (txtViewAbout.frame.size.height>103)
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 850+newtxtAboutRect.size.height+40)];
            else
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 955)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height >= 667.0f)
        {
            if (lblActivity.frame.size.height>75 && txtViewAbout.frame.size.height>103)
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 870+newtxtAboutRect.size.height+50)];
            else if (txtViewAbout.frame.size.height>103)
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 840+newtxtAboutRect.size.height+10)];
            else
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 900)];
        }
        else
        {
            if (lblActivity.frame.size.height>75 && txtViewAbout.frame.size.height>103)
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 870+newtxtAboutRect.size.height+50)];
            else if (txtViewAbout.frame.size.height>103)
                [studiodetailsscrollview setContentSize:CGSizeMake(320, 1340+newtxtAboutRect.size.height+15)];
            else
                [studiodetailsscrollview setContentSize:CGSizeMake(320,1440)];
        }
    }
    else if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        if (lblActivity.frame.size.height>90 && txtViewAbout.frame.size.height>109)
            [studiodetailsscrollview setContentSize:CGSizeMake(768, 1381+newtxtAboutRect.size.height+30)];
        else if (txtViewAbout.frame.size.height>109)
            [studiodetailsscrollview setContentSize:CGSizeMake(768, 1381+newtxtAboutRect.size.height)];
        else
            [studiodetailsscrollview setContentSize:CGSizeMake(768,1440)];
    }
}

-(void)setMapData
{
    locmanagerobj = [[CLLocationManager alloc]init];
    geocoderobj = [[CLGeocoder alloc]init];
    locmanagerobj.delegate = self;
    locmanagerobj.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locmanagerobj.distanceFilter = kCLDistanceFilterNone;
    if ([locmanagerobj respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locmanagerobj requestWhenInUseAuthorization];
    }
    if ([locmanagerobj respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locmanagerobj requestAlwaysAuthorization];
    }
    [locmanagerobj startUpdatingLocation];

    CLLocationCoordinate2D location;
    location.latitude = [_studioData.latitude floatValue];
    location.longitude = [_studioData.longitude floatValue];
    mapviewobj.region = MKCoordinateRegionMakeWithDistance(location, 800, 800);

    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation  setCoordinate:location];
    [mapviewobj addAnnotation:annotation];
//    [mapviewobj setUserInteractionEnabled:NO];
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    static NSString *identifier = @"MyLocation";
//    if ([annotation isKindOfClass:[MyLocation class]]) {
//        
        MKAnnotationView *annotationView = (MKAnnotationView *) [mapviewobj dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (annotationView == nil) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];

            annotationView.enabled = YES;
            annotationView.canShowCallout = YES;
            annotationView.image = [UIImage imageNamed:@"map_icon.png"];//here we use a nice image instead of the default pins

        } else {
            annotationView.annotation = annotation;
        }
        
        return annotationView;
   // }
    
}

- (IBAction)btnbackaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnscheduleaction:(id)sender {
    
    FPStudioDetailsScheduleVC *StudioDetailsSchedule;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        StudioDetailsSchedule = [[FPStudioDetailsScheduleVC alloc] initWithNibName:@"FPStudioDetailsScheduleVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        StudioDetailsSchedule = [[FPStudioDetailsScheduleVC alloc] initWithNibName:@"FPStudioDetailsScheduleVC_ipad" bundle:nil];
    }
    
    StudioDetailsSchedule.studio_id = _studioData.studio_id;
    StudioDetailsSchedule.studio_name = _studioData.studio_name;
    [self.navigationController pushViewController:StudioDetailsSchedule animated:YES];
    
}
-(void)yourCustomFunctionThatNeedsToKnowDeviceType
{
    NSLog(@"device type = %i",[self getDeviceType]);
    
    switch ([self getDeviceType])
    {
        case iPadRetina:
        {
            NSLog(@"This device is one of the following: iPad 3, iPad 4");
            break;
        }
        case iPadNoRetina:
        {
            NSLog(@"This device is one of the following: iPad 1, iPad 2, iPad mini");
            break;
        }
        case iPhoneiPod35InchRetina:
        {
            NSLog(@"This device is one of the following: iPhone 4/4S or iPod Touch 4th Generation");
            break;
        }
        case iPhoneiPod35InchNoRetina:
        {
            NSLog(@"This device is one of the following: iPhone 3G/3GS or iPod Touch 3rd Generation");
            break;
        }
        case iPhoneiPod4InchRetina:
        {
            NSLog(@"This device is one of the following: iPhone 5 or iPod Touch 5th Generation");
            break;
        }
    }
}

-(int)getDeviceType
{
    // Get the ratio of the device's screen (height/width)
    CGFloat screenRatio = [UIScreen mainScreen].bounds.size.height/[UIScreen mainScreen].bounds.size.width;
    
    // Initialize return value to negative value
    DeviceType type = -1;
    if(screenRatio > 1.5)
    {
        /*
         4.0-Inch Screen
         This implies that the device is either an iPhone 5 or a 5th generation iPod
         Retina display is implicit
         */
        type = iPhoneiPod4InchRetina;
    }
    else
    {
        /*
         Device must be iPad 1/2/3/4/mini or iPhone 4/4S or iPhone 3G/3GS
         */
        
        // Take a screenshot to determine if the device has retina display or not
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, 0.0);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIImage *scaleCheckImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)
        {
            /*
             Device must be iPad 1/2/3/4/mini
             */
            if(scaleCheckImage.scale == 1)
            {
                // iPad 1/2/mini (No Retina)
                type = iPadNoRetina;
            }
            else if(scaleCheckImage.scale == 2)
            {
                // iPad 3/4 (Retina)
                type = iPadRetina;
            }
        }
        else
        {
            /*
             Device must be iPhone 4/4S or iPhone 3G/3GS or iPod Touch 3rd Generation or iPod Touch 4th Generation
             */
            if(scaleCheckImage.scale == 1)
            {
                // iPhone 3G/3GS or iPod Touch 3rd Generation (No Retina)
                type = iPhoneiPod35InchNoRetina;
            }
            else if(scaleCheckImage.scale == 2)
            {
                // iPhone 4/4S or iPod Touch 4th Generation (Retina)
                type = iPhoneiPod35InchRetina;
            }
        }
    }
    return type;
}
- (IBAction)btnAddfavorates:(id)sender
{
    NSLog(@"Add Favorate");

    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    NSData *submitData = [[NSString stringWithFormat:@"favourites_status=%@&user_id=%@&studio_id=%@",([addfavorateStatus integerValue]== 1)?@"2":@"1",FITPASS_USER_ID,studio_id] dataUsingEncoding:NSUTF8StringEncoding];
        
        [YSAPICalls getaddfavouritesstudioDetailsParamData:submitData SuccessionBlock:^(id newResponse)
         {
             Generalmodel *model = newResponse;
             NSLog(@"Status %@",model.status);
             if ([model.status isEqualToString:@"1"])
             {
                 [btnaddFavorateReference setBackgroundImage:[UIImage imageNamed:@"35x35_icon_like_white.png"] forState:UIControlStateNormal];
             }
             else
             {
                 [btnaddFavorateReference setBackgroundImage:[UIImage imageNamed:@"icon_like_white_border.png"] forState:UIControlStateNormal];
             }
              [self getStudioDetails];
             
         } andFailureBlock:^(NSError *error) {
             //[viewDescription setHidden:NO];
         }];
    
}
@end
