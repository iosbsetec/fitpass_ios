//
//  AppDelegate.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//
#define kMultideviceAlert @"You are logged in on another device. For security purposes, you have been logged out from here."
#import "FPAppDelegate.H"
#import "FPMenuVC.h"
#import "HomeVC.h"
#import <CoreLocation/CoreLocation.h>

@interface FPAppDelegate ()<UIApplicationDelegate>
{
    NSTimer *idleTimer;
    UILabel *lblMessage;
}
@end

@implementation FPAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert|UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert |UIUserNotificationTypeBadge)];
    }


    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        NSLog(@"notification info %@",notification);
        [YSSupport resetDefaultsLogout];
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0 ;
    }
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *userId = [defaults objectForKey:@"Userid"];
    if (userId.length>0)
    {
        FPMenuVC *rootController;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
        }
        
        _navigationController = [[UINavigationController alloc] initWithRootViewController:rootController];
    }
    else
    {
        HomeVC *rootController;
        
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            rootController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            rootController = [[HomeVC alloc] initWithNibName:@"HomeVC_ipad" bundle:nil];
        }
        
//        FPMenuVC *rootController;
//        
//        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//        {
//            rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
//        }
//        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//        {
//            rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
//        }
        
        _navigationController = [[UINavigationController alloc]
                                 initWithRootViewController:rootController];
    }
    
    [_navigationController setNavigationBarHidden:YES animated:NO];
    
    self.window = [[UIWindow alloc]
                   initWithFrame:[[UIScreen mainScreen] bounds]];
    
    [self.window setRootViewController:_navigationController];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.window makeKeyAndVisible];
    self.window.backgroundColor = [UIColor whiteColor];
    
    sleep(2.0);
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString* deviceTokenData = [[[[deviceToken description]
                                   stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                  stringByReplacingOccurrencesOfString: @">" withString: @""]
                                 stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Device_Token     -----> %@\n",deviceTokenData);
    
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenData forKey:DEVICE_TOKEN];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"User info %@",userInfo);
    [YSSupport resetDefaultsLogout];
    NSLog(@"viewcontrollers %@",self.navigationController.viewControllers);
    
    if (![[self.navigationController topViewController] isKindOfClass:[HomeVC class]]) {
        showAlert(@"Alert!", kMultideviceAlert, @"Ok", nil);
    }
        self.window.rootViewController  = nil;
        [self setRootViewController];
}

- (void) setRootViewController
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    HomeVC *rootController;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        rootController = [[HomeVC alloc] initWithNibName:@"HomeVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rootController = [[HomeVC alloc] initWithNibName:@"HomeVC_ipad" bundle:nil];
    }
    
    _navigationController = [[UINavigationController alloc] initWithRootViewController:rootController];
     [_navigationController setNavigationBarHidden:YES animated:NO];

    self.window.rootViewController = _navigationController;
    [self.window makeKeyAndVisible];
}

#pragma mark -
-(void)onCreateToastMsgInWindow:(NSString *)strMsg
{
    //    [viewAlertText setFrame:CGRectMake(0, -viewAlertText.frame.size.height, viewAlertText.frame.size.height, viewAlertText.frame.size.height)];
    //    [self.view addSubview:viewAlertText];
    //    if ([[self.window viewWithTag:123]superview])
    //    {
    //        [[self.window viewWithTag:123]removeFromSuperview];
    //    }
    if ([[self.window viewWithTag:123]superview])
        return;
    
    CGRect newtxtAboutRect = [strMsg boundingRectWithSize:CGSizeMake(self.window.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"OPENSANS" size:13]} context:nil];
    
    if (newtxtAboutRect.size.height>50)
    {
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, -70, self.window.frame.size.width, 70)];
        }
        else
            lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, -50, self.window.frame.size.width, 50)];
    }
    else
        lblMessage = [[UILabel alloc]initWithFrame:CGRectMake(0, -50, self.window.frame.size.width, 50)];
    
    lblMessage.backgroundColor = TILLDCOLOR;
    lblMessage.text = strMsg;
    lblMessage.textColor = [UIColor whiteColor];
    lblMessage.textAlignment = NSTextAlignmentCenter;
    lblMessage.font = [UIFont fontWithName:@"OPENSANS" size:13];
    // lblMessage.layer.cornerRadius = 20;
    [lblMessage setClipsToBounds:YES];
    lblMessage.tag = 123;
    lblMessage.numberOfLines = 0;
    [self.window addSubview:lblMessage];
    [self performSelector:@selector(showViewAlertText) withObject:nil afterDelay:0.0];
    
    //  [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(onClearToastMsg) userInfo:nil repeats:NO];
}

-(void)onClearToastMsg
{
    [[self.window viewWithTag:123]removeFromSuperview];
}

-(void)showViewAlertText
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = lblMessage.frame;
        rect.origin.y = rect.origin.y + lblMessage.frame.size.height;
        [lblMessage setFrame:rect];
        [self performSelector:@selector(viewRemoveFromSuperView) withObject:nil afterDelay:2.0];
    }];
}

-(void)viewRemoveFromSuperView
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect rect = lblMessage.frame;
        rect.origin.y = rect.origin.y -lblMessage.frame.size.height;
        [lblMessage setFrame:rect];
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:YES];
    }
                     completion:^(BOOL finished){
                         [[self.window viewWithTag:123]removeFromSuperview];
                     }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  [FBSDKAppEvents activateApp];
//    [self getActivityList];
//    [self getNearByLocation];
//    [self getStudios];
//    
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
//    NSLog(@"%@",basePath);
    
    if ([GETVALUE(@"Userid") length])
    {

        NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&device_id=%@",GETVALUE(@"Userid"),[self getDeviceID]] dataUsingEncoding:NSUTF8StringEncoding];
        
        [YSAPICalls authentication:submitData SuccessionBlock:^(id newResponse)
         {
            
             // login_status: 2=>Current user already login other device
            // login_status: 1=>Current user not login any device
             
             Generalmodel *model = newResponse;
             
             if ([model.login_status integerValue] == 2) {
                 
                 [YSSupport resetDefaultsLogout];

                 if (![[self.navigationController topViewController] isKindOfClass:[HomeVC class]]) {
                     showAlert(@"Alert!", kMultideviceAlert, @"Ok", nil);
                 }
                
                     self.window.rootViewController  = nil;
                     [self setRootViewController];
               
             }
             
         } andFailureBlock:^(NSError *error) {
             
         }];
    }
    
    if([CLLocationManager locationServicesEnabled])
    {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"locationStatusCheck" object:nil];
        }
    }
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

#pragma mark --- GetActivityList ---
-(void)getActivityList
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [YSAPICalls getWorkOutActivitywithSuccessionBlock:^(id newResponse) {
        Generalmodel *model = newResponse;
        NSLog(@"Gm =%@",model.tempArray);
        
    } andFailureBlock:^(NSError *error) {
        
    }];
}

#pragma mark --- GetNearByLocation ---
-(void)getNearByLocation
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [YSAPICalls getWorkOutNearLocationwithSuccessionBlock:^(id newResponse)
     {
         Generalmodel *model = newResponse;
         NSLog(@"Gm =%@",model.tempArray);

//         [nearbylocationArray removeAllObjects];
//         [nearbylocationArray addObjectsFromArray:model.tempArray];
     } andFailureBlock:^(NSError *error) {
         
     }];
}


#pragma mark --- GetStudios ---
-(void)getStudios
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [YSAPICalls getStudiosSuccessionBlock:^(id newResponse) {
        
    } andFailureBlock:^(NSError *error) {
        
    }];
}


- (void)sendEvent:(UIEvent *)event {
//    [super sendEvent:event];
    [self sendEvent:event];
    
    // Do whatever you want
}

-(NSString*)getDeviceID
{
    id getvalue=[SPHKeyChain load:@"SavedKeyChainData"];
    if ([getvalue respondsToSelector:@selector(objectForKey:)])
    {
        return ([[getvalue valueForKey:@"ConstantUDID"] length]<1)?[self generateUUID]:[getvalue valueForKey:@"ConstantUDID"];
    }
    else{
        return [self generateUUID];
    }
}


-(NSString*)generateUUID
{
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    NSString *UniqueID = [oNSUUID UUIDString];
    NSMutableDictionary *firstNameDict=[NSMutableDictionary new];
    [firstNameDict setObject:UniqueID forKey:@"ConstantUDID"];
    [SPHKeyChain save:@"SavedKeyChainData" data:firstNameDict];
    return UniqueID;
}
@end
