//
//  FPFindClassVC.m
//  FitPass
//
//  Created by JITENDRA on 7/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//




#import "AMTumblrHud.h"
#import "FPFindClassVC.h"
#import "CellFindClass.h"
//#import "FPMapVC.h"
#import "FPRefineSearch.h"
#import "WorkOutDetailsVC.h"
#import "FPGetFitVC.h"
#import "AADatePicker.h"
#import "Keys.h"
#import "Generalmodel.h"
#import "WorkOutModel.h"
#import "WorkOutDetailsVC.h"
#import "YSAPICalls.h"
#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
#import "FilterListTable.h"
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";
#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view


@interface FPFindClassVC ()<UISearchBarDelegate,AADatePickerDelegate,FilterListTableDelegate,FPRefineSearchDelegate>
{
    IBOutlet UITableView *tblData;
    IBOutlet UIView *viewDatePicker;
    NSString *str;
    IBOutlet UIButton *btnMap;
    UIImageView *searchIcon;
    NSArray *arrSelectedDate;
    AADatePicker *datePicker;
    IBOutlet UILabel *lblDate;
    NSDate *newDate1;
    NSMutableArray *workOutListArray;
    //for checking the response firsttime loading
    BOOL isLoadingFirstTime,isServerFinisher;
    
    NSString *selectedDate;
    NSString *selectedDateDisplay;

    NSDate *pickerDate;
    BOOL noDataFound;
    AMTumblrHud *tumblrHUD;
    int pagenumberScroll;
    NSString *pagenumber;
    BOOL scrolled;
    
    FilterListTable *filterTableView;
    IBOutlet UITableViewCell *cellNoData;
    
    NSMutableDictionary *dictFinalParameter;
    NSDictionary *dictRefineSearch;
    NSString *selectedStudioId;

    BOOL clearData;
    
    
}
// keep track which indexPath points to the cell with UIDatePicker



- (IBAction)searchTapped:(UIButton *)sender;
@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (strong, nonatomic) IBOutlet UIButton *btnDate;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarText;

@property (assign) NSInteger pickerCellRowHeight;

@property (nonatomic, strong) IBOutlet UIDatePicker *pickerView;
// this button appears only when the date picker is shown (iOS 6.1.x or earlier)
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;
- (IBAction)dateButtonTapped:(UIButton *)sender;

- (IBAction)refineSearchTapped:(UIButton *)sender;
- (IBAction)btnGetFit:(id)sender;

@end

@implementation FPFindClassVC
@synthesize isComeFromFavorite;


#pragma mark -VIEW METHIODS
- (void)viewDidLoad
{
    [super viewDidLoad];
    btnMap.hidden = YES;
    nIncOrDecDate = 0;
    clearData = NO;
    [lblFavoriteWorkout setHidden:YES];
    if (isComeFromFavorite)
    {
        [imgRefineSearch setHidden:YES];
        [btnRefineSearch setHidden:YES];

        [lblFavoriteWorkout setHidden:NO];
        [btnSearchButton setHidden:YES];
        [_searchBarText setHidden:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(UpdateWithRefineData:) name:@"REFINESERACH" object:nil];
    cellNoData = NO;
    
    pagenumber = @"1";
    scrolled = NO;
    isServerFinisher = NO;

    pagenumberScroll = 1;
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"0~~0" forKey:@"selectdaterow"];
    [defaults synchronize];
    
    btnPreviousDate.userInteractionEnabled = NO;
    [imgPrevious setHidden:YES];
    
    pickerDate = [NSDate date];
    lblDate.text=[NSString stringWithFormat:@"TODAY %@",[dateFormat  stringFromDate:[NSDate date]]];
    selectedDateDisplay = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:[NSDate date]]];
    // Do any additional setup after loading the view from its nib.

    btnMap.layer.cornerRadius = 4.0;
    [btnMap setClipsToBounds:YES];
    
  
    [UITextField appearanceWhenContainedIn:[UISearchBar class], nil].leftView = nil;
 
    _searchBarText.searchTextPositionAdjustment = UIOffsetMake(10, 0);

//    searchIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"search_icon_new.png"]];
//    
//    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
//    {
//        if ([[UIScreen mainScreen] bounds].size.height <= 568.0f)
//        {
//            searchIcon.frame = CGRectMake(220, 12, 19, 21);
//        }
//        else
//        {
//            searchIcon.frame = CGRectMake(275, 12, 19, 21);
//        }
//    }
//    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
//    {
//        searchIcon.frame = CGRectMake(_searchBarText.frame.size.width-30, 12, 19, 21);
//    }
//    [_searchBarText addSubview:searchIcon];
    
    workOutListArray = [NSMutableArray array];
    dictRefineSearch = [NSDictionary dictionary];

    isLoadingFirstTime = YES;
    [self checkuserbuyfitpass];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkuserbuyfitpass) name:@"userboughtfitpass" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated
{
    scrolled = NO;
    
    [super viewDidAppear:animated];
    if (isLoadingFirstTime)
    {
        
        if  (isComeFromFavorite)
        {
            dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",pagenumber,@"page_number",@"yes",@"view_studio_workouts",nil];
        }
        else
        {
            dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",pagenumber,@"page_number",nil];
        }
        
        [self getWorkOutList :dictFinalParameter];
    }
    else  if ((selectedStudioId || selectedDate ) && clearData )
    {
        [self getWorkOutList:dictFinalParameter];
        clearData = NO;
    }
    else  if (clearData)
    {
        [self getWorkOutList:dictFinalParameter];
        clearData = NO;
    }
    
    isLoadingFirstTime = NO;
}

- (void) clearData
{
    clearData = YES;
    dictRefineSearch = nil;
    dictFinalParameter = nil;
    dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",pagenumber,@"page_number",nil];
    if (selectedStudioId) {
        [dictFinalParameter setValue:selectedStudioId forKey:@"studio_id"];
    }
    if (selectedDate) {
        [dictFinalParameter setValue:selectedDate forKey:@"date_of_workout"];
    }
}
#pragma mark --- Check User Buy fitpass ---

-(void)checkuserbuyfitpass
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&auth_key=%@",FITPASS_USER_ID,AUTHTENTICATION] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls checkuserbuyfitpass:submitData SuccessionBlock:^(id newResponse) {
        NSLog(@"Response %@",newResponse);
        Generalmodel *model = newResponse;
        
        /*
         buy_status=1, 			User has bought the fitpass. Cycle is running,
         buy_status=2,			User has bought the fitpass. But cycle had expired,
         buy_status=3,			User don't have buy fitpass
         in case 1 hide the get fit button
         in case 3 show the get fit button
         */
        
        NSLog(@"Status %@",model.status);
        if ([model.status isEqualToString:@"1"]) {
            btnMap.hidden = YES;
        }
        else if ([model.status isEqualToString:@"3"]) {
            btnMap.hidden = NO;
        }
        else // 1
        {
            btnMap.hidden = NO;
        }
    } andFailureBlock:^(NSError *error)
    {
        
    }];
}

-(NSString  *)currentDatecalc: (NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
//    selectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    
    return [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];;
}

-(NSString  *)selectedDate: (NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    selectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    
    return selectedDate;
}

-(NSString *)getDateFormat:(NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
 
    return [dateFormat stringFromDate:date];
}

-(void)checkNextDateinFindClass:(NSDate *)strDate
{
    NSDate *now = [NSDate date];
    NSDate *sixtyDaysAfter = [now dateByAddingTimeInterval:29*24*60*60];
    if([[self getDateFormat:sixtyDaysAfter] isEqualToString:[self getDateFormat:strDate]])
    {
        btnNextDate.userInteractionEnabled = NO;
        [imgNextArrow setHidden:YES];
    }
    else
    {
        btnNextDate.userInteractionEnabled = YES;
        [imgNextArrow setHidden:NO];
    }
}

- (IBAction)btnNextDateTapped:(id)sender
{
    NSDate *now = [NSDate date];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    arrSelectedDate =[[defaults objectForKey:@"selectdaterow"] componentsSeparatedByString:@"~~"];
    
    if ([[arrSelectedDate objectAtIndex:1] isEqualToString:@"1"])
    {
        nIncOrDecDate = 0;
        NSInteger selectedDatew     =   [[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        
        ++nIncOrDecDate;
        newDate1        = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text    =   [self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        
        scrolled = NO;

        [defaults setObject:[NSString stringWithFormat:@"%@~~0",[arrSelectedDate firstObject]] forKey:@"selectdaterow"];
        
    }
    else
    {
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        
        ++nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        scrolled = NO;

    }
    selectedDateDisplay = [self getDateFormat:newDate1];
    pickerDate = newDate1;
    [self checkNextDateinFindClass:newDate1];

    [dictFinalParameter setValue:@"1" forKey:@"page_number" ];
    [dictFinalParameter setValue:[self selectedDate:newDate1] forKey:@"date_of_workout" ];
    [self getWorkOutList:dictFinalParameter];

    [imgPrevious setHidden:NO];
    btnPreviousDate.userInteractionEnabled = YES;

    [defaults synchronize];
}


- (IBAction)btnPreviousDateTapped:(id)sender
{
    NSDate *now = [NSDate date];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    arrSelectedDate =[[defaults objectForKey:@"selectdaterow"] componentsSeparatedByString:@"~~"];
    NSString *strNowDate;
    if ([[arrSelectedDate objectAtIndex:1] isEqualToString:@"1"])
    {
        nIncOrDecDate = 0;
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        NSString *strSelectedDate = [self getDateFormat:selectDate];
        strNowDate = [self getDateFormat:now];
        if ([strSelectedDate isEqualToString:strNowDate])
        {
            btnPreviousDate.userInteractionEnabled = NO;
            [imgPrevious setHidden:YES];
            return;
        }
        --nIncOrDecDate;
       newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        scrolled = NO;

//        [self getWorkOutList :[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",
//                               [self selectedDate:newDate1],@"date_of_workout",pagenumber,@"page_number",nil]];
        
        [defaults setObject:[NSString stringWithFormat:@"%@~~0",[arrSelectedDate firstObject]] forKey:@"selectdaterow"];
    }
    else
    {
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        NSString *strSelectedDate =[defaults objectForKey:@"selectdate"];
        strNowDate = [self getDateFormat:now];
        if ([strSelectedDate isEqualToString:strNowDate])
        {
            btnPreviousDate.userInteractionEnabled = NO;
            [imgPrevious setHidden:YES];
            return;
        }
        
        --nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        scrolled = NO;

//        [self getWorkOutList :[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id", [self selectedDate:newDate1],@"date_of_workout",pagenumber,@"page_number",nil]];
    }
    
    [self checkNextDateinFindClass:newDate1];
    
    [dictFinalParameter setValue:@"1" forKey:@"page_number" ];
    [dictFinalParameter setValue:[self selectedDate:newDate1] forKey:@"date_of_workout" ];
  
    NSString *strSelectedDate =[defaults objectForKey:@"selectdate"];

//    if ([strSelectedDate isEqualToString:strNowDate])
//    {
//        if (selectedDate.length)
//        {
//            selectedDate = nil;
//            [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
//        }
//    }
    
    [self getWorkOutList:dictFinalParameter];
    pickerDate = newDate1;
    selectedDateDisplay = [self getDateFormat:newDate1];
    if ([strSelectedDate isEqualToString:strNowDate])
    {
        [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
        btnPreviousDate.userInteractionEnabled =NO;
        [imgPrevious setHidden:YES];
        return;
    }
    [defaults synchronize];
}

-(void)UpdateWithRefineData : (NSNotification*) notification
{
    scrolled = NO;

    NSDictionary *dict = [notification userInfo];
    dictRefineSearch = [notification userInfo];
    dictFinalParameter = nil;
    dictFinalParameter = [NSMutableDictionary dictionaryWithDictionary:dict];
    [dictFinalParameter setValue:@"1" forKey:@"page_number"];
    if ([selectedDate length]) {
        [dictFinalParameter setValue:selectedDate forKey:@"date_of_workout"];
    }
    if ([selectedStudioId length]) {
        [dictFinalParameter setValue:selectedStudioId forKey:@"studio_id"];
    }
    
    [self getWorkOutList:dictFinalParameter];
}

#pragma THIS CODE FOR FILETR VIEW

- (void) FilterListTableDelegateMethod: (FilterListTable *) sender selectedIndex :(NSString *) studioId andText:(NSString *)textData
{
    
    NSLog(@"selected date is %@",selectedDate);
    NSLog(@"studio id  is %@",studioId);

    scrolled = NO;
    if ([[dictRefineSearch allKeys] count]>1) {
        
        dictFinalParameter = nil;
        dictFinalParameter = [NSMutableDictionary dictionaryWithDictionary:dictRefineSearch];
        [dictFinalParameter setValue:@"1" forKey:@"page_number"];
        [dictFinalParameter setValue:studioId forKey:@"studio_id"];
        if ([selectedDate length])
            [dictFinalParameter setValue:selectedDate forKey:@"date_of_workout"];
        
//        NSDate *now = [NSDate date];
//        NSLog(@"%@ %@",[self selectedDate:now],[dictFinalParameter objectForKey:@"date_of_workout"]);
//        if ([[self selectedDate:now] isEqualToString:[dictFinalParameter objectForKey:@"date_of_workout"]])
//        {
//            selectedDate = nil;
//            [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
//        }
        
        [self getWorkOutList :dictFinalParameter];

    }
   else  if ([selectedDate length])
   {
       dictFinalParameter = nil;
       if  (isComeFromFavorite)
           dictFinalParameter=  [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studioId,@"studio_id",selectedDate,@"date_of_workout",@"1",@"page_number",@"yes",@"view_studio_workouts",nil];
       else
       {
           dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studioId,@"studio_id",selectedDate,@"date_of_workout",@"1",@"page_number",nil];
       }
//       NSDate *now = [NSDate date];
//       NSLog(@"%@ %@",[self selectedDate:now],[dictFinalParameter objectForKey:@"date_of_workout"]);
//       if ([[self selectedDate:now] isEqualToString:[dictFinalParameter objectForKey:@"date_of_workout"]])
//       {
//           selectedDate = nil;
//           [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
//       }
       
       [self getWorkOutList :dictFinalParameter];
   }
   else
   {
       dictFinalParameter = nil;
       if  (isComeFromFavorite)
           dictFinalParameter=  [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studioId,@"studio_id",@"1",@"page_number",@"yes",@"view_studio_workouts",nil];
       else
       {
           dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",studioId,@"studio_id",@"1",@"page_number",nil];
       }
       
//       NSDate *now = [NSDate date];
//       NSLog(@"%@ %@",[self selectedDate:now],[dictFinalParameter objectForKey:@"date_of_workout"]);
//       if ([[self selectedDate:now] isEqualToString:[dictFinalParameter objectForKey:@"date_of_workout"]])
//       {
//           selectedDate = nil;
//           [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
//       }
       
      [self getWorkOutList :dictFinalParameter];
   }
    selectedStudioId = studioId;
    _searchBarText.text = textData;
    
    UITextField *textField = [_searchBarText valueForKey:@"_searchField"];
    textField.clearButtonMode = UITextFieldViewModeNever;
    
    [filterTableView hideDropDown];
}

#pragma mark --- ScrollView end Scroll ---
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight          = scrollView.frame.size.height;
    float scrollContentSizeHeight   = scrollView.contentSize.height;
    float scrollOffset              = scrollView.contentOffset.y;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
        NSLog(@"**** Top ***");
    }
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        NSLog(@"**** End ***");
        if (!noDataFound) {
        pagenumberScroll = pagenumberScroll +1;
        scrolled = YES;
        NSLog(@"pagenumberScroll %d",pagenumberScroll);
        pagenumber = [NSString stringWithFormat:@"%d",pagenumberScroll];
        [dictFinalParameter setValue:pagenumber forKey:@"page_number"];
        //[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",                               [self selectedDate:newDate1],@"date_of_workout",pagenumber,@"page_number",nil]
        [self getWorkOutList :dictFinalParameter];
        }
    }
}

-(IBAction)showStudioFilter
{
    
    filterTableView = [[FilterListTable alloc]showFilterListWithFrame:[UIScreen mainScreen].bounds isAlreadySelected:(_searchBarText.text.length>0)?YES:NO];
    
    filterTableView.delegate = self;
   [filterTableView.searchBar setTintColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
}

- (void) FilterListTableDelegateMethodDone:(BOOL)isSelected
{
    _searchBarText.text = @"";
    
    if(isSelected)
    {
        selectedStudioId = nil;
        [dictFinalParameter removeObjectForKey:@"studio_id"];
        [self getWorkOutList:dictFinalParameter];
    }
    
    [filterTableView hideDropDown];
    [_searchBarText resignFirstResponder];
}

-(void)getWorkOutList: (NSDictionary *)paramDict
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    NSDate *now = [NSDate date];
    if([paramDict objectForKey:@"date_of_workout"] != nil)
    {
        if ([[self currentDatecalc:now] isEqualToString:[dictFinalParameter objectForKey:@"date_of_workout"]])
        {
            selectedDate = nil;
            if ([paramDict valueForKey:@"date_of_workout"])
                [dictFinalParameter removeObjectForKey:@"date_of_workout" ];
        }
    }
    paramDict = dictFinalParameter;
    [MBProgressHUD  hideHUDForView:self.view animated:YES];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [YSAPICalls getAllWorkOutListwithDateOfWork:paramDict SuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD  hideHUDForView:self.view animated:YES];

         Generalmodel *model = newResponse;
         [_searchBarText resignFirstResponder];
         
         if ([model.status_code isEqualToString:@"1"])
         {
             
             noDataFound = NO;
             
             if (model.tempArray.count)
             {
                 if (scrolled) {
                     [workOutListArray addObjectsFromArray:model.tempArray];
                 }
                 else
                 {
                     if ([workOutListArray count])
                     {
                         [workOutListArray removeAllObjects];
                     }
                     [workOutListArray addObjectsFromArray:model.tempArray];
                     
                 }
             }
         }
         else
         {
             noDataFound = YES;
         }
         
         [ tblData reloadData];
         
     } andFailureBlock:^(NSError *error) {
         [_searchBarText resignFirstResponder];
         
         [MBProgressHUD  hideHUDForView:self.view animated:YES];
     }];
}

#pragma mark --- SearchBar Delegate *** Start *** ----

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
//    UITextField *textField = [searchBar valueForKey:@"_searchField"];
//    textField.clearButtonMode = UITextFieldViewModeNever;
    
    //[_searchBarText setTintColor:[UIColor whiteColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor whiteColor]];
    
    [self showStudioFilter];
   //[searchIcon removeFromSuperview];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
   
    [_searchBarText addSubview:searchIcon];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Cancel");
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
   NSLog(@"Text Changing");
    
}

#pragma mark --- SearchBar Delegate *** End *** ----



#pragma mark --- tableView Delegate Start ---

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (scrolled) {
        return [workOutListArray count];
    }
    return (noDataFound)?1:[workOutListArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (!scrolled && noDataFound) {
        
        static NSString *simpleTableIdentifier = @"NoItems";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            
        }
        cell.textLabel.text = @"Sorry there aren't any more workouts open for reservation. Try another day?";
        cell.textLabel.font = [UIFont fontWithName:@"OPENSANS" size:14];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.textLabel.numberOfLines = 2;
        return cell;
    }
    else
    {
        static NSString *CellIdentifier = @"CellFindClass";
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            CellIdentifier = @"CellFindClass";
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            CellIdentifier = @"CellFindClass_ipad";
        }
    
    CellFindClass *cell = (CellFindClass *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects ;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CellFindClass" owner:self options:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CellFindClass_ipad" owner:self options:nil];
        }
        cell = [topLevelObjects objectAtIndex:0];
        
    }
   
    [cell setCellData:[workOutListArray objectAtIndex:indexPath.row]];
    cell.btnSelect.tag = indexPath.row;
    [cell.btnSelect addTarget:self action:@selector(navigate:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    }
}

// Variable height support

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return (![workOutListArray count])? 140:100.0f;
    }
    else
    {
        return (![workOutListArray count])? 120:80.0f;
    }
}

#pragma mark --- tableView Delegate Start ---


- (void)navigate : (UIButton *)sender
{
    // pothi
    WorkOutDetailsVC *workoutObj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC_New" bundle:nil];
    }
    workoutObj.getFitStatus = (btnMap.hidden)?@"1":@"2";

    workoutObj.workOutData = [workOutListArray objectAtIndex:sender.tag];
    workoutObj.selectedDate = [self selectedDate:pickerDate];
    workoutObj.selectedDateDisplay = selectedDateDisplay;

    NSLog(@"%@",workoutObj.workOutData);
    NSLog(@"%@",workoutObj.selectedDate);
    NSLog(@"%@",workoutObj.selectedDateDisplay);
    
    [self.navigationController pushViewController:workoutObj animated:YES];
}

- (IBAction)mapTapped:(UIButton *)sender {

    FPGetFitVC *mapVc;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        mapVc = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        mapVc = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC_ipad" bundle:nil];
    }
    [UIView beginAnimations:@"View Flip" context:nil];
    [UIView setAnimationDuration:0.80];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    
    [UIView setAnimationTransition:
     UIViewAnimationTransitionFlipFromRight
                           forView:self.navigationController.view cache:NO];
    
    [self.navigationController pushViewController:mapVc animated:YES];
    [UIView commitAnimations];
}

-(void)dateChanged:(AADatePicker *)sender isCurrentDate : (BOOL)isCurrentDate
{
    NSDate *now = [NSDate date];
    NSLog(@"%@",[self getDateFormat:sender.date]);
    if ([[self getDateFormat:now] isEqualToString:[self getDateFormat:sender.date]] || isCurrentDate)
    {
        btnPreviousDate.userInteractionEnabled = NO;
        [imgPrevious setHidden:YES];
    }
    else
    {
        btnPreviousDate.userInteractionEnabled = YES;
        [imgPrevious setHidden:NO];
    }
    
    [self checkNextDateinFindClass:sender.date];
    
    pickerDate = sender.date;
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
    str=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:(isCurrentDate)?[NSDate date]:sender.date]];
    
    NSInteger currentDate = [[[[self getDateFormat:pickerDate] componentsSeparatedByString:@" "]lastObject] integerValue];
    nIncOrDecDate = currentDate;
    
    selectedDateDisplay = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:(isCurrentDate)?[NSDate date]:sender.date]];

    lblDate.text=str;
    [UIView animateWithDuration:0.5 animations:^{
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            tblData.frame =  CGRectMake(tblData.frame.origin.x, 78, tblData.frame.size.width, tblData.frame.size.height);
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            tblData.frame =  CGRectMake(tblData.frame.origin.x, 100, tblData.frame.size.width, tblData.frame.size.height);
        }
    }completion:^(BOOL finished) {
        
        scrolled = NO;
        pagenumber = @"1";
        pagenumberScroll = 1;
        if ([[dictRefineSearch allKeys] count]>1)
        {
            dictFinalParameter = nil;
            dictFinalParameter = [NSMutableDictionary dictionaryWithDictionary:dictRefineSearch];
            [dictFinalParameter setValue:@"1" forKey:@"page_number"];
                [dictFinalParameter setValue:[self selectedDate:sender.date] forKey:@"date_of_workout"];
            if ([selectedStudioId length]) {
                [dictFinalParameter setValue:selectedStudioId forKey:@"studio_id"];
            }
            
            [self getWorkOutList :dictFinalParameter];
        }
        else   if ([selectedStudioId length])
        {
            dictFinalParameter = nil;
            if  (isComeFromFavorite)
                dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",selectedStudioId,@"studio_id",[self selectedDate:sender.date],@"date_of_workout",@"1",@"page_number",@"yes",@"view_studio_workouts",nil];
            else
            {
                dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",selectedStudioId,@"studio_id",[self selectedDate:sender.date],@"date_of_workout",@"1",@"page_number",nil];
            }
            
            [self getWorkOutList :dictFinalParameter];

        }
        else
        {
            dictFinalParameter = nil;
            
            if  (isComeFromFavorite)
                dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",[self selectedDate:sender.date],@"date_of_workout",@"1",@"page_number",@"yes",@"view_studio_workouts",nil];
            else
            {
                
                dictFinalParameter = [NSMutableDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",[self selectedDate:sender.date],@"date_of_workout",@"1",@"page_number",nil];
            }
            
            [self getWorkOutList :dictFinalParameter];
        }
    }];
    
    [datePicker removeFromSuperview];
    
}

- (IBAction)dateButtonTapped:(UIButton *)sender {
    NSDate *now = [NSDate date];
    NSDate *sixtyDaysAgo = [now dateByAddingTimeInterval:-10*24*60*60];
    NSLog(@"7 days ago: %@", sixtyDaysAgo);
    
    NSDate *sixtyDaysAfter = [now dateByAddingTimeInterval:30*24*60*60];
    NSLog(@"7 days ago: %@", sixtyDaysAfter);
    
    if ([datePicker superview] == nil) {
        
        datePicker = [[AADatePicker alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200) maxDate:sixtyDaysAfter minDate:[NSDate date] showValidDatesOnly:YES];
        
        datePicker.delegate = self;
        
        [UIView animateWithDuration:0.5 animations:^{
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
               [datePicker setFrame:CGRectMake(0, 79,datePicker.frame.size.width, datePicker.frame.size.height)];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                [datePicker setFrame:CGRectMake(0, 100,datePicker.frame.size.width, datePicker.frame.size.height)];
            }
            
            [self.view addSubview:datePicker];
                        
        }completion:^(BOOL finished)
        {
            
        }];
        
        [UIView animateWithDuration:0.6 animations:^{
            
            tblData.frame =  CGRectMake(tblData.frame.origin.x, tblData.frame.origin.y + 213, tblData.frame.size.width, tblData.frame.size.height);
            
        }completion:^(BOOL finished) {
            
        }];
    }
}

- (IBAction)refineSearchTapped:(UIButton *)sender
{
    FPRefineSearch *obj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        obj = [[FPRefineSearch alloc]initWithNibName:@"FPRefineSearch" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        obj = [[FPRefineSearch alloc]initWithNibName:@"FPRefineSearch_ipad" bundle:nil];
    }
    obj.delegate = self;
    [self.navigationController pushViewController:obj animated:YES];
}



- (IBAction)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}






- (IBAction)getfittapped:(id)sender
{
    FPGetFitVC *getfit;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        getfit = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        getfit = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:getfit animated:YES];
}

- (IBAction)searchTapped:(UIButton *)sender
{
    [_searchBarText resignFirstResponder];
    [self showStudioFilter];
}

@end
