//
//  FPTermsforUsers.m
//  FitPass
//
//  Created by bsetec on 7/28/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPTermsforUsers.h"

@interface FPTermsforUsers ()
@property (strong, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation FPTermsforUsers

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    NSURL *websiteUrl = [NSURL URLWithString:@"http://fitpass.co.in/terms-and-conditions/"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webView loadRequest:urlRequest];
    
    self.webView.delegate =self;
}
- (IBAction)backaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
