//
//  HomeVC.m
//  FitPass
//
//  Created by bsetec on 7/4/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "HomeVC.h"
#import "SignUpMain.h"
#import "SignInVc.h"
#import "SignInViewController.h"
@interface HomeVC ()


@end

@implementation HomeVC

@synthesize scrollviewobj,pagecontrolobj,arrimages,lbl1obj,lbl2obj,lbl3obj,currentPage,arrlbl1;
@synthesize btnSignin,btnSignup;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    btnSignin.layer.cornerRadius = 4.0f;
    btnSignup.layer.cornerRadius = 4.0f;
    btnSignin.clipsToBounds = YES;
    btnSignup.clipsToBounds = YES;
    CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
   
    UIImageView *imageView1,*imageView2,*imageView3,*imageView4;
    JScrollView_PageControl_AutoScroll *view=[[JScrollView_PageControl_AutoScroll alloc]initWithFrame: (iOSDeviceScreenSize.height == 736)?[[UIScreen mainScreen] bounds]:self.view.bounds];
  
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        imageView1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"15.jpg"]];
        imageView2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"4.jpg"]];
        imageView3=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"1.jpg"]];
        imageView4=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"8.jpg"]];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageView1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipadbg(8).jpg"]];
        imageView2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipadbg(13).jpg"]];
        imageView3=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipadbg(1).jpg"]];
        imageView4=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"ipadbg(10).jpg"]];
    }
//Get. Set. Sweat. Make variety your routine with just a tap.
    arrlbl1 = [NSMutableArray arrayWithObjects:@"FITPASS IS YOUR PASS\nTO UNLIMITED FITNESS\nOPTIONS ANYWHERE, ANYTIME",@"FREELY ACCESS\nTOP GYMS AND\nFITNESS STUDIOS NEAR YOU",@"CHOOSE FROM THOUSANDS\nOF WORKOUT OPTIONS\nAT YOUR CONVENIENCE",@"GET. SET. SWEAT.\nMAKE VARIETY YOUR ROUTINE\nWITH JUST A TAP", nil];
    
    [view setLblArray:arrlbl1];
    
    view.autoScrollDelayTime = 5.0;
    view.delegate=self;
    NSMutableArray *viewsArray=[[NSMutableArray alloc]initWithObjects:imageView1,imageView2,imageView3,imageView4,nil];
    [view setViewsArray:viewsArray];
    
    [self.view addSubview:view];
    [view shouldAutoShow:YES];
    [self.view sendSubviewToBack:view];
}

- (void)didClickPage:(JScrollView_PageControl_AutoScroll *)view atIndex:(NSInteger)index
{
    NSLog(@"click at %ld",(long)index  );
}
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
      CGFloat pageWidth = scrollviewobj.frame.size.width;
  //  int page = floor((scrollviewobj.contentOffset.x - pageWidth) / pageWidth) + 1;
      int page = scrollviewobj.contentOffset.x  / pageWidth;
      pagecontrolobj.currentPage = page;
      lbl1obj.text = [arrlbl1 objectAtIndex:page];
}

- (IBAction)btnSignupaction:(id)sender
{
    UIViewController *rootController;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        rootController = [[SignUpMain alloc] initWithNibName:@"SignUpMain" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rootController = [[SignUpMain alloc] initWithNibName:@"SignUpMain_ipad" bundle:nil];
    }
    
    [self.navigationController pushViewController:rootController animated:YES];
}

- (IBAction)signInTapped:(UIButton *)sender
{
    SignInViewController *sign;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        sign = [[SignInViewController alloc]initWithNibName:@"SignInViewController" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        sign = [[SignInViewController alloc]initWithNibName:@"SignInViewController_ipad" bundle:nil];
    }
    
    [self.navigationController pushViewController:sign animated:YES];
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
   // self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
       // self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)removeAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    // self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
       // self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        if (finished) {
           // [self.view removeFromSuperview];
        }
    }];
}



-(void)removeview
{
     [self removeAnimate];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
