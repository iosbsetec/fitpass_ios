//
//  FPPrivatePolicyVC.h
//  FitPass
//
//  Created by bsetec on 7/29/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FPPrivatePolicyVC : UIViewController<UIWebViewDelegate>

@property(strong,nonatomic)NSString *urlStr;
@property(strong,nonatomic)IBOutlet UIWebView *webviewprivatepolicy;
@end
