//
//  FPStudioDetailsScheduleVC.m
//  FitPass
//
//  Created by bsetec on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPStudioDetailsScheduleVC.h"
#import "CellFindClass.h"
#import "AADatePicker.h"
#import "YSAPICalls.h"
#import "Generalmodel.h"
#import "WorkOutDetailsVC.h"
#import "MBProgressHUD.h"

static NSString *kDateCellID = @"dateCell";     // the cells with the start or end date
static NSString *kDatePickerID = @"datePicker"; // the cell containing the date picker
static NSString *kOtherCell = @"otherCell";
#define kPickerAnimationDuration    0.40   // duration for the animation to slide the date picker into view


@interface FPStudioDetailsScheduleVC ()<AADatePickerDelegate>
{
    IBOutlet UIView *viewDatePicker;
    NSString *selectedDateDisplay;
    AADatePicker *datePicker;
    IBOutlet UILabel *lblDate;
    NSMutableArray *arrayScheduleDatails;
    IBOutlet UIView *viewTBLHeader;
    
    NSDate *pickerDate;
}


@property (nonatomic, strong) NSIndexPath *datePickerIndexPath;
@property (strong, nonatomic) IBOutlet UIButton *btnDate;

@property (assign) NSInteger pickerCellRowHeight;

@property (nonatomic, strong) IBOutlet UIDatePicker *pickerView;
// this button appears only when the date picker is shown (iOS 6.1.x or earlier)
@property (nonatomic, strong) IBOutlet UIBarButtonItem *doneButton;
- (IBAction)dateButtonTapped:(UIButton *)sender;

@end
@implementation FPStudioDetailsScheduleVC
@synthesize scheduletableview;

- (void)viewDidLoad
{
    [super viewDidLoad];
     _pickerView.maximumDate = [NSDate date];
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"0~~0" forKey:@"selectdaterow"];
    [defaults synchronize];
    
    btnPreviousDate.userInteractionEnabled = NO;
    [imgPrevious setHidden:YES];

//    NSLog(@"Studo id =%@",_studio_id);
    
    arrayScheduleDatails = [[NSMutableArray alloc] init];
    
    pickerDate = [NSDate date];
    lblDate.text=[NSString stringWithFormat:@"TODAY %@",[dateFormat  stringFromDate:[NSDate date]]];
    selectedDateDisplay = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:[NSDate date]]];
    
    [self getstudieoSchedule:[self selectedDateForApi:[NSDate date]]];
}

//viewTBLHeader

-(void)getstudieoSchedule:(NSString *)strScheduleDate;
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSDictionary *paramDict=[NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",_studio_id,@"studio_id",strScheduleDate,@"date_of_workout",nil];
    [YSAPICalls getStudioscheduleDetails:paramDict SuccessionBlock:^(id newresponse)
    {
        [MBProgressHUD  hideHUDForView:self.view animated:YES];
        Generalmodel *gmodel = newresponse;
        
        if (gmodel.tempArray.count > 0)
        {
            scheduletableview.tableHeaderView = nil;

            if ([arrayScheduleDatails count])
            {
                [arrayScheduleDatails removeAllObjects];
            }
            arrayScheduleDatails = gmodel.tempArray;
            scheduletableview.scrollEnabled = YES;
        }
        else
        {
            scheduletableview.tableHeaderView = viewTBLHeader;
            scheduletableview.scrollEnabled = NO;
        }
         [scheduletableview reloadData];
    }
      andFailureBlock:^(NSError *error)
    {
        [MBProgressHUD  hideHUDForView:self.view animated:YES];
    }];
}

-(NSString  *)selectedDateForApi: (NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"dd-MM-yyyy"];
    selectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    
    return selectedDate;
}

-(NSString  *)selectedDate: (NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    selectedDate = [NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:date]];
    
    return selectedDate;
}

-(NSString *)getDateFormat:(NSDate *)date
{
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
    
    return [dateFormat stringFromDate:date];
}

-(void)checkNextDate:(NSDate *)strDate
{
    NSDate *now = [NSDate date];
    NSDate *sixtyDaysAfter = [now dateByAddingTimeInterval:29*24*60*60];
    if([[self getDateFormat:sixtyDaysAfter] isEqualToString:[self getDateFormat:strDate]])
    {
        btnNextDate.userInteractionEnabled = NO;
        [imgNextArrow setHidden:YES];
    }
    else
    {
        btnNextDate.userInteractionEnabled = YES;
        [imgNextArrow setHidden:NO];
    }
}

- (IBAction)btnNextDateTapped:(id)sender
{
    NSDate *now = [NSDate date];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrSelectedDate =[[defaults objectForKey:@"selectdaterow"] componentsSeparatedByString:@"~~"];
    NSDate *newDate1;
    if ([[arrSelectedDate objectAtIndex:1] isEqualToString:@"1"])
    {
        nIncOrDecDate = 0;
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        
        ++nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        
        /***
         Need to call API
        ***/
        
        [defaults setObject:[NSString stringWithFormat:@"%@~~0",[arrSelectedDate firstObject]] forKey:@"selectdaterow"];
    }
    else
    {
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        
        ++nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        
        /***
         Need to call API
         ***/
    }
    
    [self checkNextDate:newDate1];
    
    selectedDateDisplay = [self getDateFormat:newDate1];
    pickerDate = newDate1;
    
    [self getstudieoSchedule:[self selectedDateForApi:newDate1]];
    [imgPrevious setHidden:NO];
    btnPreviousDate.userInteractionEnabled = YES;
    
    [defaults synchronize];
}

- (IBAction)btnPreviousDateTapped:(id)sender
{
    NSDate *now = [NSDate date];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrSelectedDate =[[defaults objectForKey:@"selectdaterow"] componentsSeparatedByString:@"~~"];
    NSString *strNowDate;
    NSDate *newDate1;
    if ([[arrSelectedDate objectAtIndex:1] isEqualToString:@"1"])
    {
        nIncOrDecDate = 0;
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        NSString *strSelectedDate = [self getDateFormat:selectDate];
        strNowDate = [self getDateFormat:now];
        if ([strSelectedDate isEqualToString:strNowDate])
        {
            [self getstudieoSchedule:[self selectedDateForApi:newDate1]];
            btnPreviousDate.userInteractionEnabled = NO;
            [imgPrevious setHidden:YES];
            return;
        }
        --nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        
        /***
         Need to call API
         ***/
        
        [defaults setObject:[NSString stringWithFormat:@"%@~~0",[arrSelectedDate firstObject]] forKey:@"selectdaterow"];
    }
    else
    {
        NSInteger selectedDatew =[[arrSelectedDate objectAtIndex:0] integerValue];
        
        NSDate *selectDate =  [now dateByAddingTimeInterval:selectedDatew*24*60*60];
        NSString *strSelectedDate =[defaults objectForKey:@"selectdate"];
        strNowDate = [self getDateFormat:now];
        if ([strSelectedDate isEqualToString:strNowDate])
        {
            [self getstudieoSchedule:[self selectedDateForApi:newDate1]];
            
            btnPreviousDate.userInteractionEnabled = NO;
            [imgPrevious setHidden:YES];
            return;
        }
        
        --nIncOrDecDate;
        newDate1 = [selectDate dateByAddingTimeInterval:nIncOrDecDate*24*60*60];
        lblDate.text=[self getDateFormat:newDate1];
        
        [defaults setObject:[self getDateFormat:newDate1] forKey:@"selectdate"];
        
        /***
         Need to call API
         ***/
    }
//    NSLog(@"%@",newDate1);
    
    [self checkNextDate:newDate1];
    
    selectedDateDisplay = [self getDateFormat:newDate1];
    pickerDate = newDate1;
    
    [self getstudieoSchedule:[self selectedDateForApi:newDate1]];
    NSString *strSelectedDate =[defaults objectForKey:@"selectdate"];
    if ([strSelectedDate isEqualToString:strNowDate])
    {
        btnPreviousDate.userInteractionEnabled = NO;
        [imgPrevious setHidden:YES];
        return;
    }
    [defaults synchronize];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrayScheduleDatails.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CellIdentifier = @"CellFindClass";
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CellIdentifier = @"CellFindClass_ipad";
    }
    
    CellFindClass *cell = (CellFindClass *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CellFindClass" owner:self options:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"CellFindClass_ipad" owner:self options:nil];
        }
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    [cell setCellDataStudioSchedule:[arrayScheduleDatails objectAtIndex:indexPath.row]];
    cell.lblClassName.text = _studio_name;
    cell.btnSelect.tag = indexPath.row;
    [cell.btnSelect addTarget:self action:@selector(navigateDetailPage:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (void)navigateDetailPage:(UIButton *)sender
{
    // pothi
    WorkOutDetailsVC *workoutObj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC_New" bundle:nil];
    }
    workoutObj.workOutData = [arrayScheduleDatails objectAtIndex:sender.tag];
    workoutObj.selectedDate = [self selectedDate:pickerDate];
    workoutObj.selectedDateDisplay = selectedDateDisplay;
    
    [self.navigationController pushViewController:workoutObj animated:YES];
}

//"batch_id" = 424;
//"date_of_workout" = "2015-09-09";
//"user_id" = 4902;
//"work_out_id" = 133;
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        return 100.0f;
    }else{
        return 80.0f;
    }
}

-(void)dateChanged:(AADatePicker *)sender isCurrentDate : (BOOL)isCurrentDate
{
    NSDate *now = [NSDate date];
    
    if ([[self getDateFormat:now] isEqualToString:[self getDateFormat:sender.date]] || isCurrentDate)
    {
        btnPreviousDate.userInteractionEnabled = NO;
        [imgPrevious setHidden:YES];
    }
    else
    {
        btnPreviousDate.userInteractionEnabled = YES;
        [imgPrevious setHidden:NO];
    }
    
    [self checkNextDate:sender.date];
    
    pickerDate = sender.date;
    
    NSDateFormatter *dateFormat=[[NSDateFormatter alloc]init];
    dateFormat.dateStyle=NSDateFormatterMediumStyle;
    [dateFormat setDateFormat:@"EEEE, MMMM dd"];
    selectedDateDisplay=[NSString stringWithFormat:@"%@",[dateFormat  stringFromDate:(isCurrentDate)?[NSDate date]:sender.date]];
    //    NSString *dateString = [NSDateFormatter localizedStringFromDate:sender.date
    //                                                          dateStyle:NSDateFormatterShortStyle
    //                                                          timeStyle:NSDateFormatterMediumStyle];
    lblDate.text=selectedDateDisplay;;
    [UIView animateWithDuration:0.5 animations:^{
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            scheduletableview.frame =  CGRectMake(scheduletableview.frame.origin.x, 124, scheduletableview.frame.size.width, scheduletableview.frame.size.height);
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            scheduletableview.frame =  CGRectMake(scheduletableview.frame.origin.x, 150, scheduletableview.frame.size.width, scheduletableview.frame.size.height);
        }
    }completion:^(BOOL finished) {
        [self getstudieoSchedule:[self selectedDateForApi:sender.date]];

    }];
    [datePicker removeFromSuperview];
}

- (IBAction)dateButtonTapped:(UIButton *)sender
{
    NSDate *now = [NSDate date];
//    NSDate *sixtyDaysAgo = [now dateByAddingTimeInterval:-10*24*60*60];
//    NSLog(@"7 days ago: %@", sixtyDaysAgo);
    
    NSDate *sixtyDaysAfter = [now dateByAddingTimeInterval:30*24*60*60];
    NSLog(@"7 days ago: %@", [self getDateFormat:sixtyDaysAfter]);
    if ([datePicker superview] == nil)
    {
        datePicker = [[AADatePicker alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 200) maxDate:sixtyDaysAfter minDate:[NSDate date] showValidDatesOnly:YES];
        
        datePicker.delegate = self;
        [UIView animateWithDuration:0.5 animations:^{
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                [datePicker setFrame:CGRectMake(0, 125,datePicker.frame.size.width, datePicker.frame.size.height)];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                [datePicker setFrame:CGRectMake(0, 150,datePicker.frame.size.width, datePicker.frame.size.height)];
            }
            [self.view addSubview:datePicker];
        }completion:^(BOOL finished) {
        }];
        [UIView animateWithDuration:0.6 animations:^{
            scheduletableview.frame =  CGRectMake(scheduletableview.frame.origin.x, scheduletableview.frame.origin.y + 213, scheduletableview.frame.size.width, scheduletableview.frame.size.height);
        }completion:^(BOOL finished)
        {
            
        }];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnbackaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
