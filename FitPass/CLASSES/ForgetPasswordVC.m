//
//  SignInVc.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import "FPMenuVC.h"
#import "YSAPICalls.h"
#import "FPAppDelegate.h"
#import "Generalmodel.h"

@interface ForgetPasswordVC ()
{
    IBOutlet UITextField *FEmail;
    IBOutlet UIButton *btnOK;
    FPAppDelegate *appDelegate;
}

@end

@implementation ForgetPasswordVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    FEmail.layer.cornerRadius = 5;
    btnOK.layer.cornerRadius = 5;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ******* Validate Email ID *******
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)checkUserGivenDetails
{
    appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (FEmail.text.length==0)
    {
        [appDelegate onCreateToastMsgInWindow:@"Please enter your Email id"];
        return NO;
    }
    
    BOOL isEmailOk = [self NSStringIsValidEmail:FEmail.text];
    
    if (!isEmailOk)
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDEMAILID];
        return NO;
    }
    
    return YES;
}

- (IBAction)signInTapped:(UIButton *)sender
{
    BOOL isOK = [self checkUserGivenDetails];
    
    if (isOK)
    {
        if (![YSAPICalls isNetworkRechable])
        {
            [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
            return;
        }
        
        NSData *submitData = [[NSString stringWithFormat:@"email_address=%@",FEmail.text] dataUsingEncoding:NSUTF8StringEncoding];
        
        [YSAPICalls getForgetPasswordDetailsParamData:submitData SuccessionBlock:^(id newResponse)
         {
             //             isReserveDetailsRetrive = YES;
             
             
             Generalmodel *model = newResponse;
             if ([model.status_code integerValue]) {
                 [self backTapped:nil];

             }
           
             appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
             [appDelegate onCreateToastMsgInWindow:model.status_Msg];
         
         
         } andFailureBlock:^(NSError *error) {
             
         }];
    }
}
//1280.85+220.18+130.43+6290.83+202.57
- (IBAction)backTapped:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:^(void)
    {
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
