//
//  CustomFontLabel.m
//  FitPass
//
//  Created by bsetec on 7/23/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "CustomFontLabel.h"

@implementation CustomFontLabel

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setFont:[UIFont fontWithName:@"REFSAN-1" size:20.0]];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        //[self setFont:[UIFont fontWithName:@"REFSAN-1" size:40.0]];
    self.font=[UIFont fontWithName:@"MS Reference Sans Serif" size:40];
    }
    return self;
}

-(void)setFontSize:(int)size {
    [self setFont:[UIFont fontWithName:@"REFSAN-1" size:20]];
}


@end
