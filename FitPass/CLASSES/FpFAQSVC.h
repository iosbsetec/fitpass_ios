//
//  FpFAQSVC.h
//  FitPass
//
//  Created by bsetec on 7/29/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FpFAQSVC : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webviewfaqs;
@end
