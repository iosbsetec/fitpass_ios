//
//  FavouriteStudioVC.h
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FavouriteStudioVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UIView *viewHeaderDetail;
}
@property (strong, nonatomic) IBOutlet UITableView *favouritestudio_tableview;
@property (strong, nonatomic) IBOutlet UIButton *btnviewclasses;
- (IBAction)btnviewclassesaction:(id)sender;
- (IBAction)btnbackaction:(id)sender;

@end
