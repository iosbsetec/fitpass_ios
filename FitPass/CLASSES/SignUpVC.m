//
//  SignUpVC.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "SignUpVC.h"
#import "FPMenuVC.h"
#import "FPAppDelegate.h"
#import "SPHKeyChain.h"
#import "MBProgressHUD.h"

@interface SignUpVC ()
{
    IBOutlet UIScrollView *scrollViewRegister;
    IBOutlet UITextField *Fname;
    IBOutlet UITextField *phoneNumber;
    IBOutlet UITextField *Email;
    IBOutlet UITextField *Password;
    IBOutlet UITextField *pinCode;
    FPAppDelegate *appDelegate;
    
    __weak IBOutlet UIButton *btnCheck;
    __weak IBOutlet UIImageView *imgVCheckMark;
    IBOutlet UIButton *CreateAccount;

    IBOutlet UIView *viewSegmentController;
    NSString *gender,*accepted;
    
    BOOL isFilled;
}

- (IBAction)accountTapped:(UIButton *)sender;
- (IBAction)checkTapped:(UIButton *)sender;

@end

@implementation SignUpVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Email.layer.cornerRadius = 5;
    Password.layer.cornerRadius = 5;
    Fname.layer.cornerRadius = 5;
    phoneNumber.layer.cornerRadius = 5;
    CreateAccount.layer.cornerRadius = 5;
    viewSegmentController.layer.cornerRadius = 5;
    [viewSegmentController setClipsToBounds:YES];
    [self devicesize];
    [btnCheck setSelected:YES];
    gender =@"3";
    accepted = @"1";
    
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    //Code to handle the gesture
}
-(void)devicesize
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
      
        if ([[UIScreen mainScreen] bounds].size.height < 568.0f)
        {
            [scrollViewRegister setContentSize:CGSizeMake(320, 500)];
            
            
        }
//        else
//        {
//            [scrollViewRegister setContentSize:CGSizeMake(320,865)];
//        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSString*)getDeviceID
{
    id getvalue=[SPHKeyChain load:@"SavedKeyChainData"];
    if ([getvalue respondsToSelector:@selector(objectForKey:)])
    {
        return ([[getvalue valueForKey:@"ConstantUDID"] length]<1)?[self generateUUID]:[getvalue valueForKey:@"ConstantUDID"];
    }
    else{
        return [self generateUUID];
    }
}


-(NSString*)generateUUID
{
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    NSString *UniqueID = [oNSUUID UUIDString];
    NSMutableDictionary *firstNameDict=[NSMutableDictionary new];
    [firstNameDict setObject:UniqueID forKey:@"ConstantUDID"];
    [SPHKeyChain save:@"SavedKeyChainData" data:firstNameDict];
    return UniqueID;
}


- (IBAction)backTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)accountTapped:(UIButton *)sender
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController.view setUserInteractionEnabled:NO];
    
    BOOL isOK = [self checkUserGivenDetails];
    
    if (isOK)
    {
        if (![YSAPICalls isNetworkRechable])
        {
            [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
            [CreateAccount setEnabled:YES];
            return;
        }
        
        [CreateAccount setEnabled:NO];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSData *submitData = [[NSString stringWithFormat:@"first_name=%@&mobile_no=%@&email_address=%@&password=%@&gender=%@&pin_code=%@&device_id=%@&device_type=iOS&device_token=%@",Fname.text,phoneNumber.text,Email.text,Password.text,gender,pinCode.text,[(FPAppDelegate *)[UIApplication sharedApplication].delegate getDeviceID],GETVALUE(DEVICE_TOKEN)] dataUsingEncoding:NSUTF8StringEncoding];
        
//        NSData *submitData = [[NSString stringWithFormat:@"first_name=%@&mobile_no=%@&email_address=%@&password=%@&gender=%@&pin_code=%@&device_id=%@&device_type=iOS&device_token=%@",Fname.text,phoneNumber.text,Email.text,Password.text,gender,pinCode.text,@"DABA0A50-7F67-4E7A-BA51-998592FEF1D5",@"e717e3e62a72e62b62fbce1d20597b7350e2467db5302f12b8ddd7b4a68763fe"] dataUsingEncoding:NSUTF8StringEncoding];
        
        [YSAPICalls getSignUpDetailsParamData:submitData SuccessionBlock:^(id newResponse)
         {
             //  isReserveDetailsRetrive = YES;
             [MBProgressHUD hideHUDForView:self.view animated:YES];

             Generalmodel *model = newResponse;
             [CreateAccount setEnabled:YES];

             if ([model.status_code isEqualToString:@"0"])
             {
                 appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
                 [appDelegate onCreateToastMsgInWindow:[model.status_Msg stringByAppendingString:@"Please login"]];
             }
             else
             {
                 if ([model.status_code isEqualToString:@"0"])
                 {
                     [appDelegate onCreateToastMsgInWindow:model.status_Msg];
                 }
                 else
                 {
                     SETVALUE(model.user_id, @"Userid");
                     SETVALUE(model.user_email, kUSEREMAIL);
                     SETVALUE(model.authtentication, @"Authtentication");
                     SETVALUE(phoneNumber.text, USERMOBILE);
                     SETVALUE(Fname.text, USERNAME);
                     SYNCHRONISE;
                     
                     UIViewController *rootController;
                     if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                     {
                         rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
                     }
                     else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                     {
                         rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
                     }
                     
                     [self.navigationController pushViewController:rootController animated:YES];
                     UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                     [window.rootViewController.view setUserInteractionEnabled:YES];
                 }
             }
             
         }
            andFailureBlock:^(NSError *error) {
             [CreateAccount setEnabled:YES];
             UIWindow *window = [[UIApplication sharedApplication] keyWindow];
             [window.rootViewController.view setUserInteractionEnabled:YES];
         }];
    }
}

- (IBAction)checkTapped:(UIButton *)sender
{
    if (![sender isSelected])
    {
        accepted =@"1";
        [sender setSelected:YES];
    }
    else
    {
        accepted =@"0";
        [sender setSelected:NO];

    }
}



- (IBAction)btnaccept:(id)sender
{
    FPTermsforUsers *TermsforUsers;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        TermsforUsers = [[FPTermsforUsers alloc]initWithNibName:@"FPTermsforUsers" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        TermsforUsers = [[FPTermsforUsers alloc]initWithNibName:@"FPTermsforUsers_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:TermsforUsers animated:YES];
    
}
- (IBAction)segmentTapped:(UIButton *)sender
{
    NSLog(@"Sender +%ld",(long)[sender tag]);
    
    gender = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    
    [self performSelector:@selector(deselectAll)];
    [((UIButton *) sender) setSelected:YES];
    
    [((UIButton *) sender) setBackgroundColor:[UIColor colorWithRed:20.0/255 green:135.0/255  blue:115.0/255  alpha:1.0]
];
    [((UIButton *) sender) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

}

- (void) deselectAll
{
    NSArray *views = [viewSegmentController subviews];
    for (UIView *v in views)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            [((UIButton *)v) setSelected:NO];
            
            [((UIButton *) v) setBackgroundColor:TILLDCOLOR];
            [((UIButton *) v) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
}



#pragma mark - ******* Validate Email ID *******
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

#pragma mark - ******* Validate Mobile Number *******
-(BOOL)validateMobileNumber:(NSString *)strNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    return [mobileNumberPred evaluateWithObject:strNumber];
}

-(BOOL)checkUserGivenDetails
{
    appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (Fname.text.length==0 && phoneNumber.text.length==0 && Email.text.length==0 && Password.text.length==0 && pinCode.text.length==0)
    {
        [appDelegate onCreateToastMsgInWindow:@"All fields are mandatory"];
        return NO;
    }
    
    if ([Fname.text length] < 4 || [Fname.text length] >=75)
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDUSERNAME];
        return NO;
    }
    
    if (![YSSupport validateEmail:Email.text])
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDEMAILID];
        return NO;
    }
    
    if ([Password.text length] < 6 || [Password.text length] >=16)
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDPASSWORDLENGTH];
        return NO;
    }
    
//    BOOL isMobileNumberOk = [self validateMobileNumber:phoneNumber.text];
//    BOOL isPincodeOk = [self isValidPinCode:pinCode.text];
    
    if (![self validateMobileNumber:phoneNumber.text])
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDMOBILENUMBER];
        return NO;
    }
    
    if (![YSSupport isValidPinCode:pinCode.text])
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDPINCODE];
        return NO;
    }
    
    if ([gender length]== 0) {
        [appDelegate onCreateToastMsgInWindow:@"Please select gender"];
        return NO;
    }
    
    if ([accepted isEqualToString:@"0"]) {
        [appDelegate onCreateToastMsgInWindow:kAGREETERMS];
        return NO;
    }
    
    return YES;
}

#pragma mark -Textfield Delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    isFilled = YES;
    return YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (textField == Fname) {
        return YES;
    }
    else if (textField == Email && [Fname.text length] >= 4 ) {
        return YES;
    }
    else if (textField == Password && [YSSupport validateEmail:Email.text] && [Fname.text length] >= 4 ) {
        return YES;
    }
    else if (textField == phoneNumber && [Password.text length] >= 6 &&[YSSupport validateEmail:Email.text] && [Fname.text length] >= 4 ) {
        return YES;
    }
    else if (textField == pinCode && [self validateMobileNumber:phoneNumber.text] &&[Password.text length] >= 6 &&[YSSupport validateEmail:Email.text] && [Fname.text length] >= 4 ) {
        return YES;
    }
    if ([Fname.text length] < 4 || [Fname.text length] >=75)
    {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:NO];
        [appDelegate onCreateToastMsgInWindow:kVALIDUSERNAME];
        return NO;
    }
 
    else if (![YSSupport validateEmail:Email.text])
    {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:NO];
        [appDelegate viewRemoveFromSuperView];
        [appDelegate onCreateToastMsgInWindow:kVALIDEMAILID];
        return NO;
    }
   else  if ([Password.text length] < 6 || [Password.text length] >=16)
    {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:NO];
        [appDelegate onCreateToastMsgInWindow:kVALIDPASSWORDLENGTH];
        return NO;
    }
   else if (![self validateMobileNumber:phoneNumber.text])
   {
       UIWindow *window = [[UIApplication sharedApplication] keyWindow];
       [window.rootViewController.view setUserInteractionEnabled:NO];
       [appDelegate onCreateToastMsgInWindow:kVALIDMOBILENUMBER];
       isFilled = YES;
       return NO;
   }
   else if (![YSSupport isValidPinCode:pinCode.text])
   {
       UIWindow *window = [[UIApplication sharedApplication] keyWindow];
       [window.rootViewController.view setUserInteractionEnabled:NO];
       [appDelegate onCreateToastMsgInWindow:kVALIDPINCODE];
       isFilled = YES;
       return NO;
   }
    
return YES;
}


@end
