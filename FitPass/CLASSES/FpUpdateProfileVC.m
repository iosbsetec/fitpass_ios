//
//  FpSettingVC.m
//  FitPass
//
//  Created by JITENDRA on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FpUpdateProfileVC.h"
#import "UserProfileModel.h"
#import "AsyncImageView.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "MBProgressHUD.h"

@interface FpUpdateProfileVC ()
{
    IBOutlet UIView *viewSectionHeader;
    IBOutlet UILabel *lblHeaderText;
    UserProfileModel *model;
    UIFont *myFont ;
    UIView *footerView;
    UIButton *button;
    NSMutableData *responsedata;
    NSURLConnection *conn;
    NSString *strNewImage;
}

@property (strong, nonatomic) IBOutlet UIView *tblFooterView;
@end

@implementation FpUpdateProfileVC
@synthesize strFirstName,strEmailId,strAddress1,strAddress2,strCity,strStateCode,strZipCode,strPhoneNo,strGender,strImageUrl, strEndOfCycle;

- (IBAction)btnbackaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication]setStatusBarHidden:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [actIndicator startAnimating];
    isKeyboardShown = NO;
    [self devicesize];
    viewSegmentController.layer.cornerRadius = 5;
    [viewSegmentController setClipsToBounds:YES];
//    [self setUserInfomation];
    [self getUserProfile];
    btnUpdateImg.layer.cornerRadius = 5;
    [btnUpdateImg setClipsToBounds:YES];
    
    viewImgBG.layer.cornerRadius = viewImgBG.frame.size.height/2;
    [viewImgBG setClipsToBounds:YES];
    
    btnSave.layer.cornerRadius = 5;
    [btnSave setClipsToBounds:YES];
    
    strNewImage = nil;
    
    myFont = [ UIFont fontWithName: @"OPENSANS" size: 15.0 ];
    
    NSString *strNameAndMail = [[NSUserDefaults standardUserDefaults]objectForKey:@"UserNameandEmail"];
    FFirstName.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:0];
    FEmailId.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:1];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardDidHideNotification
                                               object:nil];
    
}

- (void)keyboardDidShow: (NSNotification *) notif
{
    isKeyboardShown = YES;
    [self devicesize];
}

- (void)keyboardDidHide: (NSNotification *) notif
{
    isKeyboardShown = NO;
    [self devicesize];
}


-(void)getUserProfile
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls getUserProfileWithData:submitData SuccessionBlock:^(id newResponse)
     {
         [MBProgressHUD  hideHUDForView:self.view animated:YES];

         Generalmodel *model1 = newResponse;
         model = [model1.tempArray lastObject];
         [self setUserInfomation];
         
     } andFailureBlock:^(NSError *error)
     {
         
     }];
}

//- (IBAction)btnUserTapped:(id)sender
//{
//    FPMyProfileVC *rootController = [[FPMyProfileVC alloc] initWithNibName:@"FPMyProfileVC" bundle:nil];
//    rootController.strImgUrl = model.profile_pic;
//    rootController.strGender = model.gender;
//    rootController.strEmailId = @"test@gmail.com";
//    rootController.strFirstName = model.first_name;
//    
//    [self.navigationController pushViewController:rootController animated:YES];
//}

-(void)setUserInfomation
{
    NSString *strNameAndMail = [[NSUserDefaults standardUserDefaults]objectForKey:@"UserNameandEmail"];
    FFirstName.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:0];
    FAddress1.text = model.address_line1;
    FAddress2.text = model.address_line2;
    FCity.text = model.city;
    FStateCode.text = model.state_name;;
    FZipCode.text = model.pin_code;
    FPhoneNo.text = model.mobile_no;
    strGender = model.gender;
    strEndOfCycle = model.end_of_cyle;
    
    NSString *strData =[NSString stringWithFormat:@"%@~~%@~~%@",FFirstName.text,FEmailId.text,strEndOfCycle];
    [[NSUserDefaults standardUserDefaults]setObject:strData forKey:@"UserNameandEmail"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    FEmailId.text = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:1];

    imgUserPhoto.layer.cornerRadius = imgUserPhoto.frame.size.height/2;
    [imgUserPhoto setClipsToBounds:YES];
    imgUserPhoto.layer.borderColor = [UIColor whiteColor].CGColor;
    imgUserPhoto.layer.borderWidth = 1.0;
    NSLog(@"user image %@",[[NSUserDefaults standardUserDefaults] valueForKey:FBUSERIMAGE] );
    imgUserPhoto.imageURL = [NSURL URLWithString: ([[[NSUserDefaults standardUserDefaults] valueForKey:FBUSERIMAGE] length])?[[NSUserDefaults standardUserDefaults] valueForKey:FBUSERIMAGE]:model.profile_pic];
    
    if ([strGender isEqualToString:@"1"])
    {
        [self onMaleTapped:nil];
    }
    else if ([strGender isEqualToString:@"2"])
    {
        [self onFemaleTapped:nil];
    }
    if ([strGender isEqualToString:@"3"])
    {
        [self onUnspecifiedTapped:nil];
    }
}

-(void)devicesize
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height < 568.0f)
        {
            [scrollViewSettings setContentSize:CGSizeMake(320, isKeyboardShown?1080:860)];
        }
        else
        {
            [scrollViewSettings setContentSize:CGSizeMake(320,isKeyboardShown?1030:810)];
        }
    }
    else if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [scrollViewSettings setScrollEnabled:YES];
            [scrollViewSettings setContentSize:CGSizeMake(scrollViewSettings.frame.size.width, isKeyboardShown?1550:1250)];
            [scrollViewSettings setContentOffset:CGPointZero];
        }
        else
        {
            [scrollViewSettings setScrollEnabled:YES];
            [scrollViewSettings setContentSize:CGSizeMake(scrollViewSettings.frame.size.width, isKeyboardShown?1510:1290)];
            [scrollViewSettings setContentOffset:CGPointZero];
        }
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField.tag==15)
    {
        return NO;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - ******* Validate Mobile Number *******
-(BOOL)validateMobileNumber:(NSString *)strNumber
{
    NSString *mobileNumberPattern = @"[789][0-9]{9}";
    NSPredicate *mobileNumberPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", mobileNumberPattern];
    
    return [mobileNumberPred evaluateWithObject:strNumber];
}

#pragma mark - ******* Validate Pincode *******
-(BOOL)isValidPinCode:(NSString*)pincode    {
    NSString *pinRegex = @"^[0-9]{6}$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:pincode];
    return pinValidates;
}

-(BOOL)checkUserGivenDetails
{
    appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (FFirstName.text.length==0 || FPhoneNo.text.length==0 || FZipCode.text.length==0)
    {
        [appDelegate onCreateToastMsgInWindow:@"All fields are mandatory"];
        return NO;
    }
    
    BOOL isMobileNumberOk = [self validateMobileNumber:FPhoneNo.text];
    BOOL isPincodeOk = [self isValidPinCode:FZipCode.text];
    
    
    if ([FFirstName.text length] < 4 || [FFirstName.text length] >=75)
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDUSERNAME];
        return NO;
    }
    
    if (!isMobileNumberOk)
    {
        [appDelegate onCreateToastMsgInWindow:@"Please check your Mobile Number"];
        return NO;
    }
    
    if (!isPincodeOk)
    {
        [appDelegate onCreateToastMsgInWindow:@"Please check your Pincode"];
        return NO;
    }
    
    return YES;
}

-(IBAction)btnSaveTapped:(id)sender
{
    BOOL isOK = [self checkUserGivenDetails];
    
    if (isOK)
    {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:NO];
        
        if (![YSAPICalls isNetworkRechable])
        {
            [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
            return;
        }
        
        NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&first_name=%@&mobile_no=%@&email_address=%@&gender=%@&address_line1=%@&address_line2=%@&city=%@&state_name=%@&pin_code=%@",FITPASS_USER_ID,FFirstName.text,FPhoneNo.text,FEmailId.text,strGender,FAddress1.text,FAddress2.text,FCity.text,FStateCode.text,FZipCode.text] dataUsingEncoding:NSUTF8StringEncoding];
        
        
        NSString *strNameAndMail = [[NSUserDefaults standardUserDefaults]objectForKey:@"UserNameandEmail"];
        strNameAndMail = [[strNameAndMail componentsSeparatedByString:@"~~"] objectAtIndex:2];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [YSAPICalls updateUserProfileWithData:submitData SuccessionBlock:^(id newResponse)
         {
             [MBProgressHUD  hideHUDForView:self.view animated:YES];

             NSString *strData =[NSString stringWithFormat:@"%@~~%@~~%@",FFirstName.text,FEmailId.text,strNameAndMail];
             
             [[NSUserDefaults standardUserDefaults]setObject:strData forKey:@"UserNameandEmail"];
             [[NSUserDefaults standardUserDefaults]synchronize];
             
             Generalmodel *model1 = newResponse;
             
             SETVALUE(FPhoneNo.text, USERMOBILE);
             SETVALUE(FFirstName.text, USERNAME);
             SYNCHRONISE;
             appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
             [appDelegate onCreateToastMsgInWindow:model1.status_Msg];
             
         } andFailureBlock:^(NSError *error) {
             
         }];
    }
}

- (IBAction)segmentTapped:(UIButton *)sender
{
    [self performSelector:@selector(deselectAll)];
    [((UIButton *) sender) setSelected:YES];
    strGender = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    [((UIButton *) sender) setBackgroundColor:[UIColor colorWithRed:20.0/255.0 green:135.0/255  blue:115.0/255  alpha:1.0]
     ];
    [((UIButton *) sender) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(IBAction)onMaleTapped:(id)sender
{
//    [self performSelector:@selector(deselectAll)];
    [btnMale setBackgroundColor:[UIColor colorWithRed:20.0/255.0 green:135.0/255.0  blue:115.0/255.0  alpha:1.0]
     ];
    strGender = @"1";
    [btnFemale setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
    [btnUnspecified setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
//    [((UIButton *) sender) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(IBAction)onFemaleTapped:(id)sender
{
//    [self performSelector:@selector(deselectAll)];
    [btnFemale setBackgroundColor:[UIColor colorWithRed:20.0/255.0 green:135.0/255.0  blue:115.0/255.0  alpha:1.0]];
    
    [btnUnspecified setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
    strGender = @"2";
    
    [btnMale setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
//    [((UIButton *) sender) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(IBAction)onUnspecifiedTapped:(id)sender
{
//    [self performSelector:@selector(deselectAll)];
    [btnUnspecified setBackgroundColor:[UIColor colorWithRed:20.0/255.0 green:135.0/255.0  blue:115.0/255.0  alpha:1.0]
     ];
    strGender = @"3";
    [btnFemale setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
    [btnMale setBackgroundColor:[UIColor colorWithRed:32.0/255.0 green:186/255.0  blue:150/255.0  alpha:1.0]];
//    [((UIButton *) sender) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (void) deselectAll
{
    NSArray *views = [viewSegmentController subviews];
    for (UIView *v in views)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            [((UIButton *)v) setSelected:NO];
            [((UIButton *) v) setBackgroundColor:TILLDCOLOR];
            [((UIButton *) v) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
}

-(IBAction)btnUpdateImageTapped:(id)sender
{
    [self btnChangeImageTapped:nil];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)uploadImage
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    UIImage *img = [self imageWithImage:imgUserPhoto.image scaledToSize:CGSizeMake(120, 120)];
    NSData *imgData = UIImagePNGRepresentation(img);
    
    NSString *strPostUrl = @"http://fitpass.co.in/api/user/uploadPhoto";
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:strPostUrl]];
    
    [request setValue:@"jludmkiswzxmrdf3qewfuhasqrcmdjoqply" forHTTPHeaderField:@"X-APPKEY"];
    [request setValue:@"185EA28DB5E8C0D3B1C9BCE633596943" forHTTPHeaderField:@"X-AUTHKEY"];
    [request setValue:@"iOS" forHTTPHeaderField:@"X-DEVICE"];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"ebrj6b3qril766r";
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];

    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Disposition: form-data; name=\"user_id\"\r\n\r\n"
                          dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[FITPASS_USER_ID  dataUsingEncoding:NSUTF8StringEncoding]];

    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];

    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];

    [body appendData:[@"Content-Disposition: form-data; name=\"profile_pic\"; filename=\"images.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [body appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imgData];
    [body appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue]completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSString* newStr = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         NSLog(@"dict = %@",newStr);
         if ([[[NSUserDefaults standardUserDefaults] valueForKey:FBUSERIMAGE] length])
         [[NSUserDefaults standardUserDefaults] removeObjectForKey:FBUSERIMAGE];
         [MBProgressHUD  hideHUDForView:self.view animated:YES];
     }];
}

-(IBAction)btnChangeImageTapped:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Take Photo",
                            @"Select from Gallery",
                            nil];
    popup.tag = 1;

    if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [popup showFromRect:btnUpdateImg.frame inView:self.view animated:YES];
        }
        else
        {
            [popup showInView:[UIApplication sharedApplication].keyWindow];
        }
    }
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        [self takePhoto];
    }
    else if (buttonIndex==1)
    {
        [self selectPhoto];
    }
}

- (void)takePhoto
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        [myAlertView show];
    }
    else
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
        {
            [[NSOperationQueue mainQueue] addOperationWithBlock:^{
                
                [self presentViewController:picker animated:NO completion:nil];
            }];
        }
        else
        {
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }
}

- (void)selectPhoto
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue]>=8.0)
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            [self presentViewController:picker animated:NO completion:nil];
        }];
        
    }
    else
    {
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    NSURL *refURL = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    // for Getting Selected Image Name
    ALAssetsLibraryAssetForURLResultBlock resultblock = ^(ALAsset *imageAsset)
    {
        ALAssetRepresentation *imageRep = [imageAsset defaultRepresentation];
        NSLog(@"[imageRep filename] : %@", [imageRep filename]);
        strNewImage = [imageRep filename];
    };
    
    ALAssetsLibrary* assetslibrary = [[ALAssetsLibrary alloc] init];
    [assetslibrary assetForURL:refURL resultBlock:resultblock failureBlock:nil];
    
    imgUserPhoto.image = chosenImage;
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [self uploadImage];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
