//
//  CuisineListTable.h
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//  Copyright (c) 2015 WemakeAppz. All rights reserved.
//

#import <UIKit/UIKit.h>






@class FilterListTable;
@protocol FilterListTableDelegate
- (void) FilterListTableDelegateMethod: (FilterListTable *) sender selectedIndex :(NSString *) selectedRowNo andText : (NSString *)text;
- (void) FilterListTableDelegateMethodDone:(BOOL)isSelected;

@end

@interface FilterListTable : UIView <UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate>
{
    NSString *animationDirection;
    UIImageView *imgView;
    
}
@property (nonatomic,retain) UISearchBar*  searchBar;
@property (nonatomic,assign)BOOL isAlreadySelected;
@property (assign)BOOL isOpen;
@property (nonatomic,retain)NSIndexPath *selectIndex;
@property (nonatomic, retain) id <FilterListTableDelegate> delegate;

-(void)hideDropDown;
-(id)showFilterListWithFrame : (CGRect) frame isAlreadySelected:(BOOL)isSelected;

@end


