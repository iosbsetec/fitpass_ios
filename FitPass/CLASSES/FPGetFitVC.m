//
//  FPGetFitVC.m
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "FPGetFitVC.h"
#import "WorkOutDetailsVC.h"
#import "PaymentPageViewController.H"
@interface FPGetFitVC ()
@property (weak, nonatomic) IBOutlet UILabel *userName;

@end



@implementation FPGetFitVC
@synthesize btnpurchasenow,btn_inapp,btn_PayU;

- (void)viewDidLoad {
    [super viewDidLoad];
  btnpurchasenow.layer.cornerRadius = 4.0f;
  btnpurchasenow.clipsToBounds = YES;

    _userName.text = [NSString stringWithFormat:@"Hey %@",GETVALUE(USERNAME)];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnbackaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)radiobuttonAction:(UIButton *)Button
{
    if(![Button isSelected])
    {
        [Button setSelected:YES];
        
        if (Button == btn_PayU) {
            [btn_inapp setSelected:NO];

        }
        else
            
        {

            [btn_PayU setSelected:NO];
        }
    }
    else
    {
        [Button setSelected:NO];
    }
}

- (IBAction)payClicked:(id)sender
{
    //btnpurchasenow.enabled = NO;
    PaymentPageViewController *pcontrol;
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        pcontrol = [[PaymentPageViewController alloc]initWithNibName:@"PaymentPageViewController" bundle:nil];
    }
    else if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        pcontrol = [[PaymentPageViewController alloc]initWithNibName:@"PaymentPageViewController_ipad" bundle:nil];
    }
    
    pcontrol.strStudioName = _strStudioName;
    [self.navigationController pushViewController:pcontrol animated:YES];
}

@end
