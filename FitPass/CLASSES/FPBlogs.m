//
//  FPBlogs.m
//  FitPass
//
//  Created by JITENDRA on 7/10/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPBlogs.h"

@interface FPBlogs ()<UIWebViewDelegate>
{

    IBOutlet UIWebView *BlogWebview;
    NSString *urlAddress;
    __weak IBOutlet UILabel *lblSelectedTwit;
    __weak IBOutlet UILabel *lblSelected;
    __weak IBOutlet UILabel *lblInstagram;
    __weak IBOutlet UILabel *lblFitpass;
    
    NSString *weburllink;
}
- (IBAction)facebookTwitterTapped:(UIButton *)sender;

@end

@implementation FPBlogs

- (void)viewDidLoad {
    [super viewDidLoad];
   // BlogWebview.scalesPageToFit = YES;
    
    weburllink = @"https://www.facebook.com/fitpassindia";
    
    [self laodwebView:weburllink];
    BlogWebview.delegate=self;
}

-(void)laodwebView :(NSString *)weburl
{
    NSURL *url = [NSURL URLWithString:weburl];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [BlogWebview loadRequest:requestObj];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    lblSelected.backgroundColor = [UIColor colorWithRed:51/255.0 green: 181/255.0 blue: 229/255.0 alpha: 1.0];
}



- (IBAction)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)facebookTwitterTapped:(UIButton *)sender
{
    if ([sender tag] == 0)
    {
        NSLog(@"FaceBook");
        
        weburllink = @"https://www.facebook.com/fitpassindia";
        
        [self laodwebView:weburllink];

        lblSelected.backgroundColor = [UIColor colorWithRed:51/255.0 green: 181/255.0 blue: 229/255.0 alpha: 1.0];
        lblSelectedTwit.backgroundColor = [UIColor clearColor];
        lblInstagram.backgroundColor = [UIColor clearColor];
        lblFitpass.backgroundColor   = [UIColor clearColor];
    }
    else if ([sender tag] == 1)
    {
         NSLog(@"Twitter");
        NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"fitpass_twitter" ofType:@"html"];
        NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
        [BlogWebview loadHTMLString:htmlString baseURL:nil];
        
        lblSelectedTwit.backgroundColor = [UIColor colorWithRed:51/255.0 green: 181/255.0 blue: 229/255.0 alpha: 1.0];
        lblSelected.backgroundColor     = [UIColor clearColor];
        lblInstagram.backgroundColor    = [UIColor clearColor];
        lblFitpass.backgroundColor      = [UIColor clearColor];
    }
    else if ([sender tag] == 2)
    {
        weburllink = @"https://instagram.com/fitpassindia";
        [self laodwebView:weburllink];
        lblInstagram.backgroundColor    = [UIColor colorWithRed:51/255.0 green: 181/255.0 blue: 229/255.0 alpha: 1.0];
        lblSelectedTwit.backgroundColor = [UIColor clearColor];
        lblSelected.backgroundColor     = [UIColor clearColor];
        lblFitpass.backgroundColor      = [UIColor clearColor];
    }
    else
    {
         weburllink = @"http://fitpass.co.in/blog";
        [self laodwebView:weburllink];
        lblInstagram.backgroundColor    = [UIColor clearColor];
        lblSelectedTwit.backgroundColor = [UIColor clearColor];
        lblSelected.backgroundColor     = [UIColor clearColor];
        lblFitpass.backgroundColor      = [UIColor colorWithRed:51/255.0 green: 181/255.0 blue: 229/255.0 alpha: 1.0];
        
    }
}

@end
