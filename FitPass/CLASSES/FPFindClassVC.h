//
//  FPFindClassVC.h
//  FitPass
//
//  Created by JITENDRA on 7/8/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FPFindClassVC : UIViewController
{
    IBOutlet UIButton *btnPreviousDate;
    IBOutlet UIButton *btnNextDate;
    NSInteger nIncOrDecDate;
    IBOutlet UIButton *btnRefineSearch;
    IBOutlet UIImageView *imgRefineSearch;
    
    IBOutlet UIImageView *imgPrevious;
    IBOutlet UIImageView *imgNextArrow;
    IBOutlet UILabel *lblFavoriteWorkout;
    IBOutlet UIButton *btnSearchButton;
}

@property(nonatomic,assign)BOOL isComeFromFavorite;
- (IBAction)getfittapped:(id)sender;
- (IBAction)btnNextDateTapped:(id)sender;
- (IBAction)btnPreviousDateTapped:(id)sender;
@end
