//
//  AppDelegate.h
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
// com.bsetec.bitpasnew
// com.fitpassBusinessVenture.fitpass
#define heightforiPhone6 1050
#define heightforiPhone5 865

#define APP_DELEGATE ((FPAppDelegate *)[[UIApplication sharedApplication] delegate])

#import <UIKit/UIKit.h>
#import "KeychainItemWrapper.h"

#import "SPHKeyChain.h"

@interface FPAppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, retain) KeychainItemWrapper *keychainItemWrapper;

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UINavigationController *navigationController;
-(void)onCreateToastMsgInWindow:(NSString *)strMsg;
-(void)viewRemoveFromSuperView;
- (void) setRootViewController;
-(NSString*)getDeviceID
;
@end

