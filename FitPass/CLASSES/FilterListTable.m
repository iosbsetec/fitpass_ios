//
//  CuisineListTable.m
//  GebeChat
//
//  Created by Siba Prasad Hota  on 6/13/15.
//

#define arrayRightTableData @[@"SEARCH BY",@"CATEGORY"]
#define arraySearchBy       @[@"All Members",@"My Mints",@"My C-suite",@"Planned to do",@"Nomintated"]
#define arrayCategory       @[@"BeingHappy",@"Career",@"EnterPrenure",@"ExerCise",@"Family",@"Leadership",@"Health"]

#import "FilterListTable.h"
#import "FPAppDelegate.h"
#import "Keys.h"
#import "DBManager.h"
@interface FilterListTable () 
{
    BOOL isSearch;
    NSArray *studioArray;
    NSMutableArray *searchResults;
    BOOL isSearching;
    BOOL isNorecordFound;
}
@property(nonatomic, strong) UITableView *table;
@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation FilterListTable

@synthesize table,searchBar;
@synthesize isAlreadySelected;


-(id)showFilterListWithFrame : (CGRect) frame isAlreadySelected:(BOOL)isSelected
{
  
    self.table = (UITableView *)[super init];
    if (self) {
        isSearching = YES;
        isNorecordFound = NO;
        UIView *viewTop = [[UIView alloc]initWithFrame:CGRectMake(0, 0,CGRectGetWidth(frame), 49)];
        
        searchResults = [NSMutableArray array];

        self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"fitpassdb.sql"];

       searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 5, CGRectGetWidth([UIScreen mainScreen].bounds) - 65, 40)];
        searchBar.placeholder  = @"Find a studio";
        searchBar.tintColor = [UIColor clearColor];
        searchBar.delegate = self;
      //  [searchBar setBackgroundImage: [UIImage imageNamed:@"yourImage"]];//just add here gray image which you display in quetion
//        [searchBar setst:UITextBorderStyleNone];
       // searchBar.backgroundColor  = [UIColor blackColor];
        searchBar.barStyle = UIBarStyleBlack;
        [viewTop addSubview:searchBar];

        UIButton *searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        searchButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 70,12.0, 60.0, 27.0);
        
        if (isSelected)
            [searchButton setTitle:@"CLEAR" forState:UIControlStateNormal];
        else
            [searchButton setTitle:@"DONE" forState:UIControlStateNormal];
        
        searchButton.backgroundColor = [UIColor clearColor];
        searchButton.layer.cornerRadius = 4.0;
        [searchButton setTitleColor:TILLDCOLOR forState:UIControlStateNormal ];
        [searchButton.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];

        [searchButton addTarget:self action:@selector(onDoneTapped:) forControlEvents:UIControlEventTouchUpInside];
        [viewTop addSubview:searchButton];

        /* Section header is in 0th index... */
        UIColor * Backcolor = [UIColor colorWithRed:38/255.0f green:38/255.0f blue:34/255.0f alpha:1.0f];
        [viewTop setBackgroundColor:Backcolor];
        self.backgroundColor = [UIColor whiteColor];
        self.layer.masksToBounds = NO;
    //  self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 50, CGRectGetWidth(frame), CGRectGetHeight(frame)-50)];
        table.delegate = self;
        table.dataSource = self;
//        table.layer.cornerRadius = 5;
        table.backgroundColor = [UIColor whiteColor];
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
    //  table.separatorColor = [UIColor grayColor];
        
        FPAppDelegate *appDell = (FPAppDelegate*)[[UIApplication sharedApplication]delegate];
        [appDell.window addSubview:self];
        self.backgroundColor = [UIColor colorWithRed:244.0/255 green:244.0/255 blue:244.0/255 alpha:1];
        [self setFrame:frame];
        [self addSubview:table];
        [self addSubview:viewTop];
        [self loadData];
    }
    return self;
}

-(void)onDoneTapped:(UIButton *)sender
{
    if ([sender.titleLabel.text isEqualToString:@"CLEAR"])
        [self.delegate FilterListTableDelegateMethodDone:YES];
    else
        [self.delegate FilterListTableDelegateMethodDone:NO];
}

-(void)hideDropDown
{
    [self removeFromSuperview];
}

//- (UIButton *)addMyButton
//{    // Method for creating button, with background image and other properties
//    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    playButton.frame = CGRectMake(0.0,10.0, 30.0, 30.0);
//    [playButton setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//
//   // [searchButton setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
//    playButton.backgroundColor = [UIColor clearColor];
//    [playButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal ];
//    [playButton addTarget:self action:@selector(hideDropDown) forControlEvents:UIControlEventTouchUpInside];
//    return playButton;
//}


#pragma mark - UITableViewDelegate implementation

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return  (searchResults.count)?searchResults.count:1;
}


-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([searchResults count] == 0 && isSearching) {
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        static NSString *simpleTableIdentifier = @"NoItems";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];

        }
        cell.textLabel.text = @"No results were found.";
        cell.textLabel.font = [UIFont fontWithName:@"OPENSANS" size:14];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        

        return cell;
    }
    else
    {

    UIView *separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, 35, CGRectGetWidth([UIScreen mainScreen].bounds), 1)];
    separatorView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    separatorView.alpha = 0.1;
    separatorView.layer.borderWidth = 0.4;
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
        cell.textLabel.text = [[searchResults objectAtIndex:indexPath.row] lastObject ];
        UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth([UIScreen mainScreen].bounds), 36)];
        bgView.backgroundColor = TILLDCOLOR;
        [cell.selectedBackgroundView addSubview:bgView];
    cell.textLabel.font = [UIFont fontWithName:@"OPENSANS" size:14];
    [cell.contentView addSubview:separatorView];
    return cell;
    }

}
#pragma mark - Private method implementation
-(void)loadData{
    // Form the query.
 
     NSString *query     = @"select * from StudioList";
    
    // Get the results.
    if (studioArray != nil) {
        studioArray = nil;
    }
    studioArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    [searchResults addObjectsFromArray:studioArray];
    
   // NSLog(@"StudioList array Count %@",studioArray);
    // Reload the table view.
    [table reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     if ([searchResults count] > 0 )
     {
         NSString *selectedStudioId =  [[searchResults objectAtIndex:indexPath.row] objectAtIndex:0];
         [self.delegate FilterListTableDelegateMethod:self selectedIndex:selectedStudioId andText:[[searchResults objectAtIndex:indexPath.row] lastObject]] ;
     }
    [self.table deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    NSLog(@"began");  // this executes as soon as i tap on the searchbar, so I'm guessing this is the place to put whatever solution is available
}

- (void)searchTableList
{
    NSString *searchString = searchBar.text;
 
    for (NSArray *tempStr in studioArray)
    {
        NSComparisonResult result = [[tempStr objectAtIndex:1] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        
        // NSComparisonResult result=[[tempStr objectAtIndex:0] caseInsensitiveCompare:searchString];
        if (result == NSOrderedSame)
        {
            [searchResults addObject:tempStr];
            NSLog(@"foundddd");
        }
        else
        {
            NSLog(@"not  foundddd");
            [table reloadData];
        }
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    isSearching = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"Text change - %d",isSearching);
    NSLog(@"Text change - %@",searchText);
    //Remove all objects first.
    [searchResults removeAllObjects];
    
    if([searchText length] != 0)
    {
        [self searchTableList];
    }
    else
    {
        [searchResults addObjectsFromArray:studioArray];
    }
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [table reloadData];
    NSLog(@"TtblList reloadData]");
}

@end
