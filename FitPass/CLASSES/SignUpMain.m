//
//  FourthViewController.m
//  pageControl
//
//  Created by Bsetec on 04/07/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "SignUpMain.h"
#import "SignUpVC.h"

#import "SPHKeyChain.h"
#import "FPMenuVC.h"
#import "AsyncImageView.h"
@interface SignUpMain ()
{
    NSString *fNmae,*email;
    __weak IBOutlet UIImageView *imgVuserPhoto;
    __weak IBOutlet UIButton *btnCancel;


}
@property (weak, nonatomic) IBOutlet UIButton *btnCreateAccount;
@property (weak, nonatomic) IBOutlet UITextField *txtFldPincode;
- (IBAction)signUpwithEmail:(UIButton *)sender;
- (IBAction)cancelTapped:(UIButton *)sender;

@end

@implementation SignUpMain
@synthesize FeceBook,Email;
- (void)viewDidLoad {
    [super viewDidLoad];
    FeceBook.layer.cornerRadius = 4.0;
    [FeceBook setClipsToBounds:YES];
        _btnCreateAccount.layer.cornerRadius = 5;
    btnCancel.layer.cornerRadius = 5;
    [ _btnCreateAccount setHighlighted:NO];

    Email.layer.cornerRadius = 4.0;
    [Email setClipsToBounds:YES];
    // Do any additional setup after loading the view from its nib.
    [_btnCreateAccount setUserInteractionEnabled:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)Back:(id)sender{
    //ThirdViewController *third = [[ThirdViewController alloc]initWithNibName:@"ThirdViewController" bundle:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)signUpWithFacebook:(UIButton *)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions: @[@"public_profile",@"email"]
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled) {
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    
                                    if ([FBSDKAccessToken currentAccessToken]) {
                                        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, first_name, last_name, email"}]
                                         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                                             if (!error) {
                                                 NSLog(@"fetched user:%@", result);
                                                 
                                                 fNmae  = [result valueForKey:@"first_name"];
                                                 email  = [result valueForKey:@"email"];
                                                 
                                                 [[NSUserDefaults standardUserDefaults]setObject:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200",[result valueForKey:@"id"]] forKey:FBUSERIMAGE];
                                                 SYNCHRONISE;

                                                // imgVuserPhoto.imageURL= [ NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=200&height=200",[result valueForKey:@"id"]]];
                                                 [self createAccount];
                                                 
                                             }
                                         }];
                                    }

                                    
                                                                  }
                            }];
  
}

- (IBAction)signUpwithEmail:(UIButton *)sender
{
    SignUpVC *rootController;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        rootController = [[SignUpVC alloc] initWithNibName:@"SignUpVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        rootController = [[SignUpVC alloc] initWithNibName:@"SignUpVC_ipad" bundle:nil];
    }
    
    [self.navigationController pushViewController:rootController animated:YES];
}

- (IBAction)createAccount
{
   FPAppDelegate* appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];

    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }

    
    NSData *submitData = [[NSString stringWithFormat:@"first_name=%@&email_address=%@&password=%@&device_id=%@&device_token=%@&device_type=iOS",fNmae,email,email,[appDelegate getDeviceID],GETVALUE(DEVICE_TOKEN)] dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString * dataFinal = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];

    NSLog(@"final data %@",dataFinal);
    [YSAPICalls getSignUpSignInDetailsFBParamData:submitData SuccessionBlock:^(id newResponse)
 {
     //  isReserveDetailsRetrive = YES;
     Generalmodel *model = newResponse;
     if ([model.status_code isEqualToString:@"0"])
     {
         [appDelegate onCreateToastMsgInWindow:[model.status_Msg stringByAppendingString:@"Please login"]];
     }
     else
     {
         if ([model.status_code isEqualToString:@"0"])
         {
             [appDelegate onCreateToastMsgInWindow:model.status_Msg];
         }
         else
         {
//             [[NSUserDefaults standardUserDefaults]setObject:model.user_id forKey:@"Userid"];
//             [[NSUserDefaults standardUserDefaults]setObject:model.authtentication forKey:@"Authtentication"];
//             [[NSUserDefaults standardUserDefaults]synchronize];
             
             SETVALUE(model.user_id, @"Userid");
             SETVALUE(model.authtentication, @"Authtentication");
             SETVALUE(model.end_of_cyle, @"endofcycle");
             SETVALUE(@"", USERMOBILE);
             SETVALUE(fNmae, USERNAME);
             SETVALUE(email, kUSEREMAIL);
             SYNCHRONISE;
             
             UIViewController *rootController;
             if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
             {
                 rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
             }
             else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
             {
                 rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
             }
             
             [self.navigationController pushViewController:rootController animated:YES];
         }
     }
     
 } andFailureBlock:^(NSError *error)
     {

 }];

}
@end
