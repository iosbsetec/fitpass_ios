//
//  FpSettingVC.h
//  FitPass
//
//  Created by JITENDRA on 7/24/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPPrivatePolicyVC.h"
#import "FpFAQSVC.h"
#import "FPTermsforUsers.h"
#import "FpUpdateProfileVC.h"

@interface FpSettingVC : UIViewController
- (IBAction)btnLogout:(id)sender;

@end
