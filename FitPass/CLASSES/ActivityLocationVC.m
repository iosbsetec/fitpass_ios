//
//  ActivityLocationVC.m
//  FitPass
//
//  Created by JITENDRA on 8/7/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "ActivityLocationVC.h"
#import "DBManager.h"
@interface ActivityLocationVC ()

{
    IBOutlet UITableViewCell *cellNoresultFound;
    __weak IBOutlet UILabel *lblTitle;
    __weak IBOutlet UITableView *tblList;
    NSArray *activityArray;
    NSMutableArray *selectedIndexes;
    NSMutableArray *searchResults;
    BOOL isSearching;
    BOOL isNorecordFound;
    NSString *strItemList;

    __weak IBOutlet UISearchBar *searchBarList;
}
@property (nonatomic, strong) DBManager *dbManager;
- (IBAction)doneTapped:(UIButton *)sender;

@end

@implementation ActivityLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [searchBarList setTintColor:[UIColor darkGrayColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTintColor:[UIColor darkGrayColor]];
    
    // Do any additional setup after loading the view from its nib.
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"fitpassdb.sql"];
    selectedIndexes = [NSMutableArray array];
    searchResults = [NSMutableArray array];
    lblTitle.text = _headedrtitle;
    [self loadData];
    isSearching = NO;
    isNorecordFound = NO;
    searchBarList.placeholder = ([_headedrtitle isEqualToString:@"ACTIVITY"])?@"Search for activity": @"Search for location";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - Private method implementation
-(void)loadData{
    // Form the query.
    NSString *query;
    if ([_headedrtitle isEqualToString:@"ACTIVITY"]) {
        query = @"select * from Activity";
    }
    else
        query = @"select * from Locality";
    
    // Get the results.
    if (activityArray != nil) {
        activityArray = nil;
    }
    activityArray = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
   // NSLog(@"activity array Count %@",activityArray);
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *arrDatas;
    
    if ([_headedrtitle isEqualToString:@"ACTIVITY"])
        arrDatas = [defaults objectForKey:@"getactivitylist"];
    else
        arrDatas = [defaults objectForKey:@"getnearestlist"];
     
     if (arrDatas.count>0)
     {
         selectedIndexes = [[NSMutableArray alloc]initWithArray:arrDatas];
     }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isSearching)
    {
        return ([searchResults count])?[searchResults count]:1;
    }
    else
        return  [activityArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([searchResults count] == 0 && isSearching)
    {
        tblList.separatorStyle = UITableViewCellSeparatorStyleNone;
        return cellNoresultFound;
    }
    else
    {
        static NSString *simpleTableIdentifier = @"SimpleTableItem";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];

        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
            tblList.separatorStyle = UITableViewCellSeparatorStyleNone;
        }
        cell.accessoryView = nil;
        tblList.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        for (int i = 0; i < selectedIndexes.count; i++)
        {
            NSUInteger num = [[selectedIndexes objectAtIndex:i] intValue];
            
            if (num == indexPath.row)
            {
                break;
            }
        }
        
        for (int i = 0; i < selectedIndexes.count; i++)
        {
            NSUInteger num = [[selectedIndexes objectAtIndex:i] intValue];
            if (num == indexPath.row)
            {
                tableView.separatorColor = [UIColor whiteColor];
                cell.backgroundColor = TILLDCOLOR;
                cell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
            }
        }
        
        cell.textLabel.text = (isSearching)?[[searchResults objectAtIndex:indexPath.row] lastObject ]:[[activityArray objectAtIndex:indexPath.row] lastObject ];
        cell.textLabel.font = [UIFont fontWithName:@"OPENSANS" size:16];
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    if (selectedCell.accessoryView)
    {
        selectedCell.accessoryView = nil;
        [selectedIndexes removeObject:[NSNumber numberWithInt:(int)indexPath.row]];
        selectedCell.textLabel.textColor = [UIColor blackColor];
        selectedCell.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        [selectedIndexes addObject:[NSNumber numberWithInt:(int)indexPath.row]];
        selectedCell.accessoryView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@""]];
        selectedCell.backgroundColor = TILLDCOLOR;
        selectedCell.textLabel.textColor = [UIColor whiteColor];
    }
    
     [tableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{

}// called when cancel button pressed

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    NSLog(@"began");  // this executes as soon as i tap on the searchbar, so I'm guessing this is the place to put whatever solution is available
}

- (void)searchTableList {
    NSString *searchString = searchBarList.text;
    
    for (NSArray *tempStr in activityArray) {
        NSComparisonResult result = [[tempStr objectAtIndex:1] compare:searchString options:(NSCaseInsensitiveSearch|NSDiacriticInsensitiveSearch) range:NSMakeRange(0, [searchString length])];
        
        // NSComparisonResult result=[[tempStr objectAtIndex:0] caseInsensitiveCompare:searchString];
        if (result == NSOrderedSame) {
            [searchResults addObject:tempStr];
             NSLog(@"foundddd");
        }
        else{
            NSLog(@"not  foundddd");
//            [tblList reloadData];
        }
        
       
    }
    
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"Text change - %d",isSearching);
    NSLog(@"Text change - %@",searchText);
    
    //Remove all objects first.
    [searchResults removeAllObjects];
    
    if([searchText length] != 0) {
        isSearching = YES;
        [self searchTableList];
    }
    else {
        isSearching = NO;
    }
    tblList.separatorStyle = UITableViewCellSeparatorStyleSingleLine;

    [tblList reloadData];
    NSLog(@"TtblList reloadData]");

}

- (IBAction)btnbackaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneTapped:(UIButton *)sender
{
    NSMutableString *ItemList = [[NSMutableString alloc]init];
    NSMutableString *location_id_str = [[NSMutableString alloc]init];
    NSString * comma = @"";
    
//    if (selectedIndexes.count==0)
//    {
//        [self btnbackaction:nil];
//    }
//    else
//    {
    for (NSNumber *obj  in selectedIndexes)
    {
        if (isSearching)
        {
            if ([searchResults count])
            {
                [ItemList appendString:[NSString stringWithFormat:@"%@%@", comma,([_headedrtitle isEqualToString:@"ACTIVITY"])? [[searchResults objectAtIndex:[obj integerValue]] lastObject]:[[searchResults objectAtIndex:[obj integerValue]] objectAtIndex:1]]]; //
                
                [location_id_str appendString:[NSString stringWithFormat:@"%@%@", comma,([_headedrtitle isEqualToString:@"ACTIVITY"])? [[searchResults objectAtIndex:[obj integerValue]] lastObject]:[[searchResults objectAtIndex:[obj integerValue]] firstObject]]];
            }
        }
        else
        {
            [ItemList appendString:[NSString stringWithFormat:@"%@%@", comma, ([_headedrtitle isEqualToString:@"ACTIVITY"])?[[activityArray objectAtIndex:[obj integerValue]] lastObject]:[[activityArray objectAtIndex:[obj integerValue]] objectAtIndex:1]]];
            [location_id_str appendString:[NSString stringWithFormat:@"%@%@", comma, ([_headedrtitle isEqualToString:@"ACTIVITY"])?[[activityArray objectAtIndex:[obj integerValue]] firstObject]:[[activityArray objectAtIndex:[obj integerValue]] firstObject]]];
        }
        comma = @", ";
    }
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if ([_headedrtitle isEqualToString:@"ACTIVITY"])
    {
        [defaults setObject:selectedIndexes forKey:@"getactivitylist"];
        
    }
    else
    {
        [defaults setObject:selectedIndexes forKey:@"getnearestlist"];
    }
    [defaults synchronize];


    NSLog(@"string is %@",ItemList);
    NSLog(@"string is %@",location_id_str);

    [self.delegate ActivityLocationTableDelegateMethod:ItemList headerTitle:_headedrtitle Locationid:location_id_str] ;
    [self btnbackaction:nil];
//    }
    
}
@end
