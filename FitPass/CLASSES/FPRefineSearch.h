//
//  FPRefineSearch.h
//  FitPass
//
//  Created by JITENDRA on 7/9/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "NMRangeSlider.h"
#import "RefineActivitymodel.h"
#import <CoreLocation/CoreLocation.h>

@protocol FPRefineSearchDelegate
- (void) clearData;

@end

@interface FPRefineSearch : UIViewController<CLLocationManagerDelegate>
{
    __weak IBOutlet UIButton *btnActivity;
    __weak IBOutlet UIButton *btnNearLocation;
    IBOutlet UIButton *btnUseCurrLocation;
    float latitude;
    float longitude;
    IBOutlet UIImageView *imgLocation;
}
@property (nonatomic, retain) id <FPRefineSearchDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *lowerLabel;
@property (weak, nonatomic) IBOutlet UILabel *upperLabel;
//- (IBAction)labelSliderChanged:(NMRangeSlider*)sender;
- (IBAction)btnClearData:(id)sender;
- (IBAction)btnUseCurrLocation:(id)sender;
@property (strong ,nonatomic) RefineActivitymodel *refineActivityData;

@end
