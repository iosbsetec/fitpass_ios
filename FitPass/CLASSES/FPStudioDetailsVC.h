//
//  FPFindStudioVC.h
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FavouriteStudioVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
@class StudioModel;
@interface FPStudioDetailsVC : UIViewController <MKMapViewDelegate,CLLocationManagerDelegate>
{
    
    IBOutlet UILabel *lblratings;
    CLLocationManager *locmanagerobj;
    CLLocation *locationobj;
    IBOutlet UIView *viewLocation;
     IBOutlet UIView *viewAmenities;
}


@property(strong,nonatomic)IBOutlet UIScrollView *studiodetailsscrollview;
@property (strong ,nonatomic) StudioModel *studioData;
@property (strong ,nonatomic) NSString *studeo_id;
- (IBAction)btnbackaction:(id)sender;
- (IBAction)btnscheduleaction:(id)sender;

@end
