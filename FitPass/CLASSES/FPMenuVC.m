//
//  FPMenuVC.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPMenuVC.h"
#import "FPBlogs.h"
#import "FPFindSudioVC.h"
#import "FPFindClassVC.h"
#import "FPUpcomingVC.h"
#import "FavouriteStudioVC.h"
#import "FPStudioDetailsVC.h"
#import "FPGetFitVC.h"
#import "WorkOutDetailsVC.h"
@interface FPMenuVC ()
{
    IBOutlet UIButton *btnGetFit;

    IBOutlet UIView *viewContainer;

    IBOutlet UILabel *lblInfo;
    IBOutlet UITextField *txtFldCode;
    
    NSArray *imageArray;
}
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;


@end

@implementation FPMenuVC

- (void)viewWillAppear
{
    NSLog(@" viewWillAppear ");

}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     NSLog(@" viewDidLoad ");
    
    viewContainer.layer.cornerRadius = 4.0;
    [viewContainer setClipsToBounds:YES];
    btnGetFit.layer.cornerRadius = 4.0;
    [btnGetFit setClipsToBounds:YES];
    
    // cell.textLabel.font=[UIFont fontWithName:@"Eras Demi ITC" size:13];
    lblInfo.text = @"Fitpass connets you to the best gyms and filness studios in Delhi NCR.\nWith just one monthly pass enjoy unlimited acess to crossfit session,\ncircuit training,spinning classes,yoga sessions,zumba workouts,pilate\n      classes and much more.";
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        imageArray = [NSArray arrayWithObjects:@"menu_bg_two.jpg", @"menu_bg_three.jpg", @"menu_bg_five.jpg", nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        imageArray = [NSArray arrayWithObjects:@"ipadbg(4).jpg", @"ipadbg(11).jpg", @"ipadbg(5).jpg", nil];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    [YSSupport resetDefaults];
//    [self getCurrentLocation];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(IBAction)buttonSelected : (UIButton *)sender
{
    FPFindSudioVC *findStudioVC = nil;
    FPUpcomingVC* findclassVC   = nil;
    FPFindClassVC* findclass    = nil;
    FPBlogs      *fbBlog        = nil;
    
    
    int r = arc4random() % 3;
    
    _imgBackground.image = [UIImage imageNamed:[imageArray objectAtIndex:r]];
    
    switch (sender.tag) {
        case 1:
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                findclassVC = [[FPUpcomingVC alloc]initWithNibName:@"FPUpcomingVC" bundle:nil];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                findclassVC = [[FPUpcomingVC alloc]initWithNibName:@"FPUpcomingVC_ipad" bundle:nil];
            }
            [self.navigationController pushViewController:findclassVC animated:YES];
            break;
        case 2:
            
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                findclass = [[FPFindClassVC alloc]initWithNibName:@"FPFindClassVC" bundle:nil];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                findclass = [[FPFindClassVC alloc]initWithNibName:@"FPFindClassVC_ipad" bundle:nil];
            }
            
             [self.navigationController pushViewController:findclass animated:YES];
            break;
        case 3:
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                findStudioVC = [[FPFindSudioVC alloc] initWithNibName:@"FPFindSudioVC" bundle:nil];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                findStudioVC = [[FPFindSudioVC alloc] initWithNibName:@"FPFindSudioVC_ipad" bundle:nil];
            }
            
            [self.navigationController pushViewController:findStudioVC animated:YES];
            break;
        case 4:
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            {
                fbBlog = [[FPBlogs alloc] initWithNibName:@"FPBlogs" bundle:nil];
            }
            else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
            {
                fbBlog = [[FPBlogs alloc] initWithNibName:@"FPBlogs_ipad" bundle:nil];
            }
            [self.navigationController pushViewController:fbBlog animated:YES];
            break;
            
        default:
            break;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [txtFldCode resignFirstResponder];
    return YES;
}

- (IBAction)backTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
