//
//  FPReserveClass.h
//  FitPass
//
//  Created by JITEN on 7/23/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WorkOutModel.h"

@interface FPReserveClass : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnconformation;
@property (weak, nonatomic) IBOutlet NSString *selectedDate;
@property (weak, nonatomic) IBOutlet UIView *viewBlur;
@property (weak, nonatomic) NSString *strBatch_id;
@property (weak, nonatomic) IBOutlet UILabel *lblClassName;
@property (weak, nonatomic) IBOutlet UILabel *lblActivity;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblCancelationDataQus;
@property (weak, nonatomic) IBOutlet UIView *viewYESNO;

@property (weak, nonatomic) IBOutlet UILabel *user_notice;
@property (weak, nonatomic) IBOutlet UIView *viewConfirmationReservation;
@property (weak, nonatomic) IBOutlet UIImageView *imgBluredImage;
@property (strong,nonatomic) WorkOutModel *workoutData;
@end