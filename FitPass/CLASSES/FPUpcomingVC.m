//
//  FPUpcomingVC.m
//  FitPass
//
//  Created by bsetec on 7/10/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPUpcomingVC.h"
#import "FavouriteStudioVC.h"
#import "FpSettingVC.h"
#import "Keys.h"
#import "Generalmodel.h"
#import "WorkOutModel.h"
#import "WorkOutDetailsVC.h"
#import "YSAPICalls.h"
#import "UpComingWorkOutModel.h"
#import "PastWorkOutModel.h"
#import "MBProgressHUD.h"
#import "PastUpcomming.h" // UITableViewCell
#import "FPFindClassVC.h"
#import "FPFindSudioVC.h"

//[MBProgressHUD showHUDAddedTo:self.view animated:YES];
//[MBProgressHUD  hideHUDForView:self.view animated:YES];

@interface FPUpcomingVC ()
{
    NSMutableArray *arrUpcomingWorkout,*arrPasWorkout;
    IBOutlet UIView *viewSegment;
    __weak IBOutlet UILabel *lblNodata;
    
    IBOutlet UITableViewCell *nodataCell;
    //for checking the response firsttime loading
    BOOL isLoadingFirstTime;
    // IBOutlet UIView *viewDescription;
    IBOutlet UIView *viewHeader;
    
}
- (IBAction)btnNavigation2StudionWorkout:(id)sender;

@property (nonatomic,retain) PastWorkOutModel *pastWorkOut;
@end

@implementation FPUpcomingVC
@synthesize btnupcoming,btnPast,str;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isLoadingFirstTime = YES;
    fitpass_tableview.delegate = self;
    fitpass_tableview.dataSource = self;
    isUpcomingWorkout = YES;
    //INTIALISATION OF ARRAY
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        if ([[UIScreen mainScreen] bounds].size.height >= 667.0f)
        {
            [viewHeaderDetail setFrame:CGRectMake(viewHeaderDetail.frame.origin.x, viewHeaderDetail.frame.origin.y+50, viewHeaderDetail.frame.size.width, viewHeaderDetail.frame.size.height)];
        }
    }
    
    arrUpcomingWorkout  = [NSMutableArray array];
    arrPasWorkout  = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    [ fitpass_tableview reloadData];
    
    [super viewDidAppear:animated];

    if(isUpcomingWorkout)
        [self segmentTapped:btnupcoming];
    else
        [self segmentTapped:btnPast];
}




-(void)getUpcomingWorkOutList
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    if ([arrUpcomingWorkout count]>0)
    {
        fitpass_tableview.tableHeaderView = nil;
        fitpass_tableview.scrollEnabled = YES;
        [fitpass_tableview reloadData];
        return;
    }
    
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls getUpcomingWorkoutDetailsParamData:submitData SuccessionBlock:^(id newResponse)
     {
         Generalmodel *model = newResponse;
         [MBProgressHUD  hideHUDForView:self.view animated:YES];

         if (! model.tempArray.count > 0)
         {
             fitpass_tableview.tableHeaderView = viewHeader;
             fitpass_tableview.scrollEnabled = NO;
         }
         else
         {
             fitpass_tableview.tableHeaderView = nil;
             fitpass_tableview.scrollEnabled = YES;
         }
         
         if (model.tempArray)
         {
             [arrUpcomingWorkout addObjectsFromArray:model.tempArray];
             [fitpass_tableview reloadData];
         }
         
     } andFailureBlock:^(NSError *error) {
         [MBProgressHUD  hideHUDForView:self.view animated:YES];
         
     }];
}

//@Jnan : in this API we are getting http://fitpass.co.in/api/user/favouritesstudiolist **Studio Details**

-(void)getPastWorkOutList
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    if ([arrPasWorkout count]>0)
    {
        fitpass_tableview.tableHeaderView = nil;
        fitpass_tableview.scrollEnabled = YES;
        [fitpass_tableview reloadData];
        return;
    }
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@",FITPASS_USER_ID] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls getPastWorkoutDetailsParamData:submitData SuccessionBlock:^(id newResponse)
     {
         Generalmodel *model = newResponse;
         
         if (! model.tempArray.count > 0)
         {
             fitpass_tableview.tableHeaderView = viewHeader;
             fitpass_tableview.scrollEnabled = NO;
         }
         else
         {
             fitpass_tableview.tableHeaderView = nil;
             fitpass_tableview.scrollEnabled = YES;
         }
         
         if (model.tempArray)
         {
             [arrPasWorkout addObjectsFromArray:model.tempArray];
             [ fitpass_tableview reloadData];
             [MBProgressHUD  hideHUDForView:self.view animated:YES];
             
         }
     } andFailureBlock:^(NSError *error)
     {
         [MBProgressHUD  hideHUDForView:self.view animated:YES];
     }];
}

#pragma mark --- TableView Delegate ---

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([ str isEqualToString:@"action1"])
    {
        return arrUpcomingWorkout.count;
    }
    else if([ str isEqualToString:@"action2"])
    {
        return arrPasWorkout.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CellIdentifier = @"PastUpcomming";
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CellIdentifier = @"PastUpcomming_ipad";
    }
    
    PastUpcomming *cell = (PastUpcomming *) [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PastUpcomming" owner:self options:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PastUpcomming_ipad" owner:self options:nil];
        }
        
        cell = [topLevelObjects objectAtIndex:0];
    }
    if ([ str isEqualToString:@"action1"])
    {
        [cell setCellDataUpComingWorkout:[arrUpcomingWorkout objectAtIndex:indexPath.row]];
    }
    else if ([ str isEqualToString:@"action2"])
    {
        [cell setCellDataPastWorkout:[arrPasWorkout objectAtIndex:indexPath.row]];
    }
    [cell.btnSelect setEnabled:NO];
    
    cell.btnSelect.tag = indexPath.row;
    //[cell.btnSelect addTarget:self action:@selector(navigate:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    //    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        return 80;
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return 100;
    }
    return 0;
}





-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    UpComingWorkOutModel *obj;
    if ([ str isEqualToString:@"action1"])
    {
        obj = [arrUpcomingWorkout objectAtIndex:indexPath.row];
    }
    else if ([ str isEqualToString:@"action2"])
    {
        obj = [arrPasWorkout objectAtIndex:indexPath.row];
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:obj.date_of_workout];
    //[dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];

    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    
    NSLog(@"date %@",finalString);
    
    WorkOutDetailsVC *workoutObj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        workoutObj = [[WorkOutDetailsVC alloc] initWithNibName:@"WorkOutDetailsVC_New" bundle:nil];
    }
    
    if ([ str isEqualToString:@"action1"])
    {
        workoutObj.workOutData = [arrUpcomingWorkout objectAtIndex:indexPath.row];
    }
    else if ([ str isEqualToString:@"action2"])
    {
        workoutObj.workOutData = [arrPasWorkout objectAtIndex:indexPath.row];
    }
    workoutObj.selectedDate = finalString;
    workoutObj.workoutDetail_id = obj.workout_id;
    
    [dateFormatter setDateFormat:@"EEEE, MMMM dd"];
    
    workoutObj.selectedDateDisplay = [dateFormatter stringFromDate:orignalDate];
    [self.navigationController pushViewController:workoutObj animated:YES];
}

#pragma mark --- TableView Delegate End ---

- (IBAction)btnfavouritesaction:(id)sender
{
    
}

- (IBAction)btnsettings:(id)sender
{
    FpSettingVC *setting;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        setting = [[FpSettingVC alloc]initWithNibName:@"FpSettingVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        setting = [[FpSettingVC alloc]initWithNibName:@"FpSettingVC_ipad" bundle:nil];
    }
    [self.navigationController pushViewController:setting animated:YES];
}

- (IBAction)btnbackaction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

//============================================================================
- (IBAction)segmentTapped:(UIButton *)sender
{
    [self performSelector:@selector(deselectAll)];
    [((UIButton *) sender) setSelected:YES];
    sender.backgroundColor = [UIColor clearColor];
    [sender setTitleColor:[UIColor colorWithRed:25/255.0f green:196/255.0f blue:167/255.0f alpha:1.0f] forState:UIControlStateSelected];
    
    if (sender.tag == 2)
    {
        FavouriteStudioVC *findclassVC;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            findclassVC = [[FavouriteStudioVC alloc]initWithNibName:@"FavouriteStudioVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            findclassVC = [[FavouriteStudioVC alloc]initWithNibName:@"FavouriteStudioVC_ipad" bundle:nil];
        }
        [self.navigationController pushViewController:findclassVC animated:YES];
    }
    else
    {
        str = (sender.tag == 0) ? @"action1":@"action2";
        
        if ([ str isEqualToString:@"action1"])
        {
            isUpcomingWorkout = YES;
            [self getUpcomingWorkOutList];
            lblNodata.text = @"NO UPCOMING WORKOUTS";
        }
        else if ([ str isEqualToString:@"action2"])
        {
            isUpcomingWorkout = NO;
            [self getPastWorkOutList];
            lblNodata.text = @"NO PAST WORKOUTS";
        }
    }
}

- (void) deselectAll
{
    NSArray *views = [viewSegment subviews];
    for (UIView *v in views)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            [((UIButton *)v) setSelected:NO];
            ((UIButton *)v).backgroundColor = [UIColor clearColor];
            [((UIButton *)v) setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)btnNavigation2StudionWorkout:(id)sender
{
    if ([sender tag] == 0)
    {
        FPFindClassVC *obj;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            obj = [[FPFindClassVC alloc] initWithNibName:@"FPFindClassVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            obj = [[FPFindClassVC alloc] initWithNibName:@"FPFindClassVC_ipad" bundle:nil];
        }
        [self.navigationController pushViewController:obj animated:YES];
    }
    else
    {
        FPFindSudioVC *obj;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            obj = [[FPFindSudioVC alloc] initWithNibName:@"FPFindSudioVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            obj = [[FPFindSudioVC alloc] initWithNibName:@"FPFindSudioVC_ipad" bundle:nil];
        }
        [self.navigationController pushViewController:obj animated:YES];
    }
}
@end
