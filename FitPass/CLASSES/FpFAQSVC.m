//
//  FpFAQSVC.m
//  FitPass
//
//  Created by bsetec on 7/29/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FpFAQSVC.h"

@interface FpFAQSVC ()
@end

@implementation FpFAQSVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSURL *websiteUrl = [NSURL URLWithString:@"http://fitpass.co.in/sweatiquette-faq/"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:websiteUrl];
    [self.webviewfaqs loadRequest:urlRequest];
    
    self.webviewfaqs.delegate =self;
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)backaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
