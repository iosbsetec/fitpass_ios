//
//  SignInVc.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "SignInVc.h"
#import "FPMenuVC.h"
#import "ForgetPasswordVC.h"
#import "KeychainItemWrapper.h"
#import <Security/Security.h>
#import "Reachability.h"
#import "Keys.h"
#import "SPHKeyChain.h"
#import "MBProgressHUD.h"

@interface SignInVc ()
{
    IBOutlet UITextField *Email;
    IBOutlet UITextField *Password;
    IBOutlet UIButton *forgetPassword;
    IBOutlet UIButton *SignIn;
    FPAppDelegate *appDelegate;
}


@property (nonatomic, retain) KeychainItemWrapper *keychainItemWrapper;


@end

@implementation SignInVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    Email.layer.cornerRadius = 5;
    Password.layer.cornerRadius = 5;
    SignIn.layer.cornerRadius = 5;
    
     [self.view setMultipleTouchEnabled:YES];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    // Remove old red circles on screen
   
        
        NSLog(@"Touch by pothi");
    
}

//+ (BOOL) isNetworkRechable
//{
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
//    if (networkStatus == NotReachable) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"atError", nil) message:NSLocalizedString(@"amNetworkError", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"abOk", nil) otherButtonTitles:nil, nil];
//        [alert show];
//    }
//    return !(networkStatus == NotReachable);
//}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"touch happens");
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnForgetPassword:(UIButton *)sender
{
    ForgetPasswordVC *forgetPassView;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        forgetPassView = [[ForgetPasswordVC alloc] initWithNibName:@"ForgetPasswordVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        forgetPassView = [[ForgetPasswordVC alloc] initWithNibName:@"ForgetPasswordVC_ipad" bundle:nil];
    }
    
    forgetPassView.view.frame=CGRectMake(5,50,263,269);
    [self presentViewController:forgetPassView animated:YES completion:^(void)
     {
         
     }];
}

-(NSString*)getDeviceID
{
    id getvalue=[SPHKeyChain load:@"SavedKeyChainData"];
    if ([getvalue respondsToSelector:@selector(objectForKey:)])
    {
        return ([[getvalue valueForKey:@"ConstantUDID"] length]<1)?[self generateUUID]:[getvalue valueForKey:@"ConstantUDID"];
    }
    else{
        return [self generateUUID];
    }
}


-(NSString*)generateUUID
{
    NSUUID *oNSUUID = [[UIDevice currentDevice] identifierForVendor];
    NSString *UniqueID = [oNSUUID UUIDString];
    NSMutableDictionary *firstNameDict=[NSMutableDictionary new];
    [firstNameDict setObject:UniqueID forKey:@"ConstantUDID"];
    [SPHKeyChain save:@"SavedKeyChainData" data:firstNameDict];
    return UniqueID;
}


- (IBAction)backTapped:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)checkUserGivenDetails
{
    appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
    if (Email.text.length==0 || Password.text.length==0)
    {
        [appDelegate onCreateToastMsgInWindow:kREGISTEREDEMAIL];
        return NO;
    }
    
    if (![YSSupport validateEmail:Email.text])
    {
        [appDelegate onCreateToastMsgInWindow:kVALIDEMAILID];
        return NO;
    }
    
    return YES;
}

- (IBAction)signInTapped:(UIButton *)sender
{
    UIWindow *window = [[UIApplication sharedApplication] keyWindow];
    [window.rootViewController.view setUserInteractionEnabled:NO];
    
    BOOL isOK = [self checkUserGivenDetails];
    
    if (isOK)
    {
        if (![YSAPICalls isNetworkRechable])
        {
            [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
            [SignIn setEnabled:YES];
            return;
        }

        [SignIn setEnabled:NO];
        
        NSLog(@"final data %@",GETVALUE(DEVICE_TOKEN));
        NSData *submitData;
        submitData = [[NSString stringWithFormat:@"email_address=%@&password=%@&device_id=%@&device_type=iOS&device_token=%@",Email.text,Password.text,[(FPAppDelegate *)[UIApplication sharedApplication].delegate getDeviceID],GETVALUE(DEVICE_TOKEN)] dataUsingEncoding:NSUTF8StringEncoding];
        
//        submitData = [[NSString stringWithFormat:@"email_address=%@&password=%@&device_id=%@&device_type=iOS&device_token=%@",Email.text,Password.text,@"DABA0A50-7F67-4E7A-BA51-998592FEF1D5",@"e717e3e62a72e62b62fbce1d20597b7350e2467db5302f12b8ddd7b4a68763fe"] dataUsingEncoding:NSUTF8StringEncoding];
        
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        NSString * dataFinal = [[NSString alloc] initWithData:submitData encoding:NSUTF8StringEncoding];
        
        NSLog(@"final data %@",dataFinal);
      
        [YSAPICalls getLoginDetailsParamData:submitData SuccessionBlock:^(id newResponse)
         {
             [MBProgressHUD hideHUDForView:self.view animated:YES];

             [SignIn setEnabled:YES];

             Generalmodel *model = newResponse;
             if ([model.status_code isEqualToString:@"0"])
             {
                 appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
                 [appDelegate onCreateToastMsgInWindow:model.status_Msg];
             }
             else
             {
                 SETVALUE(model.user_email, kUSEREMAIL);

                 SETVALUE(model.user_id, @"Userid");
                 SETVALUE(model.authtentication, @"Authtentication");
                 SETVALUE(model.user_mobile, USERMOBILE);
                 SETVALUE(model.user_name, USERNAME);
                 SYNCHRONISE;
                 


                  UIViewController *rootController;
                 if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                 {
                     rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC" bundle:nil];
                 }
                 else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                 {
                     rootController = [[FPMenuVC alloc] initWithNibName:@"FPMenuVC_ipad" bundle:nil];
                 }
                 
                 [self.navigationController pushViewController:rootController animated:YES];
                 
                 UIWindow *window = [[UIApplication sharedApplication] keyWindow];
                 [window.rootViewController.view setUserInteractionEnabled:YES];
             }
         } andFailureBlock:^(NSError *error) {
             [MBProgressHUD hideHUDForView:self.view animated:YES];
             [SignIn setEnabled:YES];
         }];
    }
}


-(void)checkuserbuyfitpass
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }

    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&auth_key=%@",FITPASS_USER_ID,AUTHTENTICATION] dataUsingEncoding:NSUTF8StringEncoding];

    [YSAPICalls checkuserbuyfitpass:submitData SuccessionBlock:^(id newResponse) {
    NSLog(@"Response %@",newResponse);
        Generalmodel *model = newResponse;
        
        NSLog(@"Status %@",model.status);

        
    } andFailureBlock:^(NSError *error) {
        
    }];
}



@end
