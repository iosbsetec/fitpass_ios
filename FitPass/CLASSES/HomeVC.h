//
//  HomeVC.h
//  FitPass
//
//  Created by bsetec on 7/4/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JScrollView+PageControl+AutoScroll.h"
@interface HomeVC : UIViewController<UIScrollViewDelegate,JScrollViewViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lbl1obj;
@property (weak, nonatomic) IBOutlet UILabel *lbl2obj;
@property (weak, nonatomic) IBOutlet UILabel *lbl3obj;
@property (nonatomic) int currentPage;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollviewobj;
@property (weak, nonatomic) IBOutlet UIPageControl *pagecontrolobj;
@property (strong, nonatomic) NSArray *arrimages;
@property (strong, nonatomic) NSMutableArray *arrlbl1;
@property (weak, nonatomic) IBOutlet UIButton *btnSignup;
@property (weak, nonatomic) IBOutlet UIButton *btnSignin;
- (IBAction)btnSignupaction:(id)sender;


@end
