//
//  ActivityLocationVC.h
//  FitPass
//
//  Created by JITENDRA on 8/7/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ActivityLocationDelegate
- (void) ActivityLocationTableDelegateMethod: (NSString *)text headerTitle : (NSString *)header  Locationid :(NSString *)localityid;

@end



@interface ActivityLocationVC : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, retain) id <ActivityLocationDelegate> delegate;

@property (nonatomic,retain) NSString *headedrtitle;
@end
