//
//  WorkOutDetailsVC.h
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "FPGetFitVC.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>


@class WorkOutModel;
@interface WorkOutDetailsVC : UIViewController<MKMapViewDelegate,CLLocationManagerDelegate>
{
    IBOutlet UIButton *btnReserveOrCancel;
    CLLocationManager *locmanagerobj;
    CLLocation *locationobj;
    IBOutlet UIView *viewDetails;
    __weak IBOutlet UIView *viewGetFit;
    __weak IBOutlet UILabel *lblBuyStatus;
   
}
@property (assign, nonatomic) BOOL isComingFromSchedule;
@property (strong, nonatomic)  NSString *workoutDetail_id;
@property (strong, nonatomic)  NSString *getFitStatus;

@property(strong,nonatomic)IBOutlet      UIScrollView *workoutscrollview;
@property (strong, nonatomic) IBOutlet UIButton *btngetfit;

@property (strong, nonatomic) IBOutlet UIButton *btncalender;
@property (strong, nonatomic)  WorkOutModel *workOutData;
@property (strong, nonatomic)  NSString *selectedDate;
@property (strong, nonatomic)  NSString *selectedDateDisplay;

- (IBAction)btngetfitaction:(id)sender;
- (IBAction)btnbackaction:(id)sender;
- (IBAction)btncalenderaction:(id)sender;
-(IBAction)btnCheckReserveOrCancelClass:(id)sender;

@end
