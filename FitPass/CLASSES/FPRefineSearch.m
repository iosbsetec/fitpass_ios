//
//  FPRefineSearch.m
//  FitPass
//
//  Created by JITENDRA on 7/9/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import "FPRefineSearch.h"
#import "MARKRangeSlider.h"
#import "Generalmodel.h"
#import "NearByLocationModel.h"
#import "YSAPICalls.h"
#import "Keys.h"
#import "SPHServiceRequest.h"
#import "YSAPICalls.h"
#import "ActivityLocationVC.h"
#import "FPAppDelegate.h"

@interface FPRefineSearch ()<ActivityLocationDelegate>
{
    IBOutlet UIButton *btnShowers;
    IBOutlet UIButton *btnLockers;
    IBOutlet UILabel *lblShowers;
    IBOutlet UILabel *lblLockers;
    NSArray *activityArray;
    NSMutableArray *nearbylocationArray;
    int tagvalue;
    
    __weak IBOutlet UIButton *btnAearchNearBy;
    __weak IBOutlet UIButton *btnSearchActivity;
    __weak IBOutlet UIButton *btnApplay;
    NSString *workoutName,*locationname,*LocationId,*startTime,*EndTime,*parkingFacilities,*airconditionerFacilities;
    
    CLLocationCoordinate2D currentCentre;
    CLLocation *currentLocation;
}

- (IBAction)btnActivitySegment:(id)sender;
- (IBAction)applyTapped:(UIButton *)sender;

//@property (weak, nonatomic) IBOutlet UIButton *btnNearLocation;
@property (nonatomic, strong) MARKRangeSlider *rangeSlider;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) UILabel *label;
@end

@implementation FPRefineSearch
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//        [self configureLabelSlider];
    btnShowers.layer.cornerRadius = btnShowers.frame.size.width/2;
    [btnShowers setClipsToBounds:YES];
    btnLockers.layer.cornerRadius =  btnLockers.frame.size.width/2;
    [btnLockers setClipsToBounds:YES];
    [btnShowers setSelected:NO];
    [btnLockers setSelected:NO];
    btnActivity.layer.cornerRadius = 5.0;
    btnActivity.clipsToBounds = YES;
    
    btnSearchActivity.layer.cornerRadius = 5.0;
    btnSearchActivity.clipsToBounds = YES;
    
    btnAearchNearBy.layer.cornerRadius = 5.0;
    btnAearchNearBy.clipsToBounds = YES;
    
    btnNearLocation.layer.cornerRadius = 5.0;
    btnNearLocation.clipsToBounds = YES;
    
    btnApplay.layer.cornerRadius = 5.0;
    btnApplay.clipsToBounds = YES;
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strActivity = [defaults objectForKey:ACTIVITYNAME];
    NSString *strLocation = [defaults objectForKey:@"locationName"];
    
    [btnSearchActivity setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];
    [btnAearchNearBy setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 10.0, 0.0, 0.0)];

    if (!strActivity)
    {
        [btnSearchActivity setTitle:@"Search Activity" forState:UIControlStateNormal];
    }
    else
    {
        [btnSearchActivity setTitle:strActivity forState:UIControlStateNormal];
    }

    if (!strLocation)
    {
        [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
    }
    else
    {
        [btnAearchNearBy setTitle:strLocation forState:UIControlStateNormal];
    }

    if ([GETVALUE(@"airconditioner") isEqualToString:@"1"])
    {
        airconditionerFacilities = @"1";
        [btnShowers setSelected:YES];
    }
    
    if ([GETVALUE(@"carparking") isEqualToString:@"1"])
    {
        parkingFacilities = @"1";
        [btnLockers setSelected:YES];
    }
    
    [btnUseCurrLocation setSelected:NO];
    nearbylocationArray = [[NSMutableArray alloc] init];
//    [self getCurrentLocation];
    //btnApplay
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationStatusCheck) name:@"locationStatusCheck" object:nil];
    
    _locationManager = [[CLLocationManager alloc]init];
    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
    if ([_locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [_locationManager requestAlwaysAuthorization];
    }
    
    if([GETVALUE(@"locationcheck")isEqualToString:@"1"])
    {
        imgLocation.tag = 10;
        [self getCurrentLocation];
        [btnAearchNearBy setUserInteractionEnabled:NO];
        [imgLocation setImage:[UIImage imageNamed:@"checkboxtickgreen1.png"]];
        [btnAearchNearBy setTitle:@"Current Location" forState:UIControlStateNormal];
    }
    
    [self setUpViewComponents];
}

-(void)locationStatusCheck
{
    if (imgLocation.tag==10)
    {
        if([CLLocationManager locationServicesEnabled])
        {
            if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
            {
                [imgLocation setTag:20];
                latitude = 0.0;
                longitude =0.0;
                [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
                [btnAearchNearBy setUserInteractionEnabled:YES];
                [imgLocation setAccessibilityIdentifier:@"check_box_green_border1.png"];
                [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
            }
        }
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if ([btnSearchActivity.titleLabel.text isEqualToString:@"Search Activity"])
    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"getactivitylist"] != nil)
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"getactivitylist"];
        
        if ([[[NSUserDefaults standardUserDefaults] valueForKey:ACTIVITYNAME] length])
        {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:ACTIVITYNAME];
            SYNCHRONISE;
        }
    }
    
//    if ([btnAearchNearBy.titleLabel.text isEqualToString:@"Search Nearby Location"])
//    {
//        if([[NSUserDefaults standardUserDefaults] objectForKey:@"getnearestlist"] != nil)
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"getnearestlist"];
//        
//        if ([[[NSUserDefaults standardUserDefaults] valueForKey:LOCATIONID] length]) {
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOCATIONID];
//            SYNCHRONISE;
//        }
//    }
    
    
//    if([GETVALUE(@"locationcheck")isEqualToString:@"1"])
//    {
//        imgLocation.tag = 10;
//        [self getCurrentLocation];
//        [imgLocation setImage:[UIImage imageNamed:@"checkboxtickgreen1.png"]];
//        [btnAearchNearBy setTitle:@"Current Location" forState:UIControlStateNormal];
//    }
//    else
//    {
//        imgLocation.tag = 20;
//        [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
//       
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSString *strLocation = [defaults objectForKey:@"locationName"];
//        if (!strLocation)
//        {
//            [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
//        }
//        else
//        {
//            [btnAearchNearBy setTitle:strLocation forState:UIControlStateNormal];
//        }
//    }
}

- (IBAction)btnUseCurrLocation:(UIButton *)sender
{
    if (![sender isSelected])
    {
        NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
        [defs removeObjectForKey:NEARESTLIST];
        [defs removeObjectForKey:LOCATIONID];
        [self checkLocationPermission];
        [sender setSelected:YES];
    }
    else
    {
        [imgLocation setTag:20];
        [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
        [sender setSelected:NO];
        [_locationManager stopUpdatingLocation];
        latitude = 0.0;
        longitude =0.0;
        [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
        [btnAearchNearBy setUserInteractionEnabled:YES];
        
    }
}

-(void)checkLocationPermission
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorizedAlways)
        {
            [imgLocation setTag:10];
            [imgLocation setImage:[UIImage imageNamed:@"checkboxtickgreen1.png"]];
            [btnAearchNearBy setTitle:@"Current Location" forState:UIControlStateNormal];
            [btnUseCurrLocation setSelected:YES];
            [btnAearchNearBy setUserInteractionEnabled:NO];
            [self getCurrentLocation];
        }
        else if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied)
        {
            [imgLocation setTag:20];
            [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
            [btnUseCurrLocation setSelected:NO];
            latitude = 0.0;
            longitude =0.0;
            [btnAearchNearBy setUserInteractionEnabled:YES];
            [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
            [self showAlert];
        }
    }
    else
    {
        [imgLocation setTag:20];
        [btnAearchNearBy setUserInteractionEnabled:YES];
        latitude = 0.0;
        longitude =0.0;
        [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
        [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
        [self showAlert];
    }
}

-(void)showAlert
{
    UIAlertView *errorAlert = [[UIAlertView alloc]
                               initWithTitle:@"Location Permission" message:@"Please grant FitPass access to your location through settings > privacy > location services." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [errorAlert show];
}
- (void)setUpViewComponents
{
    // Init slider
    
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
           self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectMake(40, 300, CGRectGetWidth([UIScreen mainScreen].bounds)+50, 5)];
        }
        else
        {
            self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectMake(5, 280, CGRectGetWidth([UIScreen mainScreen].bounds), 5)];
        }
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.rangeSlider = [[MARKRangeSlider alloc] initWithFrame:CGRectMake(20, 320, 728, 20)];
    }
    
    // self.rangeSlider.backgroundColor = [UIColor backgroundColor];
    [self.rangeSlider addTarget:self
                         action:@selector(rangeSliderValueDidChange:)
               forControlEvents:UIControlEventValueChanged];
    self.rangeSlider.minimumValue = 00.00;
    self.rangeSlider.maximumValue = 23.59;
    self.rangeSlider.leftValue      = 00.00;
    self.rangeSlider.rightValue     = 23.59;
    self.rangeSlider.minimumDistance = 02.00;

    [self updateRangeText];
   // [self getNearByLocation];
 
    [self.view addSubview:self.rangeSlider];
}
//=========================================================================================

- (IBAction)btnClearData:(id)sender
{
    [self.delegate clearData];
    [YSSupport resetDefaults];
    [btnSearchActivity setTitle:@"Search Activity" forState:UIControlStateNormal];
    [btnAearchNearBy    setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
    [imgLocation setImage:[UIImage imageNamed:@"check_box_green_border1.png"]];
    [btnAearchNearBy setUserInteractionEnabled:YES];
    self.rangeSlider.minimumValue = 00.00;
    self.rangeSlider.maximumValue = 23.59;
    self.rangeSlider.leftValue      = 00.00;
    self.rangeSlider.rightValue     = 23.59;
    self.rangeSlider.minimumDistance = 02.00;
    [self updateRangeText];
    
    [btnLockers setSelected:NO];
    airconditionerFacilities = @"0";
    [btnShowers setSelected:NO];
    parkingFacilities = @"0";
    [btnUseCurrLocation setSelected:NO];
    latitude = 0.0;
    longitude =0.0;
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:nil forKey:@"getactivitylist"];
//    [defaults setObject:nil forKey:@"getnearestlist"];
//    [defaults synchronize];
}

-(void)getCurrentLocation
{
    self.locationManager = [[CLLocationManager alloc] init];
    [_locationManager setDelegate:self];

    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startMonitoringSignificantLocationChanges];
    [_locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
    [_locationManager startUpdatingLocation];
    currentLocation = [ self.locationManager location];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *newLocation = [locations lastObject];
    currentLocation = newLocation;
    NSLog(@"userLat: %f ,userLon:%f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    latitude = newLocation.coordinate.latitude;
    longitude = newLocation.coordinate.longitude;
    NSLog(@" reading horizontal accuracy :%f", newLocation.horizontalAccuracy);
    [_locationManager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [_locationManager stopUpdatingLocation];
    [btnUseCurrLocation setSelected:NO];
    latitude = 0.0;
    longitude =0.0;
    NSLog(@"didFailWithError: %@", error);
    
}

#pragma mark --- GetNearByLocation ---
-(void)getNearByLocation
{
    ActivityLocationVC *alVC;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        alVC = [[ActivityLocationVC alloc]initWithNibName:@"ActivityLocationVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        alVC = [[ActivityLocationVC alloc]initWithNibName:@"ActivityLocationVC_ipad" bundle:nil];
    }
    
    alVC.headedrtitle = @"Locality";
    [self.navigationController pushViewController:alVC  animated:YES];

}

#pragma mark - Actions
- (void)rangeSliderValueDidChange:(MARKRangeSlider *)slider
{
    [self updateRangeText];
}

- (void)updateRangeText
{
//    NSLog(@"%0.2f - %0.2f", self.rangeSlider.leftValue, self.rangeSlider.rightValue);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    
    NSUInteger slot = 64;
    NSDate *slotDate = [self timeFromSlot:slot*self.rangeSlider.leftValue-2];
    NSDate *slotDateUpper = [self timeFromSlot:slot*self.rangeSlider.rightValue-70];
    
    _lowerLabel.text = [dateFormatter stringFromDate:slotDate];
    _upperLabel.text = [dateFormatter stringFromDate:slotDateUpper];
    
    if ([_upperLabel.text isEqualToString:@"23:58"])
    {
        _upperLabel.text = @"23:59";
    }

    if ([_upperLabel.text isEqualToString:@"00:58"])
    {
        _upperLabel.text = @"01:00";
    }
    
    if ([_lowerLabel.text isEqualToString:@"23:58"])
    {
        _lowerLabel.text = @"00:00";
    }
}

- (NSDate *)timeFromSlot:(NSUInteger)slot
{
//    NSLog(@"Slot Values : %lu",(unsigned long)slot);
    NSDateComponents *components = [NSDateComponents new];
    [components setMinute:1 * slot];
    
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

- (NSDate *)timeFromSlotUpper:(NSUInteger)slot
{
//    NSLog(@"Slot Values Upper: %lu",(unsigned long)slot);
    NSDateComponents *components = [NSDateComponents new];
    [components setMinute:1 * slot];
    
    return [[NSCalendar currentCalendar] dateFromComponents:components];
}

-(IBAction)SelectedButton : (UIButton *) button
{
    
    if (![button isSelected])
    {
        [button setSelected:YES];
        if (button == btnLockers)
        {
            [btnLockers setSelected:YES];
            parkingFacilities = @"1";
        }
        if (button == btnShowers)
        {
            [btnShowers setSelected:YES];
            airconditionerFacilities = @"1";
        }
    }
    else
    {
        [button setSelected:NO];
        
        if (button == btnLockers)
        {
            [btnLockers setSelected:NO];
            parkingFacilities = @"0";
        }
        if (button == btnShowers)
        {
            [btnShowers setSelected:NO];
            airconditionerFacilities = @"0";
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) ActivityLocationTableDelegateMethod: (NSString *)text headerTitle:(NSString *)header Locationid:(NSString *)localityid;
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if ([header isEqualToString:@"ACTIVITY"])
    {
        if (text.length>0)
        {
            text = [text stringByReplacingOccurrencesOfString:@" ," withString:@","];
//            text = [text stringByReplacingOccurrencesOfString:@"  " withString:@" "];
            [defaults setObject:text forKey:@"activityName"];
            [btnSearchActivity setTitle:text forState:UIControlStateNormal];
        }
        else
        {
            [btnSearchActivity setTitle:@"Search Activity" forState:UIControlStateNormal];
        }
        workoutName = text;
    }
    else
    {
        if (text.length>0)
        {
            text = [text stringByReplacingOccurrencesOfString:@" ," withString:@","];
            [defaults setObject:localityid forKey:LOCATIONID];
            [defaults setObject:text forKey:@"locationName"];
            [btnAearchNearBy setTitle:text forState:UIControlStateNormal];
        }
        else
        {
            [btnAearchNearBy setTitle:@"Search Nearby Location" forState:UIControlStateNormal];
        }
//        locationname = text;
        LocationId = localityid;
    }
    [defaults synchronize];

}


- (IBAction)backTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- Activity and Search by Near Location Start ---
// Button Activity and Search by Near Location
- (IBAction)btnActivitySegment:(UIButton *)sender
{
    ActivityLocationVC *alVC;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        alVC = [[ActivityLocationVC alloc]initWithNibName:@"ActivityLocationVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        alVC = [[ActivityLocationVC alloc]initWithNibName:@"ActivityLocationVC_ipad" bundle:nil];
    }
    alVC.delegate = self;
    alVC.headedrtitle  =  ([sender tag] == 0)? @"ACTIVITY":@"LOCATION";
    
    [self.navigationController pushViewController:alVC  animated:YES];
}

- (IBAction)applyTapped:(UIButton *)sender
{
    if ([_lowerLabel.text isEqualToString:@"00:00"] && [_upperLabel.text isEqualToString:@"23:59"]&&[btnSearchActivity.titleLabel.text isEqualToString:@"Search Activity"] && [btnAearchNearBy.titleLabel.text isEqualToString:@"Search Nearby Location"] && [btnShowers isSelected]==NO && [btnLockers isSelected]==NO)
    {
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [window.rootViewController.view setUserInteractionEnabled:NO];
        
        FPAppDelegate *appDelegate = (FPAppDelegate *)[[UIApplication sharedApplication]delegate];
        [appDelegate onCreateToastMsgInWindow:@"Please select any one..."];
        return;
    }

    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    [dict setValue:FITPASS_USER_ID forKey:@"user_id"];
//    [dict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:ACTIVITYNAME] forKey:@"work_out_name"];
//    if (![btnSearchActivity.titleLabel.text isEqualToString:@"Search Activity"])
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:workoutName forKey:@"activityName"];
//    }
//    if (![btnAearchNearBy.titleLabel.text isEqualToString:@"Search Nearby Location"])
//    {
//        [[NSUserDefaults standardUserDefaults] setObject:locationname forKey:@"activityName"];
//    }
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:ACTIVITYNAME] length]) {
        [dict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:ACTIVITYNAME] forKey:@"work_out_name"];
    }
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:LOCATIONID] length]) {
        [dict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:LOCATIONID] forKey:@"locality_id"];
    }
    
    if (imgLocation.tag==10)
    {
        [dict setValue:[NSString stringWithFormat:@"%f",latitude] forKey:@"latitude"];
        [dict setValue:[NSString stringWithFormat:@"%f",longitude] forKey:@"longitude"];
    }

    if (![_lowerLabel.text isEqualToString:@"00:00"])
    {
        [dict setValue:[NSString stringWithFormat:@"%@",_lowerLabel.text]  forKey:@"start_time"];
        [dict setValue:[NSString stringWithFormat:@"%@",_upperLabel.text]  forKey:@"end_time"];
    }
    
    if (![_upperLabel.text isEqualToString:@"23:59"])
    {
        [dict setValue:[NSString stringWithFormat:@"%@",_lowerLabel.text]  forKey:@"start_time"];
        [dict setValue:[NSString stringWithFormat:@"%@",_upperLabel.text]  forKey:@"end_time"];
    }
    
    if ([parkingFacilities isEqualToString:@"1"])
    {
        SETVALUE(@"1", @"carparking");
        [dict setValue:parkingFacilities forKey:@"parking_facility"];
    }
    else
        SETVALUE(@"0", @"carparking");
    
    if ([airconditionerFacilities isEqualToString:@"1"])
    {
        SETVALUE(@"1", @"airconditioner");
        [dict setValue:airconditionerFacilities forKey:@"air_conditioner"];
    }
    else
        SETVALUE(@"0", @"airconditioner");

    
    if (imgLocation.tag==10)
    {
        if (longitude!=0.0 && latitude!=0.0)
        {
            SETVALUE(@"1", @"locationcheck");
        }
    }
    else
        SETVALUE(@"0", @"locationcheck");
    
    SYNCHRONISE;

    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"REFINESERACH" object:nil userInfo:dict];
    [self backTapped:nil];
}

#pragma mark --- Activity and Search by Near Location End ---

@end
