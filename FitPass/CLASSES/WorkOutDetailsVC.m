//
//  WorkOutDetailsVC.m
//  FavouriteStudio
//
//  Created by bsetec on 7/17/15.
//  Copyright (c) 2015 bsetec. All rights reserved.
//

#import "WorkOutDetailsVC.h"
#import "FPReserveClass.h"
#import "YSAPICalls.h"
#import "Keys.h"
#import "Generalmodel.h"
#import "WorkOutModel.h"
#import "AsyncImageView.h"
#import "FPStudioDetailsVC.h"
#import "MBProgressHUD.h"
#import  <EventKit/EventKit.h>
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "FpUpdateProfileVC.h"
#import "UIImageEffects.h"

@interface WorkOutDetailsVC () <MFMailComposeViewControllerDelegate,UIActionSheetDelegate>
{

    __weak IBOutlet UIImageView *imgVStudio;
    
    __weak IBOutlet UIImageView *imgVLike;
    
    __weak IBOutlet UILabel *lblStudioName;
    
    __weak IBOutlet UILabel *lblLocation;
    __weak IBOutlet UILabel *lblRatingValue;
    __weak IBOutlet UILabel *lblNearestMetroStation;
    __weak IBOutlet UILabel *lblAddress1;
    __weak IBOutlet UILabel *lblAddress2;
    __weak IBOutlet UILabel *lblLocality;
    __weak IBOutlet UILabel *lblAbout;
    __weak IBOutlet UILabel *lblDate;
     IBOutlet UIButton *btnShare;

    __weak IBOutlet UILabel *lblstudioTitles;
    
    
    __weak IBOutlet UILabel *lblStartEndTime;
    __weak IBOutlet UILabel *lblDescription;
    
    
    __weak IBOutlet UITextView *txtViewAbout;
    CLGeocoder *geocoderobj;
    
    __weak IBOutlet UIButton *btnCreateAccount;
    __weak IBOutlet UIView *viewMorefit;
    __weak IBOutlet UIImageView *imgVAirconditioner;
    __weak IBOutlet UIImageView *imgVCarparking;
    BOOL isStudioDetailsRetrive;
    IBOutlet MKMapView *mapviewobj;
    BOOL isLoadingFirstTime;
    NSString *studio_id_str;
    IBOutlet UIView *viewAccount;
}
- (IBAction)viewAccount:(UIButton *)sender;
- (IBAction)cancelAccountTapped:(UIButton *)sender;

- (IBAction)btnReserveClass:(id)sender;
- (IBAction)btnCancel:(id)sender;
// 2 Buttons Reserve, Cancel
// Depending upon the status it show,
// Already Reserved means it shows Cancel Button or it show Reserve Button
// Currently i hiding Cancel button / (Alpha Value 0)
- (IBAction)btnClassImage:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnClassImageVar;
- (IBAction)btnSeemoreclick:(id)sender;

@end

@implementation WorkOutDetailsVC
@synthesize workoutscrollview;
@synthesize isComingFromSchedule;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissPopupAction:) name:@"closeReservation" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAddWorkoutsToCalendar) name:@"addworkoutdetailstocalendar" object:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _btnClassImageVar.layer.cornerRadius = 5.0;
    _btnClassImageVar.clipsToBounds = YES;
    _btngetfit.layer.cornerRadius = 5.0;
    btnCreateAccount.layer.cornerRadius = 5.0;
//    viewGetFit.hidden = YES;
    _btngetfit.clipsToBounds = YES;
    NSLog(@"workoutDetail_id %@",_workoutDetail_id);
    isLoadingFirstTime = YES;
    _btngetfit.hidden = YES;

    [lblBuyStatus setHidden:YES];
    NSLog(@"name %@ and mobile %@",GETVALUE(USERNAME),GETVALUE(USERMOBILE));
    
    if (![GETVALUE(USERNAME) length] && ![GETVALUE(USERMOBILE) length])
        [self showviewAccount];
    
    //    _getFitStatus = @"1";
//    if ([_getFitStatus isEqualToString:@"1"])
//    {
//        _btngetfit.hidden = YES;
//        [self onUserBoughtFitpas];
//    }
//    else
//    {
        [self checkuserbuyfitpassDetail];
        [self devicesize];
//    }
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //if (isLoadingFirstTime)
    [self getWorkoutDetails];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onUserBoughtFitpas) name:@"userboughtfitpass" object:nil];
    
    //isLoadingFirstTime = NO;
}

-(void)setScrollFrame
{
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGRect frameViewGetfit = viewGetFit.frame;
        CGRect workoutscrollviewFrame = workoutscrollview.frame;
        workoutscrollviewFrame.origin.y = frameViewGetfit.origin.y;
        workoutscrollview.frame = workoutscrollviewFrame;
    }
}

-(void)onUserBoughtFitpas
{
    viewGetFit.hidden = YES;
    [self setScrollFrame];
    [self devicesize];
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        CGRect rect = workoutscrollview.frame;
        rect.origin.y = 50;
         rect.size.height =workoutscrollview.frame.size.height+130;
        [workoutscrollview setFrame:rect];
        [workoutscrollview setContentSize:CGSizeMake(768,1440)];
    }
}

-(void)devicesize
{
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        NSLog(@"%f",[[UIScreen mainScreen] bounds].size.height);
        if ([[UIScreen mainScreen] bounds].size.height == 568.0f)
        {
            [workoutscrollview setContentSize:CGSizeMake(320, (viewGetFit.hidden)?1345-viewGetFit.frame.size.height:1353)];
        }
        else if ([[UIScreen mainScreen] bounds].size.height >= 667.0f)
        {
            if ([[UIScreen mainScreen] bounds].size.height == 667.0f)
                [workoutscrollview setContentSize:CGSizeMake(320, (viewGetFit.hidden)?1261-viewGetFit.frame.size.height:1261)];
            else
                [workoutscrollview setContentSize:CGSizeMake(320, (viewGetFit.hidden)?1193-viewGetFit.frame.size.height:1193)];
        }
        else
        {
            [workoutscrollview setContentSize:CGSizeMake(320,(viewGetFit.hidden)?1258-viewGetFit.frame.size.height:1258)];
        }
    }
    else if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        [workoutscrollview setContentSize:CGSizeMake(768, (viewGetFit.hidden)?1440-viewGetFit.frame.size.height:1440)];
    }
}

-(void)checkuserbuyfitpassDetail
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
    NSData *submitData = [[NSString stringWithFormat:@"user_id=%@&auth_key=%@",FITPASS_USER_ID,AUTHTENTICATION] dataUsingEncoding:NSUTF8StringEncoding];
    
    [YSAPICalls checkuserbuyfitpass:submitData SuccessionBlock:^(id newResponse) {
        
        NSLog(@"Response %@",newResponse);
        Generalmodel *model = newResponse;
        /*
         buy_status=1, 			User has bought the fitpass. Cycle is running,
         buy_status=2,			User has bought the fitpass. But cycle had expired,
         buy_status=3,			User don't have buy fitpass
         
         in case 1 hide the get fit button
         in case 3 show the get fit button
         
         */
        NSLog(@"Status %@",model.status);
        if ([model.status isEqualToString:@"1"]) {
            viewGetFit.hidden = NO;
            _btngetfit.hidden = YES;
          //  [self setScrollFrame];
            
            //  lblBuyStatus.text = @"User has bought the fitpass. Cycle is running";
        }
        else if ([model.status isEqualToString:@"2"]) {
            viewGetFit.hidden = NO;
            _btngetfit.hidden = NO;
        }
        else // 1
        {
            viewGetFit.hidden = NO;
            _btngetfit.hidden = NO;
        }
        
        [self devicesize];
        
    } andFailureBlock:^(NSError *error) {
        
    }];
}

-(void)showviewAccount
{
    [viewAccount setFrame:CGRectMake(self.view.frame.size.width, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:viewAccount];
    
    [UIView animateWithDuration:0.7f
                          delay:0.9f
                        options: UIViewAnimationOptionTransitionFlipFromTop
                     animations:^{
                         [viewAccount setFrame:CGRectMake(0.0f, 0.0f, self.view.frame.size.width, self.view.frame.size.height)];
                     }
                     completion:nil];
}

-(void)getWorkoutDetails
{
    if (![YSAPICalls isNetworkRechable])
    {
        [APP_DELEGATE onCreateToastMsgInWindow:NOINTERNET];
        return;
    }
    
   [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString *work_id;
    if ([_workoutDetail_id length]) {
         work_id = _workoutDetail_id;
    }
    else
    {
        work_id = _workOutData.work_out_id;
    }
    
    NSDictionary *paramDict;
    
    paramDict = [NSDictionary dictionaryWithObjectsAndKeys:FITPASS_USER_ID,@"user_id",work_id,@"work_out_id",_selectedDate,@"date_of_workout",_workOutData.batch_id,@"batch_id",nil];
    [YSAPICalls getWorkoutDetailswithParamDict:paramDict SuccessionBlock:^(id newResponse) {
        isStudioDetailsRetrive = YES;
        Generalmodel *model = newResponse;
       
        _workOutData = [model.tempArray lastObject];
//        _workOutData.user_notice = @"";
        [self setScreenData:_workOutData];
        [self setMapData];
        [MBProgressHUD  hideHUDForView:self.view animated:YES];

    } andFailureBlock:^(NSError *error) {
        [MBProgressHUD  hideHUDForView:self.view animated:YES];

    }];
}

-(void)setScreenData : (WorkOutModel *)studioData
{
    NSLog(@"%@",studioData.studio_name);
    [lblBuyStatus setHidden:NO];
    imgVStudio.imageURL =  [NSURL URLWithString:studioData.work_out_pic];
    lblStudioName.text  = studioData.studio_name;
    lblNearestMetroStation.text  = studioData.nearest_metrostation;
    lblAddress1.text    = studioData.address_line1;
    lblAddress2.text    = studioData.address_line2;
    lblLocation.text    = [NSString stringWithFormat:@"%@ - %@",studioData.work_out_name,studioData.locality_name];
    lblLocality.text    = studioData.locality_name;
    studio_id_str       = studioData.studio_id;
    lblDescription.text = studioData.work_description;
    lblDescription.lineBreakMode = NSLineBreakByWordWrapping;
    
    if ([studioData.user_notice length]==0 && [_btngetfit isHidden])
    {
        [self onUserBoughtFitpas];
    }
    else if([studioData.user_notice length])
    {
        CGRect newlblStatus = [studioData.user_notice boundingRectWithSize:CGSizeMake(lblBuyStatus.frame.size.width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: lblBuyStatus.font} context:nil];
        
        CGRect rectlblAct = lblBuyStatus.frame;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            rectlblAct.size.height = newlblStatus.size.height+5;
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            if (lblBuyStatus.frame.size.height>35)
                rectlblAct.size.height = newlblStatus.size.height+5;
            else
                rectlblAct.size.height = newlblStatus.size.height+5;
            
        }
        
        if ([_btngetfit isHidden])
            rectlblAct.origin.y = (viewGetFit.frame.size.height-newlblStatus.size.height)/2;
        
        [lblBuyStatus setFrame:rectlblAct];
        
        lblBuyStatus.text = studioData.user_notice;
    }
    
    lblstudioTitles.text = studioData.studio_name;
   // lblDate.text = studioData.date_of_workout;
    lblStartEndTime.text = [NSString stringWithFormat:@"%@ - %@",studioData.start_time,studioData.end_time];
    
    imgVAirconditioner.image = ([studioData.air_conditioner isEqualToString:@"Yes"])? [UIImage imageNamed:@"icon_white_air_conditioned.png"]:[UIImage imageNamed:@"icon_bg_gray_conditioned"];
    
    imgVCarparking.image = ([studioData.parking_facility isEqualToString:@"Yes"])? [UIImage imageNamed:@"icon_white_parking.png"]:[UIImage imageNamed:@"icon_bg_gray_parking"];
//    txtViewAbout.attributedText = [[NSAttributedString alloc] initWithData:[studioData.WO dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//
    lblDate.text= _selectedDateDisplay;
    [self.btncalender setUserInteractionEnabled:NO];
    if ([studioData.batch_status integerValue]==1)
    {
        [btnReserveOrCancel setBackgroundColor:[UIColor colorWithRed:152.0/255 green:152.0/255 blue:152.0/255 alpha:1]];
        [btnReserveOrCancel setTitle:@"UNAVAILABLE" forState:UIControlStateNormal];
    }
    else if ([studioData.batch_status integerValue]==2)
    {
        [btnReserveOrCancel setBackgroundColor:TILLDCOLOR];
        [btnReserveOrCancel setTitle:@"CANCEL" forState:UIControlStateNormal];
    }
    else
    {
        [self.btncalender setUserInteractionEnabled:YES];
        [btnReserveOrCancel setBackgroundColor:TILLDCOLOR];
        [btnReserveOrCancel setTitle:@"RESERVE" forState:UIControlStateNormal];
    }
}

-(void)setMapData
{
    locmanagerobj = [[CLLocationManager alloc]init];
    geocoderobj = [[CLGeocoder alloc]init];
    locmanagerobj.delegate = self;
    locmanagerobj.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    locmanagerobj.distanceFilter = kCLDistanceFilterNone;
    if ([locmanagerobj respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [locmanagerobj requestWhenInUseAuthorization];
    }
    if ([locmanagerobj respondsToSelector:@selector(requestAlwaysAuthorization)]) {
        [locmanagerobj requestAlwaysAuthorization];
    }
    
    [locmanagerobj startUpdatingLocation];

    CLLocationCoordinate2D location;
    
    location.latitude = [_workOutData.latitude floatValue];
    location.longitude = [_workOutData.longitude floatValue];

    mapviewobj.region = MKCoordinateRegionMakeWithDistance(location, 800, 800);
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation  setCoordinate:location];
    [mapviewobj addAnnotation:annotation];
//    [mapviewobj setUserInteractionEnabled:NO];
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *identifier = @"MyLocation";
    //    if ([annotation isKindOfClass:[MyLocation class]]) {
    //
    MKAnnotationView *annotationView = (MKAnnotationView *) [mapviewobj dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (annotationView == nil) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"map_icon.png"];//here we use a nice image instead of the default pins
        
    } else {
        annotationView.annotation = annotation;
    }
    
    return annotationView;
    // }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dismissPopupAction:(NSNotification *) notification
{
    [self dismissViewControllerAnimated:YES completion:^
        {
           NSLog(@"popup view dismissed");
        }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btngetfitaction:(id)sender {
    FPGetFitVC *getfitobj;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        getfitobj = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        getfitobj = [[FPGetFitVC alloc]initWithNibName:@"FPGetFitVC_ipad" bundle:nil];
    }
    getfitobj.strStudioName = _workOutData.studio_name;
    [self.navigationController pushViewController:getfitobj animated:YES];
}

- (IBAction)btnbackaction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnsharingActions:(id)sender
{
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                            @"Facebook",@"twitter",@"WhatsApp",@"Mail",nil];
    popup.tag = 1;
    
    if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone)
    {
        [popup showInView:[UIApplication sharedApplication].keyWindow];
    }
    else if ([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPad)
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            [popup showFromRect:btnShare.frame inView:self.view animated:YES];
        }
        else
        {
            [popup showInView:[UIApplication sharedApplication].keyWindow];
        }
    }

}

-(void)onAddWorkoutsToCalendar
{
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            return;
            
        }
        
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title =  [NSString stringWithFormat:@"%@ \n %@ \n %@",_workOutData.studio_name ,[_workOutData.address_line1 stringByAppendingString:_workOutData.address_line2],_workOutData.locality_name];
        event.startDate = [NSDate date]; //today
        event.endDate = [event.startDate dateByAddingTimeInterval:60*60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        //            self.savedEventId = event.eventIdentifier;  //save the event id if you want to access this later
    }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        UIAlertView *aleret = [[UIAlertView alloc]initWithTitle:@"Class Added" message:[NSString stringWithFormat:@"Class Added:%@ has been added to your calender,Enjoy your workout!",_workOutData.studio_name] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        
        [aleret show];
        [self onAddWorkoutsToCalendar];
    }
}

- (IBAction)btncalenderaction:(id)sender
{
    UIAlertView *aleret = [[UIAlertView alloc]initWithTitle:@"Calender Event" message:[NSString stringWithFormat:@"Add %@ at %@ to your calendar?",_workOutData.studio_name,_workOutData.locality_name] delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes!", nil];
    
    [aleret show];
}

-(IBAction)btnCheckReserveOrCancelClass:(id)sender
{
    if ([_workOutData.batch_status integerValue]==2 || [_workOutData.batch_status integerValue]==3)
    {
        [self btnReserveClass:nil];
    }
}

- (IBAction)viewAccount:(UIButton *)sender
{
    [viewAccount removeFromSuperview];
    FpUpdateProfileVC *updateProfile;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        updateProfile =[[FpUpdateProfileVC alloc]initWithNibName:@"FpUpdateProfileVC" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        updateProfile = [[FpUpdateProfileVC alloc]initWithNibName:@"FpUpdateProfileVC_ipad" bundle:nil];
    }
    
    [self.navigationController pushViewController: updateProfile animated:YES];
}


- (IBAction)cancelAccountTapped:(UIButton *)sender {
    
    [viewAccount removeFromSuperview];
}

- (IBAction)btnReserveClass:(id)sender
{
    FPReserveClass *reserveclass;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        reserveclass = [[FPReserveClass alloc] initWithNibName:@"FPReserveClass" bundle:nil];
    }
    else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        reserveclass = [[FPReserveClass alloc] initWithNibName:@"FPReserveClass_ipad" bundle:nil];
    }
    reserveclass.view.frame=CGRectMake(5,50,263,269);
    reserveclass.lblTime.text = [NSString stringWithFormat:@"%@ - %@",_workOutData.start_time,_workOutData.end_time];
    reserveclass.lblClassName.text = _workOutData.studio_name;
    reserveclass.lblActivity.text = _workOutData.work_out_name;
    reserveclass.lblDate.text = _selectedDateDisplay;
    reserveclass.lblCancelationDataQus.text = [NSString stringWithFormat:@"Have you been to %@?",_workOutData.studio_name];

    UIImage *effectImage = nil;
    effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_workOutData.work_out_pic]]]];
    [reserveclass.imgBluredImage setImage:effectImage];
    
    reserveclass.imgBluredImage.frame = [UIScreen mainScreen].bounds;

    
    if ([_workOutData.batch_status integerValue] == 2)
    {
        CGRect lblFrame = reserveclass.btnconformation.frame;
        lblFrame.origin.y = reserveclass.lblCancelationDataQus.frame.origin.y;
        reserveclass.btnconformation.frame = lblFrame;
        reserveclass.lblCancelationDataQus.hidden = YES;
        reserveclass.viewYESNO.hidden = YES;
        [reserveclass.btnconformation setBackgroundColor:[UIColor clearColor]];
        reserveclass.btnconformation.layer.borderColor = [UIColor redColor].CGColor;
        reserveclass.btnconformation.layer.borderWidth = 1.0f;
        reserveclass.btnconformation.clipsToBounds = YES;
        [reserveclass.btnconformation setTitle:@"YES, PLEASE CANCEL" forState:UIControlStateNormal];
    }
    else
    {
        [reserveclass.btnconformation setBackgroundColor:[UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1]];
        reserveclass.btnconformation.layer.borderColor = [UIColor colorWithRed:25.0/255 green:196.0/255 blue:167.0/255 alpha:1].CGColor;
        [reserveclass.btnconformation setTitle:@"CONFIRM RESERVATION" forState:UIControlStateNormal];
    }
    
       [self presentViewController:reserveclass animated:YES completion:^(void)
    {
//        UIImage *effectImage = nil;
//        effectImage = [UIImageEffects imageByApplyingDarkEffectToImage:[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:_workOutData.work_out_pic]]]];
//        [reserveclass.imgBluredImage setImage:effectImage];
//        
//        reserveclass.imgBluredImage.frame = [UIScreen mainScreen].bounds;
        reserveclass.workoutData = _workOutData;
        reserveclass.user_notice.text = _workOutData.user_notice;
    }];
}

- (IBAction)btnSeemoreclick:(id)sender
{
    NSLog(@"See more Clicked");
    if ([studio_id_str length])
    {
        FPStudioDetailsVC *obj;
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC" bundle:nil];
        }
        else if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            obj = [[FPStudioDetailsVC alloc] initWithNibName:@"FPStudioDetailsVC_ipad" bundle:nil];
        }
        
        NSLog(@"studio_id_str =%@",studio_id_str);
        obj.studeo_id = studio_id_str;
        [self.navigationController pushViewController:obj animated:YES];
    }
}

//- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSString *strStudioName = _workOutData.share_url;
//    strStudioName = [strStudioName stringByReplacingOccurrencesOfString:@" " withString:@"-"];
//    strStudioName =_workOutData.share_url;
    if (buttonIndex==0)
    {
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentDescription = @"Work out with me on Fitpass!";
        content.contentURL = [NSURL URLWithString:strStudioName];
        [FBSDKShareDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];
//        if (  [FBSDKAccessToken currentAccessToken] || [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
//        {
//       
//            SLComposeViewController *controller = [[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook] init];
//            
//            //            [controller  setInitialText:[NSString stringWithFormat:@"Fitpass\n%@",strStudioName]];
//            [controller addURL:[NSURL URLWithString:strStudioName]];
//            //        [controller addImage:[UIImage imageWithData:screenShotData]];
//            [self presentViewController:controller animated:YES completion:Nil];
//        }
//        else
//        {
//            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"There are no Facebook accounts configured. You can add or create a Facebook account in Settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//        }
    }
    else if (buttonIndex==1)
    {
        if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
        {
            SLComposeViewController *tweetSheet = [SLComposeViewController
                                                   composeViewControllerForServiceType:SLServiceTypeTwitter];
            [tweetSheet addURL:[NSURL URLWithString:strStudioName]];
            //            [tweetSheet addImage:[UIImage imageWithData:screenShotData]];
            [tweetSheet setInitialText:[NSString stringWithFormat:@"Work out with me on Fitpass!\n%@",strStudioName]];
            [self presentViewController:tweetSheet animated:YES completion:nil];
        }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"There are no Twitter accounts configured. You can add or create a Twitter account in Settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if (buttonIndex==2)
    {
        NSString * urlWhats = [NSString stringWithFormat:@"whatsapp://send?text=Work out with me on Fitpass!\n%@",strStudioName];
        NSURL * whatsappURL = [NSURL URLWithString:[urlWhats stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        if ([[UIApplication sharedApplication] canOpenURL: whatsappURL])
        {
            [[UIApplication sharedApplication] openURL: whatsappURL];
        }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
    else if (buttonIndex==3)   // E-MAIL
    {
        if ([MFMailComposeViewController canSendMail])
        {
            NSString *emailSubject = @"Fitpass";
            NSString *messageBody = [NSString stringWithFormat:@"Work out with me on Fitpass!\n%@",strStudioName];
            
            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
            mc.mailComposeDelegate = self;
            [mc setSubject:emailSubject];
            [mc setMessageBody:messageBody isHTML:NO];
            [self.navigationController presentViewController:mc animated:YES completion:nil];
        }
        else
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"There are no email accounts configured. You can add or create a mail account in Settings" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
