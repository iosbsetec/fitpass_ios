//
//  main.m
//  FitPass
//
//  Created by JITENDRA on 7/6/15.
//  Copyright (c) 2015 Bsetec. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FPAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        //return UIApplicationMain(argc, argv, nil, NSStringFromClass([FPAppDelegate class]));
        
        return UIApplicationMain(argc, argv, NSStringFromClass([UIApplication class]), NSStringFromClass([FPAppDelegate class]));

    }
}
